<?php
session_start();
if (isset($_SESSION['user_name'])) {
    if ($_SESSION['user_group'] == 'p') {
        header("Location: /gas/head/");
    }
} else {
    header("Location: /gas");
}
include_once '../helper/navlinks.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Oak</title>
        <link rel=icon href="../images/icons/favi.jpeg" type="image/x-icon" />
        <link rel="stylesheet" href="../libs/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="../libs/css/bootstrap-responsive.css"/>
        <link rel="stylesheet" href="../libs/css/jquery-ui-1.10.4.custom.min.css"/>
        <link rel="stylesheet" href="../css/style.css"/>
    </head>
    <body>

        <div id="wrap" class="container thumbnail">

            <div id="loading" class="hidden">loading...</div>

            <div class="navbar">
                <div class="navbar-inner navbar-fixed-top">
                    <div class="container-fluid">                        
                        <div class="nav-collapse">
                            <ul class="nav nav-pills" style="padding:6px;">
                                <li class="navbar-brand" style="font-size: x-large;">
                                    <a href="javascript:void(0);"><div style="transform: rotate(-20deg);">Oak</div></a>
                                </li>
                                <?php
                                echo setNavLinks();
                                ?>
                                <li class="navbar-brand pull-right">
                                    <ul class="nav">
                                        <li class="dropdown">
                                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                                <p style="margin-top: -10px;">
                                                    <img class="img img-circle" src="../uploads/empl/media/images/<?php echo $_SESSION['image']; ?>" height="40" />
                                                    <?php echo $_SESSION['user_name']; ?>                                                    
                                                </p>
                                                <p style="margin-top: -1em;">
                                                    <b class="caret corner-caret"></b>
                                                </p>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Settings</a></li>
                                                <li><a class="links" id="logo" href="javascript:void(0);">Logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div style="position: absolute; left: 50%; top: 9%; z-index: 6; font-size: 0.8em" class="label label-danger">Station: <?php echo $_SESSION['station_name']; ?></div>

            <div class="row-fluid">
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <div id="l" class="thumbnail" style="overflow: hidden;">
                        <div style="height: 83vh; overflow-y: scroll; width: 20vw;">
                            <ul class="nav nav-pills nav-stacked" style="width: 14.4vw;;">
                                <?php echo setLeftLinks(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 col-md-10 col-lg-10">
                    <div id="r" class="thumbnail"> <!-- contains data --> </div>
                </div>
            </div>
        </div>
    </div>

    <script src="../libs/js/jquery-2.2.0.min.js"></script>
    <script src="../libs/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
    <script src="../libs/js/highcharts.js"></script>
    <script src="../libs/js/highcharts-more.js"></script>
    <script src="../libs/js/bootstrap-collapse.js"></script>
    <script src="../libs/js/globalize.js"></script>
    <script src="../libs/js/globalize.culture.de-DE.js"></script>
    <script src="js/base.js"></script>
</body>
</html>
