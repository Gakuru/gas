<?php

session_start();
include_once '../model/UILoader.php';
include_once '../model/save.php';
include_once '../model/update.php';
include_once '../model/search.php';
include_once '../model/refresh.php';
include_once '../model/categorize.php';
include_once '../model/remove.php';
include_once '../model/navigate.php';
include_once '../model/more_records_fetcher.php';
include_once '../model/chart.php';
include_once '../model/query.php';
include '../model/export/export.php';

ini_set('display_errors', 0);

/**
 * Description of control
 *
 * @author nelson
 */
class control {

    public function __construct() {
        $data = json_decode($_POST['data'], TRUE);
        switch ($data['act']) {
            case 'ui':
                new UILoader($data['fr']);
                break;
            case 'search':
                new Search($data['fr'], $data['data']);
                break;
            case 'save':
                new Save($data['fr'], $data['data']);
                break;
            case 'refresh':
                new Refresh($data['fr'], $data['data']);
                break;
            case 'categorize':
                new Categorize($data['fr'], $data['data']);
                break;
            case 'remove':
                new Remove($data['fr'], $data['data']);
                break;
            case 'update':
                new Update($data['fr'], $data['id'], $data['data']);
                break;
            case 'navigate':
                new Navigate($data['fr'], $data['data']);
                break;
            case 'more':
                new MoreRecordsFetcher($data['fr'], $data['data']);
                break;
            case 'chart':
                new Chart($data['fr'], $data['data']);
                break;
            case 'export':
                new Export($data['fr'], $data['data']);
                break;
            default:
                echo 'unimplemented';
                break;
        }
    }

}

if (isset($_SESSION['user_name'])) {
    new control();
} else {
    echo '<script> window.location="/gas/" </script>';
}
