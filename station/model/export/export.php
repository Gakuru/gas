<?php

/**
 * Export controller
 *
 * @author thorns
 */

include_once 'excel.php';

class Export {

    public function __construct($fr, $data) {
        switch ($data['act']) {
            case 'excel':
                new ExportToExcel($fr, $data);
                break;

            case 'pdf':
                break;

            case 'csv':
                break;

            case 'txt':
                break;
        }
    }

}
