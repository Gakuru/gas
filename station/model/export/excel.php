<?php

session_start();

/**
 * Export file and download as excel
 *
 * @author nelson
 */
class ExportToExcel {

    var $datum = NULL;
    //patches for rent
    var $dbQuery = NULL;
    var $datamHelper = NULL;

    //End patches for rent

    public function __construct($fr, $data) {
        $this->datum = new Datam($fr);

        $this->dbQuery = new DbQuery();

        $this->datamHelper = new datamHelper();

        $funcs = ['rent' => 'exportRentRecords'];

        $this->$funcs[$fr]();
    }

    private function exportRentRecords() {
//        $result = $this->dbQuery->getBySid('r_rent_view', $_SESSION['sid']);
        $result = $this->dbQuery->runQuery("select name as `tenant`, unit_no as `unit`,amount as `amount`,receipt_date as `receipt_date`,MONTH(from_unixtime(receipt_timestamp)) as `month`,   YEAR(from_unixtime(receipt_timestamp)) as `year` from r_rent_view ORDER BY `receipt_timestamp` ASC;");


        $this->generateTable($result);
    }

    private function generateTable($resultset) {
        $months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Tenant', 'House', 'Amount', 'Month', 'Slip date'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div>' . $value['tenant'] . '</div></td>'
                    . '<td><div>' . $value['unit'] . '</div></td>'
                    . '<td><div>' . number_format($value['amount'], 2) . '</div></td>'
                    . '<td><div>' . $months[($value['month'] - 1)] . ' ' . $value['year'] . '</div></td>'
                    . '<td><div>' . $value['receipt_date'] . '</div></td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        echo $a;
    }

}
