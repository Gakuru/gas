<?php

/**
 * Grabs relevent data to draw charts with
 *
 * @author codethorn
 */
class Chart {

    var $fr = NULL;
    var $datam = NULL;

    public function __construct($fr, $data) {
        $this->fr = $fr;
        $this->datam = new Datam($fr);
        echo json_encode($this->datam->getDashboard()[$data['act']], JSON_NUMERIC_CHECK);
    }

}
