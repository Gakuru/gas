<?php

session_start();

include_once '../../db/DbQuery.php';
include_once '../../db/DbInsert.php';
include_once '../../db/DbUpdateEntry.php';

/**
 * Specific queries
 *
 * @author codethorn
 */
class Query {

    private $dbQuery = NULL;
    private $dbInsert = NULL;
    private $dbUpdate = NULL;
    private $fr = NULL;

    public function __construct($fr) {
        $this->dbQuery = new DbQuery();
        $this->dbInsert = new DbInsert();
        $this->dbUpdate = new DbUpdateEntry();
        $this->fr = $fr;
    }

    public function createQuery($data) {
        $queries = ["select" =>
            ["cont" =>
                ["select sm.* from sys_modules sm where sm.id not in(select su.module_id from sys_user_control su where su.employee_id={$data[2]}) and published=1"]
            ]
        ];
        return $this->runQuery($data[0], $queries[$data[0]][$this->fr][$data[1]]);
    }

    private function runQuery($type, $query) {
        switch ($type) {
            case "select":
                return $this->dbQuery->runQuery($query);
        }
    }

}
