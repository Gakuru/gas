<?php

session_start();
include_once '../../db/DbQuery.php';
include_once 'datamPresent.php';
include_once 'datamHelper.php';

/**
 * Displays data from the database
 *
 * @author nelson
 */
class Datam {

    var $dbQuery = NULL;
    var $datamPresent = NULL;
    var $datamHelper = NULL;
    var $query = NULL;
    var $fr = NULL;

    public function __construct($fr) {
        $this->dbQuery = new DbQuery();
        $this->datamPresent = new DatamPresent($fr);
        $this->datamHelper = new datamHelper();
        $this->query = new Query($fr);

        $this->fr = $fr;
    }

    public function getEmployees($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('employee_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentEmployees($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getShifts($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('shift', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentShifts($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getProducts($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('product', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentProducts($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getPumps($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('pump_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentPumps($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getPrices($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('price_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentPrices($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getStorages($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('storage_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentStorages($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getStocks($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('stock_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentStocks($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getExpenses($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('expense', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentExpenses($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getWages($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('wage', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentWages($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getDuties($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('duty', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentDuties($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getCreditors($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('creditor', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentCreditors($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getDebtors($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('debtor', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentDebtors($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getPrepayClients($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('prepay_client', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentPrepayClients($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getPrepays($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('prepay_client', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentPrepays($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function _getPrepays($id, $start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByColumn('prepay_view', $id, 'prepay_client_id');
        }
        if (!empty($result)) {
            $a = $this->datamPresent->_presentPrepays($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getCredits($id, $start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByColumn('credit', $id, 'creditor_id');
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentCredits($result);
        } else {
            $a = 'Click the operations button and select credit';
        }
        return $a;
    }

    public function _getPrepaysConsumed($id, $start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByColumn('prepay_consumed_view', $id, 'prepay_client_id');
        }
        if (!empty($result)) {
            $a = $this->datamPresent->_presentPrepaysConsumed($result);
        } else {
            $a = 'There are no consumptions yet';
        }
        return $a;
    }

    public function getStationInfo($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getById('station_info', 1)[0]; //Pass the station id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentStationInfo($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getCollections($start = 0, $result = NULL) {
        $a = 'NULL';
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('fuel_collection_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentCollections($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getMobileMoney($type, $start = 0, $result = NULL) {
        switch ($type) {
            case 'mpesa':
                if (empty($result)) {
                    $result = $this->dbQuery->getBySid('m_pesa_payments', $_SESSION['sid'], $start); //Pass the business id
                }
                if (!empty($result)) {
                    $a = $this->datamPresent->presentMpesa($result);
                } else {
                    $a = 'There are no M~Pesa payments yet';
                }
                return $a;
        }
    }

    public function getDashboard() {
        return $this->datamPresent->presentDashboard();
    }

    public function getControl($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('user_account_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentUserAccounts($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getSystemModules($user_id) {
        $a = NULL;
        $result = $this->query->createQuery(["select", 0, $user_id]);
        if (!empty($result)) {
            $a = $this->datamPresent->presentSystemModules($result);
        } else {
            $a = '<ul id="sys_mods" class="connected-sortable" style="margin-left:-1px; margin-top:5px;">'
                    . '<li style="height:2px;"></li>'
                    . '</ul>';
        }
        return $a;
    }

    public function getUserModules($user_id) {
        $a = NULL;
        $result = $this->dbQuery->getByColumn('sys_user_control_view', $user_id, 'employee_id');
        if (!empty($result)) {
            $a = $this->datamPresent->presentUserModules($result);
        } else {
            $a = '<ul id="user_mods" class="connected-sortable" style="margin-left:-1px; margin-top:5px;">'
                    . '<li style="height:10px;"></li>'
                    . '</ul>';
        }
        return $a;
    }

    /** patch for rent * */
    public function getUnits($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('r_unit', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentUnits($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getTenants($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('r_tenant_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentTenants($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getRentRecords($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getBySid('r_rent_view', $_SESSION['sid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentRentRecords($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    /** end of patch for rent * */
}
