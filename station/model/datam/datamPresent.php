<?php

session_start();

/**
 * Description of datumPresent
 * 
 * Present data after receiving it from datam
 *
 * @author codethorn
 */
class DatamPresent {

    var $fr = NULL;
    var $datamHelper = NULL;
    var $dbQuery = NULL;
    var $image_path = '../uploads/';

    public function __construct($fr) {
        $this->fr = $fr;
        $this->datamHelper = new datamHelper();
        $this->dbQuery = new DbQuery();
    }

    public function presentEmployees($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Names', 'Identity No', 'Mobile No', 'Wage group', 'Duty', 'Employee since'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td class="editable hidden" itemref="text">' . $value['surname'] . '</td>'
                    . '<td class="editable hidden" itemref="text">' . $value['other_names'] . '</td>'
                    . '<td>
                    <div class="wakes-popover">' . $value['surname'] . ' ' . $value['other_names'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td><div class="editable" itemref="text">' . $value['id_no'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['wage_group'] . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['wage_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['duty_name'] . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['duty_id'] . '</div>'
                    . '</td>'
                    . '<td>' . date('M Y', $value['date']) . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentShifts($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Alias', 'Starts at', 'Ends at'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['sfrom'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['sto'] . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentProducts($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Product name'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentPumps($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Pump No', 'Dispenses', 'Initial reading',])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['pump_no'] . '</div></td>'
                    . '<td>'
                    . '<div>' . $value['product_name'] . '</div>'
                    . '<div class="hidden editable" itemref="combo">' . $value['product_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['initial_reading'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['initial_reading'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentPrices($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Product', 'Amount (Kes)'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td>'
                    . '<div>' . $value['product_name'] . '</div>'
                    . '<div class="hidden editable" itemref="combo">' . $value['product_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentStorages($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Product', 'Capacity (Lts)'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['storage_name'] . '</div></td>'
                    . '<td>'
                    . '<div>' . $value['product_name'] . '</div>'
                    . '<div class="hidden editable" itemref="combo">' . $value['product_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['capacity'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['capacity'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentStocks($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Product', 'Quantity (Lts)', 'Dropped in', 'Cost per Litre (Kes)', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td>'
                    . '<div>' . $value['product_name'] . '</div>'
                    . '<div class="hidden editable" itemref="combo">' . $value['product_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['quantity'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['quantity'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . $value['storage_name'] . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['storage_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td>' . date('r', $value['save_timestamp']) . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentExpenses($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Expense date', 'Amount (Kes)', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['expense_name'] . '</div></td>'
                    . '<td>'
                    . '<div>' . $this->datamHelper->formatTimestamp($value['expense_date'], FALSE) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $this->datamHelper->formatTimestamp($value['expense_date'], FALSE, 'd-m-Y') . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentWages($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Wage group', 'Amount (Kes)', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['wage_group'] . '</div></td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentDuties($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentCreditors($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Description', 'Mobile No', 'Credits amounting to (Kes)', 'Added on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr class="tr-link" id="' . $value['id'] . '">'
                    . '<td class="hidden filled editable" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover filled editable" itemref="text">' . $value['creditor_name'] . '</div>
                    <div class="hidden filled" itemref="text">' . $value['id'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td><div class="editable" itemref="text">' . $value['description'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . number_format($value['credit_balance'], 2) . '</div></td>'
                    . '<td><div>' . date('M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations($this->fr)
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentDebtors($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Description', 'Mobile No', 'Added on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover editable" itemref="text">' . $value['debtor_name'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="tblopsholder">'
                    . '<div class="editable" itemref="text">' . $value['description'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td><div>' . date('M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentPrepayClients($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Description', 'Mobile No', 'Added on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover editable" itemref="text">' . $value['name'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['description'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td><div>' . date('M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentPrepays($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Prepay client', 'Remaining balance (Kes)', 'Operations'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr class="tr-link" id="' . $value['id'] . '">'
                    . '<td class="hidden">'
                    . '<div class="filled editable" itemref="image">../uploads/prec/media/images/' . $value['profile_photo_path'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="wakes-popover filled editable" itemref="text">' . $value['name'] . '</div>'
                    . '<div class="hidden description-popover">'
                    . '<img class="imgpreviewer" src="../uploads/prec/media/images/' . $value['profile_photo_path'] . '" />'
                    . '</div>'
                    . '<div class="hidden filled editable" itemref="text">' . $value['id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['remaining_balance'], 2) . '</div>'
                    . '<div class="hidden filled editable" itemref="text">' . $value['remaining_balance'] . '</div>'
                    . '<div class="hidden filled editable" itemref="text">' . $value['remaining_balance'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="tblopsholder">'
                    . '<div class="hidden tblopsactions pull-left">'
                    . '<span class="useprepay btn btn-info btn-sm glyphicon glyphicon-tasks" popsinput="prepayinputdiv"></span>'
                    . '</div>'
                    . '<div class="tblops">...</div>'
                    . '</div>'
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function _presentPrepays($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Amount (Kes)'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="hidden">'
                    . '<div class="editable" itemref="image">../uploads/prec/media/images/' . $value['profile_photo_path'] . '</div>'
                    . '</td>'
                    . '<td class="hidden">'
                    . '<div class="editable" itemref="text">' . $value['name'] . '</div>'
                    . '<div class="editable" itemref="text">' . $value['prepay_client_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentCredits($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Amount (Kes)', 'date', 'Operations'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="hidden">'
                    . '<div class="editable" itemref="image">../uploads/prec/media/images/' . $value['profile_photo_path'] . '</div>'
                    . '</td>'
                    . '<td class="hidden">'
                    . '<div class="editable" itemref="text">' . $value['name'] . '</div>'
                    . '<div class="editable" itemref="text">' . $value['creditor_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amounted_to'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amounted_to'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="tblopsholder">'
                    . '<div class="hidden tblopsactions">'
                    . '<span class="edit btn btn-success btn-sm glyphicon glyphicon-briefcase" title="Clear balance"></span>'
                    . '</div>'
                    . '<div class="tblops">....</div>'
                    . '</div>'
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function _presentPrepaysConsumed($resultset) {
        $a = '<table class = "table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Used amount (Kes)', 'Balance before(Kes)', 'Balance after (Kes)', 'Fuel type', 'Amount (lts)', 'Date'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $balance_before = bcadd($value['balance'], $value['amount'], 2);
            $a.='<tr id = "' . $value['id'] . '">'
                    . '<td class = "hidden">'
                    . '<div class = "editable" itemref = "image">../uploads/prec/media/images/' . $value['profile_photo_path'] . '</div>'
                    . '</td>'
                    . '<td class = "hidden">'
                    . '<div class = "editable" itemref = "text">' . $value['name'] . '</div>'
                    . '<div class = "editable" itemref = "text">' . $value['prepay_client_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class = "hidden editable" itemref = "text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($balance_before, 2) . '</div>'
                    . '<div class = "hidden editable" itemref = "text">' . $balance_before . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['balance'], 2) . '</div>'
                    . '<div class = "hidden editable" itemref = "text">' . $value['balance'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class = "editable" itemref = "text">' . $value['product_name'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class = "editable" itemref = "text">' . $value['litres_sold'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div>'
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentStationInfo($resultset) {
        $a = '<table class = "table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Station', 'Manager', 'Location', 'Phone No', 'Address', 'Email'])
                . '<tbody>';
        $a.='<tr id = "' . $resultset['id'] . '">'
                . '<td class = "editable hidden" itemref = "image">' . $this->image_path . $this->fr . '/media/images/' . $resultset['profile_photo_path'] . '</td>'
                . '<td>
            <div class = "wakes-popover editable" itemref = "text">' . $resultset['name'] . '</div>
            <div class = "hidden description-popover">
            <img class = "imgpreviewer" src = "' . $this->image_path . $this->fr . '/media/images/' . $resultset['profile_photo_path'] . '" />
            </div>
            </td>'
                . '<td>'
                . '<div class = "editable" itemref = "text">' . $resultset['station_manager'] . '</div>'
                . '</td>'
                . '<td>'
                . '<div class = "editable" itemref = "text">' . $resultset['location'] . '</div>'
                . '</td>'
                . '<td>'
                . '<div class = "editable" itemref = "text">' . $resultset['contact_phone'] . '</div>'
                . '</td>'
                . '<td>'
                . '<div class = "editable" itemref = "text">' . $resultset['contact_address'] . '</div>'
                . '</td>'
                . '<td>'
                . '<div class = "editable" itemref = "text">' . $resultset['contact_email'] . '</div>'
                . '</td>'
                . '<td>'
                . $this->datamHelper->setTableOperations()
                . '</td>'
                . '</tr>';
        $a.='</tbody></table>';
        return $a;
    }

    public function presentCollections($resultset) {
        $a = '<table class = "table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Attendant', 'Shift', 'Pump', 'Product', 'Price', 'Initial', 'Final', 'Sold', 'Submited', 'Expencted', 'Difference', 'Saved'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $litres_sold = round($value['final_reading'] - $value['initial_reading'], 2);
            $amount_difference = round($value['expected_amount'] - $value['amount'], 2);
            $a.='<tr id = "' . $value['id'] . '" class = "' . ($amount_difference > 0 ? 'danger' : ($amount_difference < 0 ? 'info' : 'default')) . '" >'
                    . '<td><div>' . $value['employee_name'] . '</div></td>'
                    . '<td><div>' . $value['shift_name'] . '</div></td>'
                    . '<td><div>' . $value['pump_no'] . '</div></td>'
                    . '<td><div>' . $value['product_name'] . '</div></td>'
                    . '<td><div>' . $value['pump_price_per_litre'] . '</div></td>'
                    . '<td><div>' . number_format($value['initial_reading'], 2) . '</div></td>'
                    . '<td><div>' . number_format($value['final_reading'], 2) . '</div></td>'
                    . '<td><div>' . number_format($litres_sold, 2) . '</div></td>'
                    . '<td><div>' . number_format($value['amount'], 2) . '</div></td>'
                    . '<td><div>' . number_format($value['expected_amount'], 2) . '</div></td>'//                    
                    . '<td>'
                    . '<div>' . ($amount_difference != 0 ? '<strong>' . number_format($amount_difference, 2) . '</strong>' : number_format($amount_difference, 2)) . '</div>'
                    . '</td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentDashboard() {
        return [$this->storageSummary(), $this->saleSummary(), $this->salesPerShiftSummary(), $this->salesPerProductSummary(), $this->revenueSummary(), $this->stockSummary()];
    }

    private function dashSummary() {
        $a = NULL;
        $result = $this->dbQuery->runQuery("SELECT product_name,sum(capacity)capacity FROM `storage_view` where sid = {$_SESSION['sid']} group by product_id;");
        $a.='<div class = "thumbnail">';
        foreach ($result as $value) {
            $a.='<span class = "label label-default"><span>' . $value['product_name'] . '</span>&nbsp;
            <span>' . number_format($value['capacity'], 2) . '</span></span>&nbsp;
            ';
        }
        $a.='</div>';
        return $a;
    }

    private function storageSummary() {
        $result = $this->dbQuery->runQuery("SELECT product_name,sum(capacity)capacity FROM `storage_view` where sid = {$_SESSION['sid']} group by product_id;");
        foreach ($result as $value) {
            $dataSet[] = array($value['product_name'], ($value['capacity'] / 10000));
        }
        $output = array("caption" => ["title" => "Storage summary", "seireName" => "share"], "data" => $dataSet);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function saleSummary() {
        $result = $this->dbQuery->runQuery("SELECT product_name, substring(from_unixtime(save_timestamp),1,10) as day,sum(final_reading-initial_reading) as sold FROM `fuel_collection_view` where sid = {$_SESSION['sid']} group by day order by day desc;");
        foreach ($result as $value) {
            $dataSetDay[] = array($value['day']);
            $dataSetSold[] = array($value['sold']);
        }
        $output = array("caption" => ["title" => "Aggregate per day", "valueSuffix" => "lts", "ytitle" => "lts", "seriesname" => "Litres sold"], "day" => $dataSetDay, "sold" => $dataSetSold);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function salesPerShiftSummary() {
        $result = $this->dbQuery->runQuery("SELECT *,sum(final_reading-initial_reading) as sold FROM `fuel_collection_view` where sid = {$_SESSION['sid']} group by shift_id order by id desc");
        foreach ($result as $value) {
            $dataSetShift[] = array($value['shift_name']);
            $dataSetSold[] = array($value['sold']);
        }
        $output = array("caption" => ["title" => "Aggragate per shift", "valueSuffix" => "lts", "ytitle" => "lts", "seriesname" => "Litres sold"], "shift" => $dataSetShift, "sold" => $dataSetSold);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function salesPerProductSummary() {
        $result = $this->dbQuery->runQuery("SELECT *,sum(final_reading-initial_reading) as sold FROM `fuel_collection_view` where sid = {$_SESSION['sid']} group by product_id order by id desc");
        foreach ($result as $value) {
            $dataSetProduct[] = array($value['product_name']);
            $dataSetSold[] = array($value['sold']);
        }
        $output = array("caption" => ["title" => "Aggragate per product", "valueSuffix" => "lts", "ytitle" => "lts", "seriesname" => "Litres sold"], "product" => $dataSetProduct, "sold" => $dataSetSold);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function revenueSummary() {
        $result = $this->dbQuery->runQuery("SELECT sum(amount) as amount, substring(from_unixtime(save_timestamp),1,10) as day FROM `fuel_collection_view` where sid = {$_SESSION['sid']} group by day order by day desc;");
        foreach ($result as $value) {
            $dataSetDay[] = array($value['day']);
            $dataSetAmount[] = array($value['amount']);
        }
        $output = array("caption" => ["title" => "Aggragate per day", "valueSuffix" => "Kes", "ytitle" => "Kes", "seriesname" => "Amount collected"], "day" => $dataSetDay, "amount" => $dataSetAmount);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function stockSummary() {
        $scale_value = 1000;
        $result = $this->dbQuery->runQuery("SELECT * FROM `fuel_gauge_view` where sid = {$_SESSION['sid']};");
        foreach ($result as $value) {
            $dataSet[] = [$value['product_name'], (($value['fill_up'] - $value['fetch_out']) / $scale_value), ($value['capacity'] / $scale_value)];
        }
        $output = array("caption" => ["scaleValue" => "Scaled to {$scale_value}", "valueSuffix" => "Lts"], "data" => $dataSet);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    public function presentUserAccounts($resultset) {
        $a = '<table class = "table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Enrolled on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id = "' . $value['id'] . '">'
                    . '<td class = "editable hidden" itemref = "image">../uploads/empl/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
            <div class = "wakes-popover editable" itemref = "text">' . $value['name'] . '</div>
            <div class = "hidden editable" itemref = "text">' . $value['employee_id'] . '</div>
            <div class = "hidden description-popover">
            <img class = "imgpreviewer" src = "../uploads/empl/media/images/' . $value['profile_photo_path'] . '" />
            </div>
            </td>'
                    . '<td><div>' . date('D d M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentSystemModules($resultset) {
        $a = '<ul id = "sys_mods" class = "connected-sortable nav nav-pills nav-stacked">';

        foreach ($resultset as $value) {
            $a.='<li id = "' . $value['id'] . '" class = ""><a>' . $value['activity'] . '</a></li>';
        }
        $a .='</ul>';

        return $a;
    }

    public function presentUserModules($resultset) {
        $a = '<ul id = "user_mods" class = "connected-sortable nav nav-pills nav-stacked">';

        foreach ($resultset as $value) {
            $a.='<li id = "' . $value['module_id'] . '" class = ""><a>' . $value['activity'] . '</a></li>';
        }
        $a .='</ul>';

        return $a;
    }

    public function presentMpesa($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Transaction ref', 'Amount (Kes)', 'Paid in By', 'Payer phone', 'Date'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td>'
                    . '<div>' . $value['transaction_reference'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . $value['first_name'] . ' ' . $value['middle_name'] . ' ' . $value['last_name'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . $value['sender_phone'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . $value['transaction_datestamp'] . '</div>'
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    /** patch for rent * */
    public function presentUnits($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['House No', 'Unit Amount', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['unit_no'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="hidden editable" itemref="text">' . $value['unit_amount'] . '</div>'
                    . '<div>' . number_format($value['unit_amount'], 2) . '</div>'
                    . '</td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentTenants($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Names', 'Unit No', 'Tenant since'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['unit_no'] . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['unit_id'] . '</div>'
                    . '</td>'
                    . '<td>' . date('M Y', $value['save_timestamp']) . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }
    
    public function presentRentRecords($resultset) {
        $months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Tenant', 'House', 'Amount','Month','Slip date','Save date'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td><div class="editable">' . $value['unit_no'] . '</div></td>'
                    . '<td>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '<div>' . number_format($value['amount'],2) . '</div>'
                    . '</td>'
                    . '<td><div class="editable" itemref="combo">' . $months[($value['month']-1)] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['receipt_date'] . '</div></td>'
                    . '<td>' . date('M Y', $value['save_timestamp']) . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    /** end of patch for rent * */
}
