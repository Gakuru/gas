<?php

/**
 * Description of datamHelper
 * 
 * Helper functions for datam
 *
 * @author codethorn
 */
class datamHelper {

    var $operations = TRUE;

    public function getEmptyResultSetMessage() {
        return 'The blue button with a plus sign must be important!';
    }

    public function setTableHeader($headers, $operations = TRUE) {
        $this->operations = $operations;
        $a = '<thead style="font-weight:bold;"><tr>';
        foreach ($headers as $value) {
            $a.='<th>' . $value . '</th>';
        }
        if ($operations) {
            $a.='<th class="operations">Operations</th>';
        }
        $a.='</thead></tr>';

        return $a;
    }

    public function setTableOperations($fr = NULL) {
        return '<div class="tblopsholder">'
                . '<div class="hidden tblopsactions">'
                . '<div class="tblopsactions-inner">'
                . $this->appendSpecificTableOperations($fr)
                . '<span class="edit btn btn-primary btn-sm glyphicon glyphicon-edit"></span>'
                . '<span class="remove btn btn-danger btn-sm glyphicon glyphicon-trash"></span>'
                . '</div>'
                . '</div>'
                . ($this->operations == TRUE ? '<div class="tblops">....</div>' : NULL)
                . '</div>';
    }

    private function appendSpecificTableOperations($fr) {
        switch ($fr) {
            case 'cred':
                return '<span class="credit btn btn-success btn-sm glyphicon glyphicon-tasks" popsinput="creditinputdiv"></span>';
            default :
                return NULL;
        }
    }

    public function formatTimestamp($timestamp, $time = \TRUE, $format = 'D d M Y') {
        return ($time ? date($format . ' H:i:s', $timestamp) : date($format, $timestamp));
    }

}
