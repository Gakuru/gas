<?php

session_start();

/**
 * Adds life to the refresh and back to all results buttons
 *
 * @author nelson
 */
class Refresh {

    var $datum = NULL;

    public function __construct($fr, $data = NULL) {
        $this->datum = new Datam($fr);
        $funcs = ['empl' => 'refreshEmployees', 'shif' => 'refreshShifts',
            'prod' => 'refreshProducts', 'pump' => 'refreshPumps', 'pric' => 'refreshPrices',
            'stor' => 'refreshStorages', 'expe' => 'refreshExpenses', 'wage' => 'refreshWages',
            'duty' => 'refreshDuties', 'cred' => 'refreshCreditors', 'debt' => 'refreshDebtors',
            'prec' => 'refreshPrepayClients', 'prep' => 'refreshPrepays', 'coll' => 'refreshCollections',
            'dash' => 'refreshDashBoard', 'cont' => 'refreshControls', 'stoc' => 'refreshStocks',
            'unit' => 'refreshUnits','tena' => 'refreshTenants'];
        $this->$funcs[$fr]($data);
    }

    private function refreshEmployees() {
        echo $this->datum->getEmployees();
    }

    private function refreshShifts() {
        echo $this->datum->getShifts();
    }

    private function refreshProducts() {
        echo $this->datum->getProducts();
    }

    private function refreshPumps() {
        echo $this->datum->getPumps();
    }

    private function refreshPrices() {
        echo $this->datum->getPrices();
    }

    private function refreshStorages() {
        echo $this->datum->getStorages();
    }

    private function refreshStocks() {
        echo $this->datum->getStocks();
    }

    private function refreshExpenses() {
        echo $this->datum->getExpenses();
    }

    private function refreshWages() {
        echo $this->datum->getWages();
    }

    private function refreshDuties() {
        echo $this->datum->getDuties();
    }

    private function refreshCreditors() {
        echo $this->datum->getCreditors();
    }

    private function refreshDebtors() {
        echo $this->datum->getDebtors();
    }

    private function refreshPrepayClients() {
        echo $this->datum->getPrepayClients();
    }

    private function refreshPrepays() {
        echo $this->datum->getPrepays();
    }

    private function refreshCollections() {
        echo $this->datum->getCollections();
    }

    private function refreshDashBoard() {
        echo $this->datum->getDashboard();
    }

    private function refreshControls($data = NULL) {
        if (empty($data)) {
            echo $this->datum->getControl();
        } else {
            $output = array('sys_mods' => $this->datum->getSystemModules($data['data']), 'user_mods' => $this->datum->getUserModules($data['data']));
            echo json_encode($output, JSON_NUMERIC_CHECK);
        }
    }

    /** patch for rent * */
    
    private function refreshUnits() {
        echo $this->datum->getUnits();
    }
    
    private function refreshTenants() {
        echo $this->datum->getTenants();
    }

    /** end patch for rent * */
}
