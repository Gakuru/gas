<?php

session_start();
include_once '../../db/DbInsert.php';
include_once '../../db/DbQuery.php';
include_once '../../db/DbUpdateEntry.php';
include_once '../../helper/uploadFile.php';

/**
 * Saves the data
 *
 * Invokes datum to display results
 *
 * @author nelson
 */
class Save {

    var $dbInsert = NULL;
    var $uploadFile = NULL;
    var $datum = NULL;
    var $dbQuery = NULL;
    var $dbUpdate = NULL;

    public function __construct($fr, $data) {
        $this->dbInsert = new DbInsert();
        $this->uploadFile = new FileUpload($fr);
        $this->datum = new Datam($fr);
        $this->dbQuery = new DbQuery();
        $this->dbUpdate = new DbUpdateEntry();
        $funcs = ['empl' => 'saveEmployee', 'shif' => 'saveShift', 'prod' => 'saveProduct',
            'pump' => 'savePump', 'pric' => 'savePrice', 'stor' => 'saveStorage',
            'expe' => 'saveExpense', 'wage' => 'saveWage', 'duty' => 'saveDuty',
            'cred' => 'saveCreditor', 'debt' => 'saveDebtor', 'prec' => 'savePrepayClient',
            'prep' => 'savePrepay', 'coll' => 'saveCollection', 'cont' => 'saveUserAccount',
            'acon' => 'saveUserModule', 'stoc' => 'saveStock', 'unit' => 'saveUnit',
            'tena' => 'saveTenant', 'rent' => 'saveRent'];
        if (array_key_exists('act', $data)) {
            $this->$funcs[$fr](explode(',', $data['data']), $data['act']);
        } else {
            $this->$funcs[$fr](explode(',', $data['data']), json_decode(str_replace("'", '"', $data['params']), true));
        }
    }

    private function saveEmployee($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_saveEmployee';
        if ($this->uploadFile->uploadedFile($timestamp)) {
//Save other data
            $this->$save_function($data, $timestamp);
        } else {
//Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _saveEmployee($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('employee_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$data[3]}", "{$data[4]}", "{$data[5]}"
            , "{$this->uploadFile->uploaded_file_name}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getEmployees');
    }

    private function saveShift($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('shift_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getShifts');
    }

    private function saveProduct($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('product_insert_pro', ["{$data[0]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getProducts');
    }

    private function savePump($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('pump_insert_pro', ["{$data[0]}", "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getPumps');
    }

    private function savePrice($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('price_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getPrices');
    }

    private function saveStorage($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('storage_insert_pro', ["{$data[0]}", "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getStorages');
    }

    private function saveStock($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('stock_insert_pro', ["{$data[0]}", "{$data[1]}", "{$data[2]}", "{$data[3]}", "{$timestamp}"]);
        if ($saved) {
            $saved = $this->dbInsert->insertProcedure('fuel_gauge_insert_pro', ["{$data[0]}", "{$data[2]}", "0"]);
        }
        $this->refreshRecordSet($saved, 'getStocks');
    }

    private function saveExpense($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $data[1] = strtotime($data[1]);
        $saved = $this->dbInsert->insertProcedure('expense_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getExpenses');
    }

    private function saveWage($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('wage_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getWages');
    }

    private function saveDuty($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('duty_insert_pro', ["{$data[0]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getDuties');
    }

    private function saveCreditor($data, $options = NULL) {
        if ($options['save'] == 'crfu') {
            $this->saveCredit($data);
        } else {
            $timestamp = $_SERVER['REQUEST_TIME'];
            $save_function = '_saveCreditor';
            if ($this->uploadFile->uploadedFile($timestamp)) {
//Save other data
                $this->$save_function($data, $timestamp);
            } else {
//Image failed to upload nullify the image and save other data
                array_shift($data, '');
                $this->$save_function($data, $timestamp);
            }
        }
    }

    private function _saveCreditor($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('creditor_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$this->uploadFile->uploaded_file_name}"
            , "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getCreditors');
    }

    private function saveCredit($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $result = $this->dbQuery->getByColumn('pump_product_price_view', $data[2], 'pump_id')[0];
        $amounted_to = round($data[6] * $result['amount'], 2);
        array_splice($data, 6, 1);
        array_splice($data, 3, 0, [$result['id'], $result['price_id']]);
        array_push($data, $amounted_to, $timestamp);

        $savecred = $this->dbInsert->insertProcedure('credit_insert_pro', $data);
        $updatepump = $this->dbUpdate->updateProcedure('collection_update_final_pump_reading_pro', [$data[7], $data[2]]);
        $updatecreditbalance = $this->dbUpdate->updateProcedure('credit_update_remaining_balance', [$amounted_to, $data[5]]);

        $this->refreshRecordSet($savecred && $updatepump && $updatecreditbalance, 'getCreditors');
    }

    private function saveDebtor($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_saveDebtor';
        if ($this->uploadFile->uploadedFile($timestamp)) {
//Save other data
            $this->$save_function($data, $timestamp);
        } else {
//Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _saveDebtor($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('debtor_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$this->uploadFile->uploaded_file_name}"
            , "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getDebtors');
    }

    private function savePrepayClient($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_savePrepayClient';
        if ($this->uploadFile->uploadedFile($timestamp)) {
//Save other data
            $this->$save_function($data, $timestamp);
        } else {
//Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _savePrepayClient($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('prepay_client_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$this->uploadFile->uploaded_file_name}"
            , "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getPrepayClients');
    }

    private function savePrepay($data, $options = NULL) {
        if ($options['save'] == 'upre') {
//Save prepayment being consumed
            $this->saveConsumedPrepay($data);
        } else {
            $timestamp = $_SERVER['REQUEST_TIME'];
            $saved = $this->dbInsert->insertProcedure('prepay_deposit_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
            $updated = $this->dbUpdate->updateProcedure('prepay_client_update_remaining_balance', [$data[1], $data[0]]);
            $this->refreshRecordSet($saved, 'getPrepays');
        }
    }

    private function saveConsumedPrepay($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
//Get price , product_id and the price_id
        $result = $this->dbQuery->getByColumn('pump_product_price_view', $data[2], 'pump_id')[0];
        array_splice($data, 3, 0, [$result['id'], $result['price_id']]);
        $consume_data = array_splice($data, 6, 1);
        array_unshift($consume_data, $data[5]);
        array_push($consume_data, $data[8], $timestamp);
        array_push($data, $data[8], $timestamp);
//        var_dump($data);
        $savecoll = $this->dbInsert->insertProcedure('collection_prepay_insert_pro', $data);
        $updatepump = $this->dbUpdate->updateProcedure('collection_update_final_pump_reading_pro', [$data[7], $data[2]]);
        $saveprep = $this->dbInsert->insertProcedure('prepay_consumed_insert_pro', $consume_data);
        $updateprepaybalance = $this->dbUpdate->updateProcedure('prepay_client_fixed_update_remaining_balance', [$data[8], $data[5]]);
        if ($savecoll && $updatepump && $saveprep && $updateprepaybalance) {
//get prepayments
            $this->refreshRecordSet($savecoll, 'getPrepays');
        }
    }

    private function saveCollection($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
//Get price , product_id and the price_id
        $result = $this->dbQuery->getByColumn('pump_product_price_view', $data[2], 'pump_id')[0];
        array_splice($data, 3, 0, [$result['id'], $result['price_id']]);
        array_splice($data, 7, 1);
//        $expected_amount = round((round($result['amount'], 2) * (bcsub($data[6], $data[5], 2))), 2);
        $expected_amount = round($result['amount'] * ($data[6] - $data[5]), 2);
        array_push($data, $expected_amount, $timestamp);
        $this->verifyShort(['expected' => $expected_amount, 'submited' => $data[7], 'save_timestamp' => $timestamp], $data);
    }

    private function verifyShort($data, $_data) {
        $difference = bcsub($data['expected'], $data['submited']);
//        echo date('r', $data['save_timestamp']);
//        echo '<br/>';
//        echo date('r', strtotime('2015-01-04T12:04:30Z'));
        if ($difference > 0) {
            echo 'short';
            /** checking in m-pesa * */
//With the shift_id, get the shift start and end time
            $result = $this->dbQuery->getColumsById('shift', 'sfrom,sto', $_data[1])[0];
            var_dump($result);
            $sfrom = substr($result['sfrom'], 0, 2);
            $sto = substr($result['sto'], 0, 2);
            $result = $this->dbQuery->runQuery("SELECT sum(mpp.amount) as amount,hour(transaction_datestamp) trhr FROM `m_pesa_payments` mpp where sid={$_SESSION['sid']} and transaction_datestamp like '%" . date('Y-m-d', $data['save_timestamp']) . "%' group by trhr having trhr between {$sfrom} and {$sto};");
            var_dump($result);
            echo date('Y-m-d', $data['save_timestamp']) . ' ' . $data['save_timestamp'] . '<br />';
            echo $difference . ' ' . $result[0]['amount'];
//if not in mobile money check cards
//if not in cards check prepayments
//if not in prepayments check credits
//if not in credits then its truly a short
        } else if ($difference < 0) {
            echo 'overpay';
        } else {
//No short all submission was cash, go ahead and save the collection
            $this->_saveCollection($_data);
        }
    }

    private function _saveCollection($data) {
        $saved = $this->dbInsert->insertProcedure('collection_insert_pro', $data);
        $updated = $this->dbUpdate->updateProcedure('collection_update_final_pump_reading_pro', [$data[6], $data[2]]);
        if ($updated && $saved) {
            $saved = $this->dbInsert->insertProcedure('fuel_gauge_insert_pro', ["{$data[3]}", "0", "{$data[6]}"]);
            $this->refreshRecordSet($saved, 'getCollections');
        }
    }

    private function saveUserAccount($data, $act = NULL) {
        if ($act == NULL) {
            $timestamp = $_SERVER['REQUEST_TIME'];
            $saved = $this->dbInsert->insertProcedure('user_account_insert_pro', ["{$data[0]}", "{$timestamp}"]);
            echo $this->dbInsert->sql_error;
            if ($saved) {
                if ($this->createUser($data, $this->dbQuery->getById('employee', $data[0])[0], $timestamp)) {
                    $this->refreshRecordSet($saved, 'getControl');
                }
            }
        } else {
            switch ($act) {
                case 'amod':
                    $this->saveUserModule($data);
                    break;
                case 'rmod':
                    $this->removeUserModule($data);
                    break;
            }
        }
    }

    private function createUser($data, $result, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('login_insert_pro', ["{$data[0]}"
            , "{$result['id_no']}", "{$result['id_no']}", 'b', "{$timestamp}"]);
        echo $this->dbInsert->sql_error;
        return $saved;
    }

    private function saveUserModule($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('sys_user_control_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
    }

    private function removeUserModule($data) {
        print_r($data);
        $trashed = $this->dbInsert->executeQuery("delete from sys_user_control where employee_id={$data[0]} and module_id={$data[1]};");
//        $timestamp = $_SERVER['REQUEST_TIME'];
//        $saved = $this->dbInsert->insertProcedure('sys_user_control_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
    }

    /** patch for rent * */
    private function saveUnit($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('r_unit_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getUnits');
    }

    private function saveTenant($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('r_tenant_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getTenants');
    }

    private function saveRent($data) {
        $unit_id = $this->dbQuery->getById('r_tenant', $data[0])[0]['unit_id'];
        array_splice($data, 1, 0, $unit_id);
        array_push($data, strtotime($data[4]));
//         print_r($data);
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('r_rent_insert_pro', ["{$data[0]}", "{$data[1]}", "{$data[2]}", "{$data[3]}", "{$data[4]}", "{$data[5]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getRentRecords');
    }

    /** end of patch for rent * */
    private function refreshRecordSet($saved, $func) {
        if ($saved) {
//Refresh
            echo $this->datum->$func();
        } else {
            echo $this->dbInsert->sql_error;
            echo 0;
        }
    }

}
