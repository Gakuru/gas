<?php

/**
 * Trashes an item from the database
 *
 * @author nelson
 */
class Navigate {

    var $datum = NULL;

    public function __construct($fr, $data) {
        $this->datum = new Datam($fr);
        $funcs = ['empl' => 'navigateEmployees', 'shif' => 'navigateShifts',
            'prod' => 'navigateProducts', 'pump' => 'navigatePumps', 'pric' => 'navigatePrices',
            'stor' => 'navigateStorages', 'expe' => 'navigateExpenses', 'wage' => 'navigateWages',
            'duty' => 'navigateDuties', 'cred' => 'navigateCreditors', 'debt' => 'navigateDebtors',
            'prec' => 'navigatePrepayClients', 'prep' => 'navigatePrepays', 'coll' => 'navigateCollections',
            'cont' => 'navigateControls','stoc' => 'navigateStocks','unit' => 'navigateUnits',
            'tena' => 'navigateTenants','rent' => 'navigateRentRecords'];

        $this->$funcs[$fr]($data['start']);
    }

    private function navigateEmployees($start) {
        echo $this->datum->getEmployees($start);
    }

    private function navigateShifts($start) {
        echo $this->datum->getShifts($start);
    }

    private function navigateProducts($start) {
        echo $this->datum->getProducts($start);
    }

    private function navigatePumps($start) {
        echo $this->datum->getPumps($start);
    }

    private function navigatePrices($start) {
        echo $this->datum->getPrices($start);
    }

    private function navigateStorages($start) {
        echo $this->datum->getStorages($start);
    }
    
    private function navigateStocks($start) {
        echo $this->datum->getStocks($start);
    }

    private function navigateExpenses($start) {
        echo $this->datum->getExpenses($start);
    }

    private function navigateWages($start) {
        echo $this->datum->getWages($start);
    }

    private function navigateDuties($start) {
        echo $this->datum->getDuties($start);
    }

    private function navigateCreditors($start) {
        echo $this->datum->getCreditors($start);
    }

    private function navigateDebtors($start) {
        echo $this->datum->getDebtors($start);
    }

    private function navigatePrepayClients($start) {
        echo $this->datum->getPrepayClients($start);
    }

    private function navigatePrepays($start) {
        echo $this->datum->getPrepayClients($start);
    }

    private function navigateCollections($start) {
        echo $this->datum->getCollections($start);
    }

    private function navigateControls($start) {
        echo $this->datum->getControl($start);
    }
    
    /** patch for rent **/
    
    private function navigateUnits($start) {
        echo $this->datum->getUnits($start);
    }
    
    private function navigateTenants($start) {
        echo $this->datum->getTenants($start);
    }
    
    private function navigateRentRecords($start) {
        echo $this->datum->getRentRecords($start);
    }
    
    /** end patch for rent **/

}
