<?php

session_start();
include_once '../../helper/UIfileReader.php';
include_once 'datam/datam.php';
include_once '../../helper/comboLoader.php';
include_once '../../helper/logout.php';
//include_once './person/Employee.php';

/**
 * Loads a particular UI when its links is clicked
 *
 * @author nelson
 */
class UILoader {

    var $datum = NULL;
    var $comboBoxLoader = NULL;
    var $path = '../html/';
    var $error_404 = '404.html';
    var $files = ['empl' => 'employee.html', 'shif' => 'shift.html', 'prod' => 'product.html',
        'pump' => 'pump.html', 'pric' => 'price.html', 'stor' => 'storage.html',
        'expe' => 'expense.html', 'wage' => 'wage.html', 'duty' => 'duty.html',
        'cred' => 'creditor.html', 'debt' => 'debtor.html', 'prec' => 'prepay_client.html',
        'prep' => 'prepay.html', 'admi' => 'administration.html', 'coll' => 'collection.html',
        'dash' => 'dashboard.html', 'cont' => 'control.html', 'stoc' => 'stock.html',
        'mmon' => 'mobilemoney.html', 'acco' => 'accounting.html', 'unit' => 'unit.html',
        'tena' => 'tenant.html','rent' => 'rent_payment.html'];

    public function __construct($uiid) {
        $this->datum = new Datam($uiid);
        $this->comboBoxLoader = new comboLoader($uiid);
        switch ($uiid) {
            case 'empl':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getEmployees()]);
                break;
            case 'shif':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getShifts()]);
                break;
            case 'prod':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getProducts()]);
                break;
            case 'pump':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getPumps(), $this->comboBoxLoader->setOptions()]);
                break;
            case 'pric':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getPrices(), $this->comboBoxLoader->setOptions()]);
                break;
            case 'stor':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getStorages(), $this->comboBoxLoader->setOptions()]);
                break;
            case 'stoc':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getStocks(), $this->comboBoxLoader->setOptions()]);
                break;
            case 'expe':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getExpenses()]);
                break;
            case 'wage':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getWages()]);
                break;
            case 'duty':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getDuties()]);
                break;
            case 'cred':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getCreditors()]);
                break;
            case 'debt':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getDebtors()]);
                break;
            case 'prec':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getPrepayClients()]);
                break;
            case 'prep':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getPrepays()]);
                break;
            case 'admi':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getStationInfo()]);
                break;
            case 'mmon':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getMobileMoney('mpesa')]);
                break;
            case 'coll':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getCollections()]);
                break;
            case 'dash':
                new UIfileReader($this->path . $this->files[$uiid], $this->datum->getDashboard());
                break;
            case 'cont':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getControl()]);
                break;
            case 'acco':
                new UIfileReader($this->path . $this->files[$uiid], []);
                break;

            /** patch up for rent * */
            case 'unit':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getUnits()]);
                break;
            case 'tena':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getTenants()]);
                break;
            case 'rent':
                new UIfileReader($this->path . $this->files[$uiid], [$this->datum->getRentRecords(),$this->comboBoxLoader->setOptions()]);
                break;
            /** end of patch up for rent * */
            case 'logo':
                new Logout();
                break;
            default :
                new UIfileReader($this->path . $this->error_404);
                break;
        }
    }

}
