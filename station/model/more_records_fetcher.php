<?php

/**
 * Fetches item records when a tr link is clicked
 *
 * @author nelson
 */
class MoreRecordsFetcher {

    var $datum = NULL;
    //patches for rent
    var $dbQuery = NULL;

    //End patches for rent

    public function __construct($fr, $data) {
        $this->datum = new Datam($fr);

        $this->dbQuery = new DbQuery();

        $funcs = ['prep' => 'fetchPrepayments', 'cred' => 'fetchCredits', 'rent' => 'fetchRentRecords'];

        $this->$funcs[$fr]($data['id']);
    }

    private function fetchPrepayments($id) {
//        echo $this->datum->_getPrepays($id);
        new UIfileReader('../html/prepays.html', [$this->datum->_getPrepays($id), $this->datum->_getPrepaysConsumed($id)]);
    }

    private function fetchCredits($id) {
//        echo $this->datum->_getPrepays($id);
        new UIfileReader('../html/credits.html', [$this->datum->getCredits($id)]);
    }

    /** patch for rent * */
    private function fetchRentRecords($a) {
        $data = explode(',', $a);
//        print_r($data);

        $result = $this->dbQuery->runQuery("SELECT * FROM `r_rent_view` where month = {$data[0]} and substr(receipt_date,7) = '{$data[1]}';");

        echo $this->datum->getRentRecords(0, $result);
    }

    /** patch for rent * */
}
