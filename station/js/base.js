$(function ($) {
    var r = $('div#r'), datam = null, editing = false, loading = $('div#loading'), linkid = null, tableCursor = 0, dash_link = false;
    var standardfunctions =
            {
                allFunctions: function () {
                    this.standardFunctions().navBarFunctions().datamFunctions().imageUpload().tableOperationsClicked().trLinkClicked().graphTypeClicked();
                    $('.datepicker').datepicker({
                        dateFormat: 'dd-mm-yy'
                    });
                    //patch for rent
                    $('#year-spinner').spinner({
                        start: 2016,
                        change: function () {
                            console.info('foo');
                            console.info($(this).spinner('value'));
                        }
                    });

                    $('.month').children('li').unbind('click').on('click', function () {
                        $('.month').children('li').removeClass('active');
                        $(this).addClass('active');
                        //Get month numerical value and year as january 2016 will be 1,2016
                        var a = [$(this).index() + 1, $('#year-spinner').spinner('value')];
                        post('{"act":"more", "fr":"' + linkid + '", "data": ' + '{"id":"' + a + '"}' + '}', function (data) {
                            datam.html(data);
                        });
                    });
                    //End patch for rent
                }, standardFunctions: function () {
                    dialogFunctions.standardfunctions();
                    saveBtnClicked();
                    btnCheckBox();
                    btnRadioOption();
                    return this;
                }, navBarFunctions: function () {
                    navBarFunctions.standardNavBarFunctions();
                    return this;
                }, datamFunctions: function () {
                    window.setTimeout(function () {
                        editBtnClicked();
                        usePrepayBtnClicked();
                        creditPrepayBtnClicked();
                        removeBtnClicked();
                        tableNavigation();
                        wakesPopover();
                        timeSpinner();
                        if (dash_link) {
                            drawChart();
                        }
                    }, 0, false);
                    return this;
                }, imageUpload: function () {
                    $('.imgupload').unbind('click').on('click', function (e) {
                        var me = $(this);
                        $('#myfile').trigger('click');
                        $('#myfile').change(function (e) {
                            var reader = new FileReader(),
                                    files = e.dataTransfer ? e.dataTransfer.files : e.target.files,
                                    i = 0;
                            reader.onload = onFileLoad;
                            while (files[i])
                                reader.readAsDataURL(files[i++]);
                        });
                        function onFileLoad(e) {
                            var data = e.target.result;
                            me.attr("src", data);
                        }
                        e.preventDefault();
                    });
                    return this;
                },
                tableOperationsClicked: function () {
                    $('.tblopsholder').unbind('click').on('click', '.tblops', function (e) {
                        var me = $(this);
                        var parent = me.parents('tr:eq(0)');
                        var opsactions = parent.find('.tblopsactions');
                        if (!me.hasClass('opsactive')) {
                            $('.tblops').removeClass('opsactive');
                            $('.tblopsactions:visible').addClass('hidden');
                            me.addClass('opsactive');
                            opsactions.removeClass('hidden');
                            standardfunctions.datamFunctions();
                        } else {
                            me.removeClass('opsactive');
                            opsactions.addClass('hidden');
                        }
                        e.stopPropagation();
                    });
                    return this;
                },
                trLinkClicked: function () {
                    $('.tr-link').unbind('click').on('click', function (e) {
                        var trObject = {
                            me: $(this),
                            id: null,
                            siblings: null,
                            colspan: null,
                            more_records: null,
                            siblings_hidden: null,
                            construct: function () {
                                var me = this.me;
                                this.id = me.attr('id');
                                this.siblings = me.siblings();
                                this.siblings_hidden = this.siblings.is(':hidden');
                                this.colspan = me.children().length;
                                this.more_records = null;
                                return this;
                            }
                        };
                        trObject.construct();

                        if (!trObject.siblings_hidden) {
                            trObject.more_records = '<tr class="more_records_holder"><td style="text-align:left;" colspan="' + trObject.colspan + '"><div class="more_records" style="margin-left:-15px;">records</div></td></tr>';
                            trObject.siblings.hide();
                            trObject.me.parents('table').removeClass('table-hover');
                            //on hidding siblings show more records
                            post('{"act":"more", "fr":"' + linkid + '", "data": ' + '{"id":"' + trObject.id + '"}' + '}', function (data) {
                                $('.more_records').html(data);
                                standardfunctions.allFunctions();
                            });
                            $(trObject.more_records).insertAfter(trObject.me);
                        } else {
                            trObject.siblings.show();
                            trObject.me.parents('table').addClass('table-hover');
                            //on showing siblings remove more records
                            $('.more_records_holder').remove();
                        }
                        e.preventDefault();
                        delete(trObject);
                    });
                    return this;
                },
                graphTypeClicked: function () {
                    $('.graph-type>li').unbind('click').on('click', function (e) {
                        var me = $(this);
                        $('#graph-type-title').text(me.text());
                        e.preventDefault();
                    });
                    return this;
                }
            };

    var dialogFunctions = {
        standardfunctions: function () {
            this.dialogMinimizeBtnClicked().dialogCloseBtnClicked();
            return this;
        },
        dialogMinimizeBtnClicked: function () {
            $('.dminimize').unbind('click').on('click', function () {
                $(this).parents('.inputdiv').find('.inputholder').toggle();
            });
            return this;
        },
        dialogCloseBtnClicked: function () {
            $('.dclose').unbind('click').on('click', function () {
                $(this).parents('.inputdiv').addClass('hidden');
            });
            return this;
        }
    };

    var navBarFunctions = {
        standardNavBarFunctions: function () {
            this.newBtnClicked().refreshBtnClicked().search().backToResultsBtnClicked().printBtnClicked().excelBtnClicked().filler();
        },
        newBtnClicked: function () {
            $('.new').unbind('click').on('click', function () {
                $('.save').removeClass('hidden');
                $('.update').addClass('hidden');
                resetDataEntryForm();
                $('.inputdiv:eq(0)').removeClass('hidden');
                $('#inputholder').show();
            });
            return this;
        },
        refreshBtnClicked: function () {
            $('.refresh').unbind('click').on('click', function () {
                navBarFunctions.refresh();
            });
            return this;
        },
        backToResultsBtnClicked: function () {
            $('.backtoresults').unbind('click').on('click', function () {
                //Same functionality as refresh
                navBarFunctions.refresh();
                $('.searchbox').val('');
                $(this).addClass('hidden');
            });
            return this;
        },
        refresh: function () {
            post('{"act":"refresh", "fr":"' + linkid + '"}', function (resp) {
                datam.html(resp);
                standardfunctions.allFunctions();
            });
            return this;
        },
        printBtnClicked: function () {
            //New print functionality @v1.0
            $('.print').unbind('click').on('click', function () {
//                if (datam.find('table').is(':visible'))
//                    print(item[1] + " Collections", '<table>' + $('#datam table:visible').html() + '</table>');
                var win = window.open();
                self.focus();
                win.document.open();
                win.document.write('<html><body><title>Report</title><br><div style="text-align:left;"><br>Records</div>');
                win.document.write('<table>' + datam.find('table:visible').html() + '</table>');
                win.document.write('</body' + '></html>');
                win.document.close();
                win.print();
                win.close();

            }); //End click event on print

            return this;
        },
        excelBtnClicked: function () {
            //New print functionality @v1.0
            $('.excel').unbind('click').on('click', function () {
//                if (datam.find('table').is(':visible'))
//                    print(item[1] + " Collections", '<table>' + $('#datam table:visible').html() + '</table>');
//                var win = window.open();
//                self.focus();
//                win.document.open();
//                win.document.write('<html><body><title>Report</title><br><div style="text-align:left;"><br>Records</div>');
//                win.document.write('<table>' + datam.find('table:visible').html() + '</table>');
//                win.document.write('</body' + '></html>');
//                win.document.close();
//                win.print();
//                win.close();

//                javascript:window.open('data:application/vnd.ms-excel,' + '<table>' + datam.find('table:visible').html() + '</table>');

                post('{"act":"export", "fr":"' + linkid + '", "data": ' + '{"act":"excel"}' + '}', function (resp) {
//                    console.info(resp);

//                    var win = window.open();
//                    self.focus();
//                    win.document.open();
//                    win.document.write('<html><body><title>Report</title><br><div style="text-align:left;"><br>Records</div>');
//                    win.document.write('<table>' + resp + '</table>');
//                    win.document.write('</body' + '></html>');
//                    win.document.close();
//                    win.print();
//                    win.close();

                javascript:window.open('data:application/vnd.ms-excel,' + '<table>' + resp + '</table>');




                });


            }); //End click event on print

            return this;
        },
        search: function () {
            var param = null;
            var selections = [];
            var searchbox = null;
            var suggest = null;
            var linkto = null;
            var saveableInput = null;
            var me = null;
            var id = null;
            $('.searchable').unbind('keyup').on('keyup', function () {
                searchbox = $(this);
                suggest = $(this).parents('div').next('.suggest');
                linkto = ($(this).attr('itemid') === undefined ? linkid : $(this).attr('itemid'));
                saveableInput = searchbox.next('input:hidden.saveable');
                param = $.trim(this.value);
                if (param.length > 0) {
                    suggest.removeClass('hidden');
                    post('{"act":"search", "fr":"' + linkto + '", "data": ' + '{"act":"search","data":"' + param + '"}' + '}', function (suggestresp) {
                        suggest.html(suggestresp);
                        $('li.sresults').unbind('click').on('click', function () {
                            me = $(this);
                            id = me.attr('id');
                            if (searchbox.hasClass('searchable-inline')) {
                                search_inline();
                            } else {
                                post('{"act":"search", "fr":"' + linkid + '", "data": ' + '{"act":"present","data":"' + id + '"}' + '}', function (tableresp) {
                                    suggest.html('').addClass('hidden');
                                    $('.backtoresults').removeClass('hidden');
                                    datam.html(tableresp);
                                    standardfunctions.allFunctions();
                                });
                            }
                        });
                    });
                } else {
                    suggest.addClass('hidden').html('');
                    if (!searchbox.hasClass('searchable-inline')) {
                        navBarFunctions.refresh();
                    }
                }
            });

            {
                function search_inline() {
                    if (searchbox.hasClass('multiple-input')) {
                        $('.selections').append('<li class="label label-default">' + me.find('.extractable').text() + '&nbsp;<span class="cancel-selection">&#10007;</span></li>');
                        searchbox.val('');
                        selections.push(id);
                        saveableInput.val(selections);
                        var vals = saveableInput.val();
                        saveableInput.val(vals.replace(/,/g, ':'));
                        $('.cancel-selection').unbind('click').on('click', function () {
                            var parent = $(this).parents('li');
                            var parentId = parent.index();
                            parent.remove();
                            selections.splice(parentId, 1);
                            saveableInput.val(selections);
                            var vals = saveableInput.val();
                            saveableInput.val(vals.replace(/,/g, ':'));
                        });
                    } else if (searchbox.hasClass('sets-datam')) {
                        console.info(selections);
                        post('{"act":"refresh", "fr":"' + linkid + '","data": ' + '{"act":"redraw","data":"' + [id] + '"}' + '}', function (resp) {
                            sortable(id, $.parseJSON(resp));//Pass the entity id
                        });
                    } else {
                        var useable = (me.find('.useable').text() !== '' ? true : false);
                        if (useable) {
                            var useableVal = me.text();
                            useableVal = useableVal.replace(/ » /g, ':').split(':');
                            console.info(useableVal);
                            var toref = [];

                            me.find('span').each(function () {
                                if ($(this).attr('toref')) {
                                    var text = $(this).text();
                                    toref.push($.trim(text.replace(/ » /g, '')));
                                }
                            });

                            $('input:text').each(function () {
                                if ($(this).attr('toref')) {
                                    $(this).val(toref.pop());
                                }
                            });
                            delete(toref);
                        }
                        saveableInput.val(id);
                    }
                    if (searchbox.hasClass('refsimage')) {
                        console.info(me.find('.preview-image').attr('src'));
                        $('.imgpreview:visible').attr('src', me.find('.preview-image:visible').attr('src'));
                    }
                    searchbox.val(me.find('.extractable').text());
                    suggest.html('').addClass('hidden');
                }
            }

            return this;
        },
        filler: function () {
            $('.filler').unbind('keyup').on('keyup', function () {
                var me = $(this);
                try {
                    var oak_data = $(this).attr('oak-data').split('|');
                    var parent_input = $(this).parents('.inputdiv:visible');
                    if (me.val().length > 0) {
                        $(oak_data).each(function (i) {
                            var data = $.parseJSON(oak_data[i]);
                            var extract_elems = data.extract_elems.split(',');
                            var result = eval(parent_input.find('.inputbox:eq(' + extract_elems[0] + ')').val() + data.computation + parent_input.find('.inputbox:eq(' + extract_elems[1] + ')').val());
                            parent_input.find('.fillable:eq(' + data.fill_elem + ')').val(result.toFixed(2));
                        });
                    } else {
                        $(oak_data).each(function (i) {
                            var data = $.parseJSON(oak_data[i]);//                            
                            if (parent_input.find('.fillable:eq(' + data.fill_elem + ')').hasClass('urestable')) {
                                if (me.val().length > 0) {
                                    var extract_elems = data.extract_elems.split(',');
                                    var result = eval(parent_input.find('.inputbox:eq(' + extract_elems[0] + ')').val() + data.computation + parent_input.find('.inputbox:eq(' + extract_elems[1] + ')').val());
                                    parent_input.find('.fillable:eq(' + data.fill_elem + ')').val(result.toFixed(2));
                                } else {
                                    var defaultValue = parent_input.find('.fillable:eq(' + data.fill_elem + ')').next('.inputbox').val();
                                    parent_input.find('.fillable:eq(' + data.fill_elem + ')').val(defaultValue);
                                }
                            } else {
                                parent_input.find('.fillable:eq(' + data.fill_elem + ')').val('');
                            }
                        });
                    }
                } catch (e) {
                    console.warn(e);
                }

            });
            return this;
        }
    };

    function btnCheckBox() {
        $('.btncheckbox').unbind('click').on('click', function () {
            var text = $(this).text();
            if ($(this).hasClass('active')) {
                $(this).removeClass('btn-info');
                $(this).text($(this).attr('data-toggle-text'));
                $(this).attr('data-toggle-text', text);
            } else {
                $(this).addClass('btn-info');
                $(this).text($(this).attr('data-toggle-text'));
                $(this).attr('data-toggle-text', text);
            }
        });
    }

    function btnRadioOption() {
        $('.btnradio').unbind('click').on('click', function (e) {
            var me = $(this);
            var value = me.attr('value');
            if (value === 'all') {
                $('.refresh').trigger('click');
            } else {
                post('{"act":"categorize", "fr":"' + linkid + '","data":"' + value + '"}', function (resp) {
                    datam.html(resp);
                    standardfunctions.allFunctions();
                });
            }
            e.preventDefault();
        });
    }

    function ajaxSetup(url, type) {
        $.ajaxSetup({
            url: (url !== undefined ? url : 'control/control.php'),
            type: (type !== undefined ? type : 'POST'),
            dataType: (type !== undefined ? type : 'html')
        });
    }

    /**
     * Restore ajax settings to default
     * **/
    function ajaxReset() {
        $.ajaxSetup({
            url: 'control/control.php',
            type: 'POST',
            dataType: 'html'
        });
    }

    (function init() {
        ajaxSetup();
    }());

    (function fire() {
        navlinks();
        $('.linksleft:first').trigger('click');
    }());

    function navlinks() {
        $('.links').unbind('click').on('click', function (e) {
            $('.links').removeClass('bactive');
            $(this).addClass('bactive');
            linkid = this.id;
            dash_link = linkid === 'dash' ? true : false;
            post('{"act": "ui", "fr": "' + linkid + '"}', function (data) {
                r.html(data);
                datam = $('#datum');
                standardfunctions.allFunctions();
            });
            e.preventDefault();
        });
    }

    function editBtnClicked() {
        $('.edit').unbind('click').on('click', function (e) {
            var me = $(this);
            var parent = me.parents('tr:eq(0)');
            var id = parent.attr('id');
            var a = [];
            editing = true;
            if (editing) {
                $('.inputdiv:eq(0)').removeClass('hidden');
                var inputdiv = r.find('.inputdiv:visible');
                inputdiv.find('.save').addClass('hidden');
                inputdiv.find('.update').removeClass('hidden');
                getEditValues(parent);
                a = getInputs(me);
                if (validInput(a)) {
                    updateBtnClicked(id, a);
                } else {
                    console.info('not okay');
                }
            }
            e.preventDefault();
        });
    }

    function saveBtnClicked() {
        $('.save').unbind('click').on('click', function (e) {
            var me = $(this);
            var params = me.attr('param');
            var a = getInputs(me);
            var formdata = new FormData(me.parents('.dataform')[0]);
            if (validInput(a)) {
                var data = '{"act":"save","fr":"' + linkid + '","data": ' + '{"params":"' + params + '","data":"' + a + '"}' + ' }';
                formdata.append("data", data);
                postForm(formdata, function (resp) {
                    console.info(resp);
                    if (resp) {
                        resetDataEntryForm();
                        datam.html(resp);
                        standardfunctions.allFunctions();
                    } else {
                        console.info('there was an error saving the data, please try again later');
                    }
                });
            } else {
                console.info('not okay');
            }
        });
    }

    function usePrepayBtnClicked() {
        $('.useprepay').unbind('click').on('click', function (e) {
            resetDataEntryForm();
            var me = $(this);
            var parent = me.parents('tr:eq(0)');
            $('.' + me.attr('popsinput')).removeClass('hidden');
            fillEditValues(parent);
            e.stopPropagation();
        });
    }

    function creditPrepayBtnClicked() {
        $('.credit').unbind('click').on('click', function (e) {
            resetDataEntryForm();
            var me = $(this);
            var parent = me.parents('tr:eq(0)');
            $('.' + me.attr('popsinput')).removeClass('hidden');
            fillEditValues(parent);
            e.stopPropagation();
        });
    }

    function removeBtnClicked() {
        $('.remove').unbind('click').on('click', function () {
            var me = $(this);
            var parent = me.parents('tr');
            var id = parent.attr('id');
            post('{"act":"remove", "fr":"' + linkid + '", "data": ' + '{"id":"' + id + '"}' + '}', function (resp) {
                if (resp) {
                    parent.remove();
                }
            });
        });
    }

    function updateBtnClicked(id, a) {
        $('.update').unbind('click').on('click', function (e) {
            var formdata = new FormData($('.dataform:visible')[0]);
            a = getInputs($(this));
            var data = '{"act":"update","fr":"' + linkid + '","id":"' + id + '","data":"' + a + '"}';
            formdata.append("data", data);
            postForm(formdata, function (resp) {
                if (resp.length > 1) {
                    resetDataEntryForm();
                    datam.html(resp);
                    standardfunctions.allFunctions();
                } else {
                    console.info('there was an error saving the data, please try again later');
                }
            });
        });
    }

    function getEditValues(parent) {
        var a = [];
        var b = [];
        parent.find('.editable').each(function () {
            a.push($(this).text());
            b.push($(this).attr('itemref'));
        });
        bindEditValues(a, b, 'editable');
    }

    function fillEditValues(parent) {
        var a = [];
        var b = [];
        parent.find('.filled').each(function () {
            a.push($(this).text());
            b.push($(this).attr('itemref'));
        });
        bindEditValues(a, b, 'filled');
    }

    function bindEditValues(editVal, editRef, _class) {
        $('.inputdiv:visible').find('.' + _class).each(function (i) {
            var me = $(this);
            console.info(i, me.attr('itemref'), editRef[i]);
            if (me.attr('itemref') === editRef[i]) {
                if (me.attr('itemref') === 'image') {
                    me.attr('src', editVal[i]);
                } else if (me.attr('itemref') === 'text') {
                    me.val(editVal[i]);
                } else if (me.attr('itemref') === 'combo') {
                    me.children('option').each(function (j) {
                        if (j > 0 && editVal[i].localeCompare($(this).val()) === 0) {
                            $(this).attr('selected', 'selected');
                        }
                    });
                } else if (me.attr('itemref') === 'radio') {
                    var name = $(this).attr('name');
                    $("input[name=" + name + "][value='" + editVal[i] + "']").prop("checked", true);
                } else if (me.attr('itemref') === 'checkbox') {
                    if (editVal[i] === 'yes') {
                        me.prop("checked", true);
                    } else {
                        me.prop("checked", false);
                    }
                }
            }
        });
    }

    function getInputs(btn) {
        var a = [];
        btn.parents('.dataform:visible').find('.saveable,:radio:checked').each(function () {
            if ($(this).attr('type') === 'checkbox' && !$(this).is(':checkbox:checked')) {
                a.push('NULL');
            } else {
                a.push($.trim($(this).val()));
            }
        });
        return a;
    }

    function validInput(a) {
        var okay = true;
        $.each(a, function (i) {
            if (a[i] === "") {
                okay = false;
            }
        });
        return okay;
    }

    function resetDataEntryForm() {
        var forms = document.getElementsByClassName('dataform');
        for (var i = 0; i < forms.length; i++) {
            forms[i].reset();
        }
        $('.dataform:visible').find('img').attr('src', '');
        $('.selections').find('li').remove();
    }

    function tableNavigation() {
        $('.table-navigation').unbind('click').on('click', function () {
            var step = 100;
            if (tableCursor < 0) {
                tableCursor = 0;
            } else {
                tableCursor = ($(this).attr('direction') === 'forward' ? (tableCursor + step) : (tableCursor - step));
            }
            if (tableCursor >= 0) {
                post('{"act":"navigate", "fr":"' + linkid + '", "data": ' + '{"start":"' + tableCursor + '"}' + '}', function (data) {
                    datam.html(data);
                    standardfunctions.allFunctions();
                });
            }
        });
    }

    function timeSpinner() {
        $.widget("ui.timespinner", $.ui.spinner, {
            options: {
                // seconds
                step: 60 * 1000 * 60

            },
            _parse: function (value) {
                if (typeof value === "string") {
                    // already a timestamp
                    if (Number(value) == value) {
                        return Number(value);
                    }
                    return +Globalize.parseDate(value);
                }
                return value;
            },
            _format: function (value) {
                return Globalize.format(new Date(value), "t");
            }
        });
        Globalize.culture('de-DE');
        $(".timespinner").timespinner();
    }

    function wakesPopover() {
        var me = null;
        $('.wakes-popover').on('mouseenter', function () {
            me = $(this);
            me.next('.description-popover').removeClass('hidden');
        }).on('mouseleave', function () {
            me.next('.description-popover').addClass('hidden');
        });
    }

    function post(data, callback, dataType) {
        var act = JSON.parse(data).act;
        $.ajax({
            dataType: dataType === undefined ? 'html' : dataType,
            beforeSend: function () {
                if (act === 'search') {
                    $('.ajxloader').removeClass('hidden');
                } else {
                    loading.removeClass('hidden');
                }
            },
            data: {data: data},
            success: function (resp) {
                if (act === 'search') {
                    $('.ajxloader').addClass('hidden');
                } else {
                    loading.addClass('hidden');
                }
                callback(resp);
            }
        });
    }

    function postForm(formdata, callback) {
        $('.dataform:visible').unbind('submit').on('submit', function () {
            $.ajax({
                data: formdata,
                dataType: "html",
                success: function (msg) {
                    callback(msg);
                },
                cache: false,
                processData: false,
                contentType: false
            });
            return false;
        });
    }


    {
        var link_id = '#';
        var sender = null;
        var item = null;
        var item_id = null;
        var entity_id = null;

        //get modules for that particular user

        function sortable(id, data) {
            entity_id = id;
            $('#smods').html(data.sys_mods);
            $('#umods').html(data.user_mods);
            $('ul.connected-sortable').each(function () {
                link_id += $(this).attr('id') + ', #';
            });
            link_id = link_id.substring(0, link_id.length - 3);

            $(link_id).sortable({
                connectWith: '.connected-sortable',
                receive: function (event, ui) {
                    sender = ui.sender.attr('id');
                },
                stop: function (event, ui) {
                    item = ui.item[0];
                    item_id = item.getAttribute('id');
                    if (sender === 'sys_mods') {
                        addModule();

                    } else if (sender === 'user_mods') {
                        removeModule();
                    }
                    sender = null;
                }
            }).disableSelection();
        }

        function addModule() {
            post('{"act":"save", "fr":"' + linkid + '","data": ' + '{"act":"amod","data":"' + [entity_id, item_id] + '"}' + '}', function (data) {
                console.info(data);
            });
        }

        function removeModule() {
            post('{"act":"save", "fr":"' + linkid + '","data": ' + '{"act":"rmod","data":"' + [entity_id, item_id] + '"}' + '}', function (data) {
                console.info(data);
            });
        }
    }

    {
        var chartProperties = {type: 'column'};
        var serie = null;
        function drawChart() {
            var graphTypeTitle = $('#graph-type-title');
            var chartContainer = [$('#container0'), $('#container1'), $('#container2'), $('#container3'), $('#container4')];
            var stockGaugesContainer = $('.stock-gauges');
            stockGaugesContainer.html('');
            graphTypeTitle.text(chartProperties.type);
            post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"1"}' + '}', function (data) {
                serie = JSON.parse(data);
                draw(serie.day, serie.sold, chartContainer[0], serie.caption);
            }, 'json');
            post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"2"}' + '}', function (data) {
                serie = JSON.parse(data);
                draw(serie.shift, serie.sold, chartContainer[1], serie.caption);
            }, 'json');
            post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"3"}' + '}', function (data) {
                serie = JSON.parse(data);
                draw(serie.product, serie.sold, chartContainer[2], serie.caption);
            }, 'json');
            post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"4"}' + '}', function (data) {
                serie = JSON.parse(data);
                draw(serie.day, serie.amount, chartContainer[3], serie.caption);
            }, 'json');
            post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"0"}' + '}', function (data) {
                serie = JSON.parse(data);
                drawPie(serie.data, chartContainer[4], serie.caption);
            }, 'json');
            //gauges
            post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"5"}' + '}', function (data) {
                serie = JSON.parse(data);
                $(serie.data).each(function (i) {
                    var containerId = 'container' + serie.data[i][0].replace(/ /g, '');
                    stockGaugesContainer.append('<p><span><strong>' + serie.data[i][0] + '</strong></span>&nbsp;|&nbsp;<span>' + serie.caption.scaleValue + '</span></p><div id="' + containerId + '" style="height:300px;"></div>');
                    drawGauge($('#' + containerId), serie.data[i][1], serie.caption, serie.data[i][2]);
                });
            }, 'json');
            //end gauges
            $('.graph-type>li').unbind('click').bind('click', function () {
                var text = $(this).text();
                chartProperties.type = text.toLowerCase();
                graphTypeTitle.text(chartProperties.type);
                //Refresh
                navBarFunctions.refresh();
            });
            function draw(xData, yData, container, caption) {
                container.highcharts({
                    chart: {
                        type: chartProperties.type,
                        marginRight: 130,
                        marginBottom: 25
                    },
                    title: {
                        text: caption.title,
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'Source: Oak Platform',
                        x: -20
                    },
                    xAxis: {
                        categories: xData
                    },
                    yAxis: {
                        title: {
                            text: caption.ytitle
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        valueSuffix: caption.valueSuffix
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -10,
                        y: 100,
                        borderWidth: 0
                    },
                    series: [{
                            name: caption.seriesname,
                            data: yData
                        }]
                });
            }
            function drawPie(serie, container, caption) {
                container.highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: caption.title
                    },
                    subtitle: {
                        text: 'Source: Oak Platform'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage}%</b>',
                        percentageDecimals: -2,
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(2) + ' %';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: caption.seireName,
                            data:
                                    serie
                        }]
                });
            }
            function drawGauge(container, value, caption, max) {
                var min = 0;
                container.highcharts({
                    chart: {
                        type: 'gauge',
                        plotBackgroundColor: null,
                        plotBackgroundImage: null,
                        plotBorderWidth: 0,
                        plotShadow: true
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 'Source: Oak Platform'
                    },
                    pane: {
                        startAngle: -150,
                        endAngle: 150,
                        background: [{
                                backgroundColor: {
                                    linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                                    stops: [
                                        [0, '#FFF'],
                                        [1, '#333']
                                    ]
                                },
                                borderWidth: 0,
                                outerRadius: '109%'
                            }, {
                                backgroundColor: {
                                    linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                                    stops: [
                                        [0, '#333'],
                                        [1, '#FFF']
                                    ]
                                },
                                borderWidth: 1,
                                outerRadius: '107%'
                            }, {
                                // default background
                            }, {
                                backgroundColor: '#DDD',
                                borderWidth: 0,
                                outerRadius: '105%',
                                innerRadius: '103%'
                            }]
                    },
                    // the value axis
                    yAxis: {
                        min: min,
                        max: max,
                        minorTickInterval: 'auto',
                        minorTickWidth: 1,
                        minorTickLength: 10,
                        minorTickPosition: 'inside',
                        minorTickColor: '#666',
                        tickPixelInterval: 30,
                        tickWidth: 2,
                        tickPosition: 'inside',
                        tickLength: 10,
                        tickColor: '#666',
                        labels: {
                            step: 2,
                            rotation: 'auto'
                        },
                        title: {
                            text: caption.valueSuffix
                        },
                        plotBands: [{
                                from: 0,
                                to: parseInt(0.2 * max),
                                color: '#DF5353' // red
                            }, {
                                from: parseInt(0.2 * max),
                                to: parseInt(0.9 * max),
                                color: '#DDDF0D' // yellow
                            }, {
                                from: parseInt(0.9 * max),
                                to: max,
                                color: '#55BF3B' // green
                            }]
                    },
                    series: [{
                            name: 'Remaining',
                            data: [value],
                            tooltip: {
                                valueSuffix: caption.valueSuffix
                            }
                        }]

                });
            }
        }
    }
});
