<?php
session_start();
if (isset($_SESSION['user_name'])) {
    if ($_SESSION['user_group'] == 'p') {
        header("Location: head/");
    } else {
        header("Location: station/");
    }
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Oak</title>
        <link rel=icon href="images/icons/favi.jpeg" type="image/x-icon" />
        <link rel="stylesheet" href="libs/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="libs/css/bootstrap-responsive.css"/>
        <link rel="stylesheet" href="libs/css/jquery-ui-1.10.4.custom.min.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <style>
            body {
                background: #f0f0f0;
                height: 90vh;
            }
            #oaklogo {
                font-size: larger;
            }

            #oaklogo:hover {
                color: #ffa067;
            }
            #myCarousel {
                width: 7in;
                height: 2.5in;
            }
            .carousel-control{
                position: absolute;
                height: 2em;
                width: 3em;
                border-radius: 15px;
                top: 40%;
                margin-left: 1em;
                margin-right: 1em;
            }
            .item {
                position: absolute;
                padding-left: 12em;
                padding-right: 10em;
                padding-top:4%; 
            }
            .article-text{
                font-size: 1.5em;
            }
            .heading1 {
                font-weight: bolder;
                font-size: 2em;
            }

            .heading2 {
                font-weight: bolder;
                font-size: 1.5em;
            }
            .inputboxholder{
                margin-bottom: 15px;
            }
        </style>

    </head>
    <body>

        <div id="wrap" class="container">

            <div class="navbar">
                <div class="navbar-inner navbar-fixed-top">
                    <ul class="nav nav-pills" style="padding:6px;">
                        <li class="navbar-brand" style="font-size: xx-large;">
                            <a href="javascript:void(0);"><div style="transform: rotate(-20deg);">Oak</div></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row-fluid">
                <div class="col-sm-7 col-md-7 col-lg-7">
                    <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="active item">
                                <div class="hero-unit">
                                    <div class="heading1">
                                        Oak Fuel Station Software
                                    </div>
                                    <p class="article-text">
                                        The best Fuel management system yet.
                                    </p>
                                    <p>
                                        <a class="btn btn-primary btn-large"> Learn more </a>
                                    </p>
                                </div>
                                <!--
                                end of hero unit
                                -->
                            </div>
                            <div class="item">
                                <div class="hero-unit">
                                    <div class="heading1">
                                        Amazingly Accessible.
                                    </div>
                                    <p class="article-text">
                                        Keep in Touch With your business wherever you go
                                    </p>
                                    <p>
                                        <a class="btn btn-primary btn-large"> Learn more </a>
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="hero-unit">
                                    <div class="heading1">
                                        Cost Effective.
                                    </div>
                                    <p class="article-text">
                                        No Hardware costs, Maintenance Costs or Storage Costs
                                        Involved.
                                    </p>
                                    <p>
                                        <a class="btn btn-primary btn-large"> Learn more </a>
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="hero-unit">
                                    <div class="heading1">
                                        No Losses
                                    </div>
                                    <p class="article-text">
                                        All your data is securely Stored and optimized on secure
                                        Servers
                                    </p>
                                    <p>
                                        <a class="btn btn-primary btn-large"> Learn more </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--end of carousel inner-->
                        <!-- Carousel nav -->
                        <a class="carousel-control left" href="#myCarousel"data-slide="prev">&lsaquo;</a>
                        <a class="carousel-control right"href="#myCarousel" data-slide="next">&rsaquo;</a>
                    </div>
                    <!--
                    end of carousel
                    -->
                </div>
                <div class="col-sm-5 col-md-5 col-lg-5">
                    <!--login-->
                    <div id="login">
                        <div style="border-bottom: gray dashed thin; padding-bottom: 10px;">
                            <ul class="nav nav-pills">
                                <li class="inputtitle">Login</li>
                            </ul>
                        </div>
                        <div class="inputholder">
                            <form class="dataform" act="login">
                                <div class="container-fluid">
                                    <br />
                                    <div class="inputboxholder signup hidden">
                                        <div style="padding: 5px;">
                                            <label class="inputlabel">Proriator</label>
                                            <input type="text" class="saveable editable inputbox" itemref="text" placeholder="Proriator" />
                                        </div>
                                    </div>
                                    <div class="inputboxholder signup hidden">
                                        <div style="padding: 5px;">
                                            <label class="inputlabel">Stations' name</label>
                                            <input type="text" class="saveable editable inputbox" itemref="text" placeholder="stations name" />
                                        </div>
                                    </div> 
                                    <div class="inputboxholder signup hidden">
                                        <div style="padding: 5px;">
                                            <label class="inputlabel">Contact phone</label>
                                            <input type="text" class="saveable editable inputbox" itemref="text" placeholder="contact mobile no" />
                                        </div>
                                    </div>
                                    <div class="inputboxholder signup hidden">
                                        <div style="padding: 5px;">
                                            <label class="inputlabel">Email address</label>
                                            <input type="text" class="saveable editable inputbox" itemref="text" placeholder="email address" />
                                        </div>
                                    </div>
                                    <div class="inputboxholder">
                                        <div style="padding: 5px;">
                                            <label class="inputlabel">Username</label>
                                            <input type="text" class="saveable editable inputbox" itemref="text" placeholder="username" />
                                        </div>
                                    </div> 
                                    <div class="inputboxholder">
                                        <div style="padding: 5px;">
                                            <label class="inputlabel">Password</label>
                                            <input type="password" class="saveable editable inputbox" itemref="text" placeholder="password" />
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <div class="save" style="padding: 5px;" class="save">
                                            <input type="submit" class="save btn btn-primary btn-lg" act="login" value="Login" />
                                        </div>            
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end login-->
                </div>
            </div>

            <div class="navbar-inner navbar-fixed-bottom">
                <ul class="nav nav-pills" style="padding:10px; font-size: small;">
                    <li class="link">
                        <a class="signsup" href="javascript:void(0);">Signup</a>
                    </li>
                    <li class="link">
                        <a href="javascript:void(0);">About</a>
                    </li>
                    <li class="link">
                        <a href="javascript:void(0);">Feedback</a>
                    </li>
                </ul>
            </div>

        </div>

        <script src="libs/js/jquery-2.1.0.min.js"></script>
        <script src="libs/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="libs/js/bootstrap.min.js"></script>
        <script src="libs/js/highcharts.js"></script>
        <script>
            jQuery(document).ready(function ($) {

                function ajaxSetup(url, type) {
                    $.ajaxSetup({
                        url: (url !== undefined ? url : 'control/control.php'),
                        type: (type !== undefined ? type : 'POST'),
                        dataType: (type !== undefined ? type : 'html')
                    });
                }

                function ajaxReset() {
                    $.ajaxSetup({
                        url: 'control/control.php',
                        type: 'POST',
                        dataType: 'html'
                    });
                }

                (function init() {
                    ajaxSetup();
                }());

                $('.carousel:visible').carousel();
                saveBtnClicked();

                function saveBtnClicked() {
                    $('.save:visible').unbind('click').on('click', function (e) {
                        var a = getInputs();
                        var act = $('.dataform').attr('act');
                        var formdata = new FormData($('.dataform:visible')[0]);
                        if (validInput(a)) {
                            var data = '{"act":"' + act + '","data":"' + a + '"}';
                            formdata.append("data", data);
                            postForm(formdata, function (resp) {
                                window.location = resp;
                            });
                        } else {
                            console.info('not okay');
                        }
                    });
                    return false;
                }

                $('.signsup:visible').unbind('clcik').bind('click', function () {
                    $('.signup').is(':hidden') ? showSignup($(this)) : showLogin($(this));
                });

                function getInputs() {
                    var a = [];
                    $('.inputholder:visible').find('.saveable:visible,:radio:visible:checked').each(function () {
                        if ($(this).attr('type') === 'checkbox' && !$(this).is(':checkbox:checked')) {
                            a.push('NULL');
                        } else {
                            a.push($.trim($(this).val()));
                        }
                    });
                    return a;
                }

                function validInput(a) {
                    var okay = true;
                    $.each(a, function (i) {
                        if (a[i] === "") {
                            okay = false;
                        }
                    });
                    return okay;
                }

                function postForm(formdata, callback) {
                    $('.dataform:visible').unbind('submit').on('submit', function () {
                        $.ajax({
                            data: formdata,
                            dataType: "html",
                            success: function (msg) {
                                callback(msg);
                            },
                            cache: false,
                            processData: false,
                            contentType: false
                        });
                        return false;
                    });
                }

                function showLogin(elem) {
                    $('.dataform').attr('act', 'login');
                    elem.text('Signup');
                    $('.signup').addClass('hidden');
                    $('.inputtitle:visible').text('Login');
                    $('input.save').val('Login');
                    $('input:text,input:password').val('');
                }

                function showSignup(elem) {
                    $('.dataform').attr('act', 'signup');
                    elem.text('Login');
                    $('.signup').removeClass('hidden');
                    $('.inputtitle:visible').text('Signup');
                    $('input.save').val('Signup');
                    $('input:text,input:password').val('');
                }

            });

        </script>
    </body>
</html>
