<?php

/**
 * Trashes an item from the database
 *
 * @author nelson
 */
class MoreRecordsFetcher {

    var $datum = NULL;

    public function __construct($fr, $data) {
        $this->datum = new Datam($fr);
        $funcs = ['prep' => 'fetchPrepayments'];

        $this->$funcs[$fr]($data['id']);
    }

    private function fetchPrepayments($id) {
        echo $this->datum->_getPrepays($id);
    }

}
