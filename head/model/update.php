<?php

include_once '../../db/DbUpdateEntry.php';
include_once '../../helper/uploadFile.php';

/**
 * Saves the data
 * 
 * Invokes datum to display results
 *
 * @author nelson
 */
class Update {

    var $dbUpdate = NULL;
    var $uploadFile = NULL;
    var $datum = NULL;

    public function __construct($fr, $id, $data) {
        $this->dbUpdate = new DbUpdateEntry();
        $this->uploadFile = new FileUpload($fr);
        $this->datum = new Datam($fr);

        $funcs = ['empl' => 'updateEmployee', 'shif' => 'updateShift', 'prod' => 'updateProduct',
            'pump' => 'updatePump', 'pric' => 'updatePrice', 'stor' => 'updateStorage',
            'expe' => 'updateExpense', 'wage' => 'updateWage', 'duty' => 'updateDuty',
            'cred' => 'updateCreditor', 'debt' => 'updateDebtor', 'prec' => 'updatePrepayClient',
            'prep' => 'updatePrepay', 'admi' => 'updateStationInfo', 'cont' => 'updateUserAccount',
            'unit' => 'updateUnit'];
        $this->$funcs[$fr]($id, explode(',', $data));
    }

    private function updateEmployee($id, $data) {
        array_push($data, $id);
        $timestamp = $_SERVER['REQUEST_TIME'];
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            array_unshift($data, $this->uploadFile->uploaded_file_name);
        } else {
            //No image was uploaded nullify the image
            array_unshift($data, '');
        }
        $this->requestUpdate('employee_update_pro', $data, 'getEmployees');
    }

    private function updateShift($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('shift_update_pro', $data, 'getShifts');
    }

    private function updateProduct($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('product_update_pro', $data, 'getProducts');
    }

    private function updatePump($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('pump_update_pro', $data, 'getPumps');
    }

    private function updatePrice($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('price_update_pro', $data, 'getPrices');
    }

    private function updateStorage($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('storage_update_pro', $data, 'getStorages');
    }

    private function updateExpense($id, $data) {
        array_push($data, $id);
        $data[1] = strtotime($data[1]);
        $this->requestUpdate('expense_update_pro', $data, 'getExpenses');
    }

    private function updateWage($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('wage_update_pro', $data, 'getWages');
    }

    private function updateDuty($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('duty_update_pro', $data, 'getDuties');
    }

    private function updateCreditor($id, $data) {
        array_push($data, $id);
        $timestamp = $_SERVER['REQUEST_TIME'];
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            array_unshift($data, $this->uploadFile->uploaded_file_name);
        } else {
            //No image was uploaded nullify the image
            array_unshift($data, '');
        }
        $this->requestUpdate('creditor_update_pro', $data, 'getCreditors');
    }

    private function updateDebtor($id, $data) {
        array_push($data, $id);
        $timestamp = $_SERVER['REQUEST_TIME'];
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            array_unshift($data, $this->uploadFile->uploaded_file_name);
        } else {
            //No image was uploaded nullify the image
            array_unshift($data, '');
        }
        $this->requestUpdate('debtor_update_pro', $data, 'getdebtors');
    }

    private function updatePrepayClient($id, $data) {
        array_push($data, $id);
        $timestamp = $_SERVER['REQUEST_TIME'];
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            array_unshift($data, $this->uploadFile->uploaded_file_name);
        } else {
            //No image was uploaded nullify the image
            array_unshift($data, '');
        }
        $this->requestUpdate('prepay_client_update_pro', $data, 'getPrepayClients');
    }

    private function updatePrepay($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('prepay_update_pro', $data, 'getPrepays');
    }

    private function updateStationInfo($id, $data) {
        array_push($data, $id);
        $timestamp = $_SERVER['REQUEST_TIME'];
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            array_unshift($data, $this->uploadFile->uploaded_file_name);
        } else {
            //No image was uploaded nullify the image
            array_unshift($data, '');
        }
        $this->requestUpdate('station_info_update_pro', $data, 'getStationInfo');
    }

    private function updateUserAccount($id, $data) {
        array_push($data, $id);
        $this->requestUpdate('user_account_update_pro', $data, 'getControl');
    }
    private function requestUpdate($procedure, $data, $refreshFunction) {
//        var_dump($data);
        $updated = $this->dbUpdate->updateProcedure($procedure, $data);
        if ($updated) {
            echo $this->datum->$refreshFunction();
        } else {
            echo $this->dbUpdate->sql_error;
            echo $this->uploadFile->uploaded_file_name;
            echo FALSE;
        }
    }

}
