<?php

/**
 * Adds life to the refresh and back to all results buttons
 *
 * @author nelson
 */
class Refresh {

    var $datum = NULL;
    var $fr = NULL;

    public function __construct($fr, $data = NULL) {
        $this->datum = new Datam($fr);
        $this->fr = $fr;
        $funcs = ['empl' => 'refreshEmployees', 'shif' => 'refreshShifts',
            'prod' => 'refreshProducts', 'pump' => 'refreshPumps', 'pric' => 'refreshPrices',
            'stor' => 'refreshStorages', 'expe' => 'refreshExpenses', 'wage' => 'refreshWages',
            'duty' => 'refreshDuties', 'cred' => 'refreshCreditors', 'debt' => 'refreshDebtors',
            'prec' => 'refreshPrepayClients', 'prep' => 'refreshPrepays', 'coll' => 'refreshCollections',
            'dash' => 'refreshDashBoard', 'cont' => 'refreshControls', 'mana' => 'refreshManagers',
            'stat' => 'refreshStations'];
        $this->$funcs[$fr]($data);
    }

    private function refreshEmployees() {
        echo $this->datum->getEmployees();
    }

    private function refreshShifts() {
        echo $this->datum->getShifts();
    }

    private function refreshProducts() {
        echo $this->datum->getProducts();
    }

    private function refreshPumps() {
        echo $this->datum->getPumps();
    }

    private function refreshPrices() {
        echo $this->datum->getPrices();
    }

    private function refreshStorages() {
        echo $this->datum->getStorages();
    }

    private function refreshExpenses() {
        echo $this->datum->getExpenses();
    }

    private function refreshWages() {
        echo $this->datum->getWages();
    }

    private function refreshDuties() {
        echo $this->datum->getDuties();
    }

    private function refreshCreditors() {
        echo $this->datum->getCreditors();
    }

    private function refreshDebtors() {
        echo $this->datum->getDebtors();
    }

    private function refreshPrepayClients() {
        echo $this->datum->getPrepayClients();
    }

    private function refreshPrepays() {
        echo $this->datum->getPrepays();
    }

    private function refreshCollections() {
        echo $this->datum->getCollections();
    }

    private function refreshDashboard($data = NULL) {
        if (empty($data)) {
            new UIfileReader('../html/dashboard.html', $this->datum->getDashboard());
        } else {
            new UIfileReader('../html/dashboard.html', $this->datum->getDashboard());
        }
    }

    private function refreshManagers() {
        echo $this->datum->getManagers();
    }

    private function refreshStations() {
        echo $this->datum->getStationInfo();
    }

    private function refreshControls($data = NULL) {
        if (empty($data)) {
            echo $this->datum->getControl();
        } else {
            $output = array('sys_mods' => $this->datum->getSystemModules($data['data'][0], 1), 'user_mods' => $this->datum->getUserModules($data['data'][0]));
            echo json_encode($output, JSON_NUMERIC_CHECK);
        }
    }

}
