<?php

session_start();

/**
 * Description of datumPresent
 * 
 * Present data after receiving it from datam
 *
 * @author codethorn
 */
class DatamPresent {

    var $fr = NULL;
    var $datamHelper = NULL;
    var $dbQuery = NULL;
    var $image_path = '../uploads/';

    public function __construct($fr) {
        $this->fr = $fr;
        $this->datamHelper = new datamHelper();
        $this->dbQuery = new DbQuery();
    }

    public function presentEmployees($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Names', 'Identity No', 'Mobile No', 'Wage group', 'Duty', 'Employee since'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td class="editable hidden" itemref="text">' . $value['surname'] . '</td>'
                    . '<td class="editable hidden" itemref="text">' . $value['other_names'] . '</td>'
                    . '<td>
                    <div class="wakes-popover">' . $value['surname'] . ' ' . $value['other_names'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="tblopsholder">'
                    . '<div class="editable" itemref="text">' . $value['id_no'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['wage_group'] . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['wage_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['duty_name'] . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['duty_id'] . '</div>'
                    . '</td>'
                    . '<td>' . date('M Y', $value['date']) . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentShifts($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Alias', 'Starts at', 'Ends at'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['sfrom'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['sto'] . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentProducts($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Product name'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentReadings($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Pump No', 'Dispenses', 'Shift', 'Initial reading', 'Final reading', 'Date'], false)
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['pump_no'] . '</div></td>'
                    . '<td><div>' . $value['product_name'] . '</div></td>'
                    . '<td><div>' . $value['shift_name'] . '</div></td>'
                    . '<td><div>' . number_format($value['initial_reading'], 2) . '</div></td>'
                    . '<td><div>' . number_format($value['final_reading'], 2) . '</div></td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp'],FALSE) . '</div></td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentPrices($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Product', 'Amount (Kes)'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td>'
                    . '<div>' . $value['product_name'] . '</div>'
                    . '<div class="hidden editable" itemref="combo">' . $value['product_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentStorages($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Product', 'Capacity (Lts)'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['storage_name'] . '</div></td>'
                    . '<td>'
                    . '<div>' . $value['product_name'] . '</div>'
                    . '<div class="hidden editable" itemref="combo">' . $value['product_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['capacity'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['capacity'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentExpenses($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Expense date', 'Amount (Kes)', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['expense_name'] . '</div></td>'
                    . '<td>'
                    . '<div>' . $this->datamHelper->formatTimestamp($value['expense_date'], FALSE) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $this->datamHelper->formatTimestamp($value['expense_date'], FALSE, 'd-m-Y') . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentWages($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Wage group', 'Amount (Kes)', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['wage_group'] . '</div></td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentDuties($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Saved on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div class="editable" itemref="text">' . $value['name'] . '</div></td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentCreditors($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Description', 'Mobile No', 'Added on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover editable" itemref="text">' . $value['creditor_name'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="tblopsholder">'
                    . '<div class="editable" itemref="text">' . $value['description'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td><div>' . date('M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentDebtors($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Description', 'Mobile No', 'Added on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover editable" itemref="text">' . $value['debtor_name'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="tblopsholder">'
                    . '<div class="editable" itemref="text">' . $value['description'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td><div>' . date('M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentPrepayClients($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Description', 'Mobile No', 'Added on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover editable" itemref="text">' . $value['name'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . $this->fr . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['description'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td><div>' . date('M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentPrepays($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Prepay client', 'Remaining balance (Kes)'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr class="tr-link" id="' . $value['prepay_client_id'] . '">'
                    . '<td class="hidden">'
                    . '<div class="editable" itemref="image">uploads/prec/media/images/' . $value['profile_photo_path'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="wakes-popover editable" itemref="text">' . $value['name'] . '</div>'
                    . '<div class="hidden description-popover">'
                    . '<img class="imgpreviewer" src="uploads/prec/media/images/' . $value['profile_photo_path'] . '" />'
                    . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['prepay_client_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['total_amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['total_amount'] . '</div>'
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function _presentPrepays($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Amount (Kes)'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="hidden">'
                    . '<div class="editable" itemref="image">uploads/prec/media/images/' . $value['profile_photo_path'] . '</div>'
                    . '</td>'
                    . '<td class="hidden">'
                    . '<div class="editable" itemref="text">' . $value['name'] . '</div>'
                    . '<div class="editable" itemref="text">' . $value['prepay_client_id'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div>' . number_format($value['amount'], 2) . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['amount'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentStationInfo($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Station', 'Manager', 'Location', 'Phone No', 'Address', 'Email'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $resultset['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . 'admi' . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover editable" itemref="text">' . $value['name'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . 'admi' . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['station_manager'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['location'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['contact_phone'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['contact_address'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['contact_email'] . '</div>'
                    . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentManagers($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Station', 'Names', 'Identity No', 'Mobile No', 'Employee since'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">' . $this->image_path . 'empl' . '/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['station_name'] . '</div>'
                    . '<div class="hidden editable" itemref="text">' . $value['sid'] . '</div>'
                    . '</td>'
                    . '<td class="editable hidden" itemref="text">' . $value['surname'] . '</td>'
                    . '<td class="editable hidden" itemref="text">' . $value['other_names'] . '</td>'
                    . '<td>
                    <div class="wakes-popover">' . $value['surname'] . ' ' . $value['other_names'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="' . $this->image_path . 'empl' . '/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td>'
                    . '<div class="editable" itemref="text">' . $value['id_no'] . '</div></td>'
                    . '<td><div class="editable" itemref="text">' . $value['contact_phone'] . '</div></td>'
                    . '<td>' . date('M Y', $value['date']) . '</td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentCollections($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Attendatn', 'Shift', 'Pump', 'Product', 'Price', 'Initial', 'Final', 'Sold', 'Submited', 'Expencted', 'Difference', 'Saved'], FALSE)
                . '<tbody>';
        foreach ($resultset as $value) {
            $litres_sold = ($value['final_reading'] - $value['initial_reading']);
            $amount_difference = ($value['expected_amount'] - $value['amount']);
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td><div>' . $value['employee_name'] . '</div></td>'
                    . '<td><div>' . $value['shift_name'] . '</div></td>'
                    . '<td><div>' . $value['pump_no'] . '</div></td>'
                    . '<td><div>' . $value['product_name'] . '</div></td>'
                    . '<td><div>' . $value['pump_price_per_litre'] . '</div></td>'
                    . '<td><div>' . number_format($value['initial_reading'], 2) . '</div></td>'
                    . '<td><div>' . number_format($value['final_reading'], 2) . '</div></td>'
                    . '<td><div>' . number_format($litres_sold, 2) . '</div></td>'
                    . '<td><div>' . number_format($value['amount'], 2) . '</div></td>'
                    . '<td><div>' . number_format($value['expected_amount'], 2) . '</div></td>'
                    . '<td><div>' . number_format($amount_difference, 2) . '</div></td>'
                    . '<td><div>' . $this->datamHelper->formatTimestamp($value['save_timestamp']) . '</div></td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentDashboard($sid) {
        return [$this->storageSummary($sid), $this->stockSummary($sid), $this->saleSummary($sid), $this->salesPerShiftSummary($sid), $this->salesPerProductSummary($sid), $this->revenueSummary($sid)];
    }

    private function dashSummary($sid) {
        $a = NULL;
        $result = $this->dbQuery->runQuery("SELECT product_name,sum(capacity)capacity FROM `storage_view` where sid = {$sid} group by product_id;");
        $a.='<div class="thumbnail">';
        foreach ($result as $value) {
            $a.='<span class="label label-default"><span>' . $value['product_name'] . '</span>&nbsp;<span>' . number_format($value['capacity'], 2) . '</span></span>&nbsp;';
        }
        $a.='</div>';
        return $a;
    }

    private function storageSummary($sid) {
        $result = $this->dbQuery->runQuery("SELECT product_name,sum(capacity)capacity FROM `storage_view` where sid = {$sid} group by product_id;");
        foreach ($result as $value) {
            $dataSet[] = array($value['product_name'], ($value['capacity'] / 10000));
        }
        $output = array("draw" => [TRUE, "pie"], "caption" => ["title" => "Storage summary", "seireName" => "share"], "data" => $dataSet);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function saleSummary($sid) {
        $result = $this->dbQuery->runQuery("SELECT product_name, substring(from_unixtime(save_timestamp),1,10) as day,sum(final_reading-initial_reading) as sold FROM `fuel_collection_view` where sid = {$sid} and pid={$_SESSION['pid']} group by day order by day desc;");
        foreach ($result as $value) {
            $dataSetDay[] = array($value['day']);
            $dataSetSold[] = array($value['sold']);
        }
        $output = array("draw" => [TRUE, 'chart'], "caption" => ["title" => "Aggregate per day", "valuesuffix" => "lts", "ytitle" => "lts", "seriesname" => "Litres sold"], "xData" => $dataSetDay, "yData" => $dataSetSold);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function salesPerShiftSummary($sid) {
        $result = $this->dbQuery->runQuery("SELECT *,sum(final_reading-initial_reading) as sold FROM `fuel_collection_view` where sid = {$sid} and pid={$_SESSION['pid']} group by shift_id order by id desc");
        foreach ($result as $value) {
            $dataSetShift[] = array($value['shift_name']);
            $dataSetSold[] = array($value['sold']);
        }
        $output = array("draw" => [TRUE, 'chart'], "caption" => ["title" => "Aggragate per shift", "valuesuffix" => "lts", "ytitle" => "lts", "seriesname" => "Litres sold"], "xData" => $dataSetShift, "yData" => $dataSetSold);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function salesPerProductSummary($sid) {
        $result = $this->dbQuery->runQuery("SELECT *,sum(final_reading-initial_reading) as sold FROM `fuel_collection_view` where sid = {$sid} and pid={$_SESSION['pid']} group by product_id order by id desc");
        foreach ($result as $value) {
            $dataSetShift[] = array($value['product_name']);
            $dataSetSold[] = array($value['sold']);
        }
        $output = array("draw" => [TRUE, 'chart'], "caption" => ["title" => "Aggragate per product", "valuesuffix" => "lts", "ytitle" => "lts", "seriesname" => "Litres sold"], "xData" => $dataSetShift, "yData" => $dataSetSold);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function revenueSummary($sid) {
        $result = $this->dbQuery->runQuery("SELECT sum(amount) as amount, substring(from_unixtime(save_timestamp),1,10) as day FROM `fuel_collection_view` where sid = {$sid} and pid={$_SESSION['pid']} group by day order by day desc;");
        foreach ($result as $value) {
            $dataSetDay[] = array($value['day']);
            $dataSetAmount[] = array($value['amount']);
        }
        $output = array("draw" => [TRUE, 'chart'], "caption" => ["title" => "Aggragate per day", "valuesuffix" => "Kes", "ytitle" => "Kes", "seriesname" => "Amount collected"], "xData" => $dataSetDay, "yData" => $dataSetAmount);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    private function stockSummary($sid) {
        $scale_value = 1000;
        $result = $this->dbQuery->runQuery("SELECT * FROM `fuel_gauge_view` where sid = {$sid};");
        foreach ($result as $value) {
            $dataSet[] = [$value['product_name'], (($value['fill_up'] - $value['fetch_out']) / $scale_value), ($value['capacity'] / $scale_value)];
        }
        $output = array("draw" => [TRUE, "gauge"], "caption" => ["scaleValue" => "Scaled to {$scale_value}", "valueSuffix" => "Lts"], "data" => $dataSet);
        return json_encode($output, JSON_NUMERIC_CHECK);
    }

    public function presentUserAccounts($resultset) {
        $a = '<table class="table table-condensed table-hover">'
                . $this->datamHelper->setTableHeader(['Name', 'Enrolled on'])
                . '<tbody>';
        foreach ($resultset as $value) {
            $a.='<tr id="' . $value['id'] . '">'
                    . '<td class="editable hidden" itemref="image">uploads/empl/media/images/' . $value['profile_photo_path'] . '</td>'
                    . '<td>
                    <div class="wakes-popover editable" itemref="text">' . $value['name'] . '</div>
                    <div class="hidden editable" itemref="text">' . $value['employee_id'] . '</div>
                    <div class="hidden description-popover">
                    <img class="imgpreviewer" src="uploads/empl/media/images/' . $value['profile_photo_path'] . '" />
                    </div>
                    </td>'
                    . '<td><div>' . date('D d M Y', $value['save_timestamp']) . '</div></td>'
                    . '<td>'
                    . $this->datamHelper->setTableOperations()
                    . '</td>'
                    . '</tr>';
        }
        $a.='</tbody></table>';
        return $a;
    }

    public function presentSystemModules($resultset) {
        $a = '<ul id="sys_mods" class="connected-sortable nav nav-pills nav-stacked">';

        foreach ($resultset as $value) {
            $a.='<li id="' . $value['id'] . '" class=""><a>' . $value['activity'] . '</a></li>';
        }
        $a .='</ul>';

        return $a;
    }

    public function presentUserModules($resultset) {
        $a = '<ul id="user_mods" class="connected-sortable nav nav-pills nav-stacked">';

        foreach ($resultset as $value) {
            $a.='<li id="' . $value['module_id'] . '" class=""><a>' . $value['activity'] . '</a></li>';
        }
        $a .='</ul>';

        return $a;
    }

}
