<?php

/**
 * Description of datamHelper
 * 
 * Helper functions for datam
 *
 * @author codethorn
 */
class datamHelper {

    public function getEmptyResultSetMessage() {
        return 'There arer no entries yet';
    }

    public function setTableHeader($headers, $operations = TRUE) {
        if ($operations) {
            array_push($headers, 'Operations');
        }
        $a = '<thead style="font-weight:bold;"><tr>';
        foreach ($headers as $value) {
            $a.='<td>' . $value . '</td>';
        }
        $a.='</thead></tr>';

        return $a;
    }

    public function setTableOperations() {
        return '<div class="tblopsholder">'
                . '<div class="hidden tblopsactions pull-left">'
                . '&emsp;'
                . '<span class="edit btn btn-primary btn-sm glyphicon glyphicon-edit"></span>'
                . '&emsp;'
                . '<span class="remove btn btn-danger btn-sm glyphicon glyphicon-trash"></span>'
                . '</div>'
                . '<div class="tblops">...</div>'
                . '</div>';
    }

    /**
     * Formats a timestamp
     * if time is true it only reutns time else it returns the date
     * **/
    public function formatTimestamp($timestamp, $time = \TRUE, $format = 'D d M Y') {
        return ($time ? date($format . ' H:i:s', $timestamp) : date($format, $timestamp));
    }

}
