<?php

session_start();

//include_once '../../../db/DbQuery.php';
include_once 'datamPresent.php';
include_once 'datamHelper.php';

/**
 * Displays data from the database
 *
 * @author nelson
 */
class Datam {

    var $dbQuery = NULL;
    var $datamPresent = NULL;
    var $datamHelper = NULL;
    var $fr = NULL;

    public function __construct($fr) {
        $this->dbQuery = new DbQuery();
        $this->datamPresent = new DatamPresent($fr);
        $this->datamHelper = new datamHelper();
        $this->fr = $fr;
    }

    public function getEmployees($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('employee_view', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentEmployees($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getShifts($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('shift', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentShifts($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getProducts($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('product', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentProducts($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getReadings($start = 0, $result = NULL, $sid = NULL) {
        if ($sid == NULL) {
            $sid = $this->dbQuery->runQuery("select min(id) as sid from station_info where pid={$_SESSION['pid']};")[0]['sid'];
        }
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('fuel_collection_view', $sid, $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentReadings($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getPrices($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('price_view', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentPrices($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getStorages($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('storage_view', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentStorages($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getExpenses($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('expense', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentExpenses($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getWages($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('wage', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentWages($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getDuties($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('duty', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentDuties($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getCreditors($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('creditor', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentCreditors($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getDebtors($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('debtor', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentDebtors($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getPrepayClients($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('prepay_client', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentPrepayClients($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getPrepays($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('prepay_total_view', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentPrepays($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function _getPrepays($id, $start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByColumn('prepay_view', $id, 'prepay_client_id');
        }
        if (!empty($result)) {
            $a = $this->datamPresent->_presentPrepays($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getStationInfo($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('station_info', $_SESSION['pid'], $start);
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentStationInfo($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getManagers($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('manager_view', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentManagers($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getCollections($start = 0, $result = NULL) {
        $a = 'NULL';
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('fuel_collection_view', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentCollections($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getDashboard($sid = NULL) {
        return $this->datamPresent->presentDashboard($sid);
    }

    public function getControl($start = 0, $result = NULL) {
        $a = NULL;
        if (empty($result)) {
            $result = $this->dbQuery->getByPid('user_account_view', $_SESSION['pid'], $start); //Pass the business id
        }
        if (!empty($result)) {
            $a = $this->datamPresent->presentUserAccounts($result);
        } else {
            $a = $this->datamHelper->getEmptyResultSetMessage();
        }
        return $a;
    }

    public function getSystemModules($user_id, $sid) {
        $a = NULL;
        $result = $this->dbQuery->selectProcedure('sys_user_control_get_system_modules_pro', [$user_id, $sid]);
        if (!empty($result)) {
            $a = $this->datamPresent->presentSystemModules($result);
        } else {
            $a = '<ul id="sys_mods" class="connected-sortable" style="margin-left:-1px; margin-top:5px;">'
                    . '<li style="height:2px;"></li>'
                    . '</ul>';
        }
        return $a;
    }

    public function getUserModules($user_id) {
        $a = NULL;
        $result = $this->dbQuery->getByColumn('sys_user_control_view', $user_id, 'employee_id');
        if (!empty($result)) {
            $a = $this->datamPresent->presentUserModules($result);
        } else {
            $a = '<ul id="user_mods" class="connected-sortable" style="margin-left:-1px; margin-top:5px;">'
                    . '<li style="height:10px;"></li>'
                    . '</ul>';
        }
        return $a;
    }
}
