<?php

include_once '../../db/DbRemoveEntry.php';

/**
 * Trashes an item from the database
 *
 * @author nelson
 */
class Remove {

    var $dbremove = NULL;

    public function __construct($fr, $data) {
        $this->dbremove = new DbRemoveEntry();
        $funcs = ['empl' => ['trashEmployee', 'employee'], 'shif' => ['trashShift', 'shift'],
            'prod' => ['trashProduct', 'product'], 'pump' => ['trashPump', 'pump'],
            'pric' => ['trashPrice', 'price'], 'stor' => ['trashStorage', 'storage'],
            'expe' => ['trashExpense', 'expense'], 'wage' => ['trashWage', 'wage'],
            'duty' => ['trashDuty', 'duty'],'cred' => ['trashCreditor', 'creditor'],
            'debt' => ['trashDebtor', 'debtor'],'prec' => ['trashPrepayClient', 'prepay_client'],
            'prep' => ['trashPrepay', 'prepay']];
        
        $this->$funcs[$fr][0]($funcs[$fr][1], $data['id']);
    }

    private function trashEmployee($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashShift($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashProduct($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashPump($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashPrice($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashStorage($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashExpense($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashWage($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }
    
    private function trashDuty($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }
    
    private function trashCreditor($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }
    
    private function trashDebtor($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }
    
    private function trashPrepayClient($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }
    
    private function trashPrepay($table, $id) {
        $trashed = $this->dbremove->trashRecord($table, $id);
        $this->trashedResponse($trashed);
    }

    private function trashedResponse($trashed) {
        if ($trashed) {
            echo TRUE;
        } else {
            echo FALSE;
        }
    }

}
