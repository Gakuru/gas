<?php

/**
 * Acts more like the search control
 * 
 * Invokes datum to display results
 *
 * @author nelson
 */
class Search {

    var $dbQuery = NULL;
    var $datum = NULL;
    var $query = NULL;
    var $profile_photo_path = NULL;
    var $fr = NULL;

    public function __construct($fr, $data) {
        $this->dbQuery = new DbQuery();
        $this->datum = new Datam($fr);
        $this->query = new Query($fr);
        $this->fr = $fr;
        $this->profile_photo_path = '../uploads/' . ($fr == 'cont' ? 'empl' : $fr == 'stat' ? 'admi' : $fr) . '/media/images/';

        switch ($data['act']) {
            case 'search':
                $funcs = ['empl' => 'suggestEmployees', 'shif' => 'suggestShifts',
                    'prod' => 'suggestProducts', 'pump' => 'suggestPumps',
                    'pric' => 'suggestPrices', 'stor' => 'suggestStorages',
                    'expe' => 'suggestExpenses', 'wage' => 'suggestWages', 'duty' => 'suggestDuties',
                    'cred' => 'suggestCreditors', 'debt' => 'suggestDebtors', 'prec' => 'suggestPrepayClients',
                    'prep' => 'suggestPrepays', 'cont' => 'suggestControls', 'coll' => 'suggestCollections',
                    'stat' => 'suggestStations'];
                $this->$funcs[$fr]($data['data'], $data['data-restrict']);
                break;
            case 'present':
                $funcs = ['empl' => 'presentEmployee', 'shif' => 'presentShift',
                    'prod' => 'presentProduct', 'pump' => 'presentPump',
                    'pric' => 'presentPrices', 'stor' => 'presentStorage',
                    'expe' => 'presentExpense', 'wage' => 'presentWage', 'duty' => 'presentDuty',
                    'cred' => 'presentCreditor', 'debt' => 'presentDebtor', 'prec' => 'presentPrepayClient',
                    'prep' => 'presentPrepay', 'cont' => 'presentControl', 'coll' => 'presentCollection',
                    'stat' => 'presentStation', 'dash' => 'PresentDashboard', 'pumr' => 'PresentReadings'];
                $this->$funcs[$fr]($data['data']);
                break;
        }
    }

    private function suggestEmployees($param) {
        echo '<h4>Searching in employees for <i>' . $param . '</i></h4>';
        $result = $this->dbQuery->getProcedure('employee_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . '<img class="preview-image img-circle" style="width:80px;" src="' . $this->profile_photo_path . '' . $value['profile_photo_path'] . '" />'
                . '</span>'
                . '<span style="margin-left:10px;" class="extractable">'
                . $value['surname'] . ' ' . $value['other_names']
                . '</span>'
                . '<span>'
                . '&nbsp;&raquo;&nbsp;'
                . $value['duty_name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentEmployee($id) {
        $result = $this->dbQuery->getById('employee_view', $id);
        echo $this->datum->getEmployees(0, $result);
    }

    private function suggestShifts($param) {
        echo 'searching in shifts for ' . $param;
        $result = $this->dbQuery->getProcedure('shift_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div class="extractable">'
                . '<span>'
                . $value['name']
                . '</span>'
                . '<span>'
                . '&nbsp;&raquo;&nbsp;'
                . '</span>'
                . '<span>'
                . $value['sfrom'] . '&nbsp;to&nbsp;' . $value['sto']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentShift($id) {
        $result = $this->dbQuery->getById('shift', $id);
        echo $this->datum->getShifts(0, $result);
    }

    private function suggestProducts($param) {
        echo 'searching in products for ' . $param;
        $result = $this->dbQuery->getProcedure('product_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div class="extractable">'
                . '<span>'
                . $value['name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentProduct($id) {
        $result = $this->dbQuery->getById('product', $id);
        echo $this->datum->getProducts(0, $result);
    }

    private function suggestPumps($param) {
        echo 'searching in pumps for ' . $param;
        $result = $this->dbQuery->getProcedure('pump_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['pump_id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span class="extractable">'
                . $value['pump_no']
                . '</span>'
                . '<span class="extractable">'
                . '&nbsp;&raquo;&nbsp;'
                . '</span>'
                . '<span class="extractable">'
                . $value['product_name']
                . '</span>'
                . '<span class="extractable">'
                . '&nbsp; pump'
                . '</span>'
                . '<span class="extractable">'
                . '&nbsp;&raquo;&nbsp;'
                . '</span>'
                . '<span class="extractable">'
                . $value['amount']
                . '</span>'
                . '<span class="hidden useable" toref="toref">'
                . '&nbsp;&raquo;&nbsp;'
                . $value['final_reading']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentPump($id) {
        $result = $this->dbQuery->getById('pump_view', $id);
        echo $this->datum->getPumps(0, $result);
    }

    private function suggestPrices($param) {
        echo 'searching in prices for ' . $param;
        $result = $this->dbQuery->getProcedure('price_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div class="extractable">'
                . '<span>'
                . $value['product_name']
                . '</span>'
                . '<span>'
                . '&nbsp;@&nbsp;'
                . '</span>'
                . '<span>'
                . number_format($value['amount'], 2)
                . '</span>'
                . '<span>'
                . '&nbsp; /= Kes'
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentPrices($id) {
        $result = $this->dbQuery->getById('price_view', $id);
        echo $this->datum->getPrices(0, $result);
    }

    private function suggestStorages($param) {
        echo 'searching in storage for ' . $param;
        $result = $this->dbQuery->getProcedure('storage_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . $value['storage_name']
                . '</span>'
                . '<span>'
                . '&nbsp;&raquo;&nbsp;'
                . '</span>'
                . '<span>'
                . $value['product_name']
                . '</span>'
                . '<span>'
                . '</span>'
                . '<span>'
                . '&nbsp;'
                . number_format($value['capacity'], 2)
                . '&nbsp; Lts'
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentStorage($id) {
        $result = $this->dbQuery->getById('storage_view', $id);
        echo $this->datum->getStorages(0, $result);
    }

    private function suggestExpenses($param) {
        echo 'searching in expenses for ' . $param;
        $result = $this->dbQuery->getProcedure('expense_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . $value['expense_name']
                . '</span>'
                . '<span>'
                . '&nbsp;&raquo&nbsp;'
                . date('D d M Y', $value['expense_date'])
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentExpense($id) {
        $result = $this->dbQuery->getById('expense', $id);
        echo $this->datum->getExpenses(0, $result);
    }

    private function suggestWages($param) {
        echo 'searching in wages for ' . $param;
        $result = $this->dbQuery->getProcedure('wage_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span class="extractable">'
                . $value['wage_group']
                . '</span>'
                . '<span>'
                . '&nbsp;@&nbsp;'
                . '</span>'
                . '<span>'
                . number_format($value['amount'], 2)
                . '</span>'
                . '<span>'
                . '&nbsp; /= Kes'
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentWage($id) {
        $result = $this->dbQuery->getById('wage', $id);
        echo $this->datum->getWages(0, $result);
    }

    private function suggestDuties($param) {
        echo 'searching in duties for ' . $param;
        $result = $this->dbQuery->getProcedure('duty_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span class="extractable">'
                . $value['name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentDuty($id) {
        $result = $this->dbQuery->getById('duty', $id);
        echo $this->datum->getDuties(0, $result);
    }

    private function suggestCreditors($param) {
        echo '<h4>Searching in creditors for <i>' . $param . '</i></h4>';
        $result = $this->dbQuery->getProcedure('creditor_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . '<img class="preview-image img-circle" style="width:80px;" src="' . $this->profile_photo_path . '' . $value['profile_photo_path'] . '" />'
                . '</span>'
                . '<span style="margin-left:10px;" class="extractable">'
                . $value['creditor_name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentCreditor($id) {
        $result = $this->dbQuery->getById('creditor', $id);
        echo $this->datum->getCreditors(0, $result);
    }

    private function suggestDebtors($param) {
        echo '<h4>Searching in debtors for <i>' . $param . '</i></h4>';
        $result = $this->dbQuery->getProcedure('debtor_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . '<img class="preview-image img-circle" style="width:80px;" src="' . $this->profile_photo_path . '' . $value['profile_photo_path'] . '" />'
                . '</span>'
                . '<span style="margin-left:10px;" class="extractable">'
                . $value['debtor_name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentDebtor($id) {
        $result = $this->dbQuery->getById('debtor', $id);
        echo $this->datum->getDebtors(0, $result);
    }

    private function suggestPrepayClients($param) {
        echo '<h4>Searching in prepay clients for <i>' . $param . '</i></h4>';
        $result = $this->dbQuery->getProcedure('prepay_client_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . '<img class="preview-image img-circle" style="width:80px;" src="' . $this->profile_photo_path . '' . $value['profile_photo_path'] . '" />'
                . '</span>'
                . '<span style="margin-left:10px;" class="extractable">'
                . $value['name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentPrepayClient($id) {
        $result = $this->dbQuery->getById('prepay_client', $id);
        echo $this->datum->getPrepayClientsnts(0, $result);
    }

    private function suggestPrepays($param) {
        echo 'searching in prepayments for ' . $param;
        $result = $this->dbQuery->getProcedure('prepay_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['prepay_client_id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . $value['name']
                . '</span>'
                . '<span>'
                . '&nbsp;&raquo&nbsp;'
                . '</span>'
                . '<span>'
                . number_format($value['total_amount'], 2)
                . '</span>'
                . '<span>'
                . '&nbsp; /= Kes'
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentPrepay($id) {
        $result = $this->dbQuery->getById('prepay_total_view', $id);
        echo $this->datum->getPrepays(0, $result);
    }

    private function suggestControls($param) {
        echo 'searching in user accounts for ' . $param;
        $result = $this->dbQuery->getProcedure('user_account_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . '<img class="preview-image img-circle" style="width:80px;" src="' . $this->profile_photo_path . '' . $value['profile_photo_path'] . '" />'
                . '</span>'
                . '<span style="margin-left:10px;" class="extractable">'
                . $value['name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentControl($id) {
        $result = $this->dbQuery->getById('user_account_view', $id);
        echo $this->datum->getControl(0, $result);
    }

    private function suggestCollections($param) {
        echo 'searching in fuel collections for ' . $param;
        $result = $this->dbQuery->getProcedure('collection_search_pro', [$param, 1]);
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['employee_id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span style="margin-left:10px;" class="extractable">'
                . $value['employee_name']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            echo 'There are no results for <i>' . $param . '</i>';
        }
        echo '</ul>';
    }

    private function presentCollection($id) {
        $result = $this->dbQuery->getByColumn('fuel_collection_view', $id, 'employee_id');
        echo $this->datum->getCollections(0, $result);
    }

    private function suggestStations($param, $data_restrict = NULL) {
        $this->query->createQuery(['select', 0]);
        echo '<h4>Searching in stations for <i>' . $param . '</i></h4>';
        if ($data_restrict == NULL) {
            $result = $this->dbQuery->getProcedure('station_info_search_pro', [$param, $_SESSION['pid']]);
        } else {
            $result = $this->query->createQuery(['select', 0, $param]);
        }
        echo '<hr />';
        echo '<ul class="nav nav-pills nav-stacked">';
        if (!empty($result)) {
            foreach ($result as $value) {
                echo '<li class="sresults" id="' . $value['id'] . '">'
                . '<a href="javascript:void(0);">'
                . '<div>'
                . '<span>'
                . '<img class="preview-image img-polaroid" style="width:80px;" src="' . $this->profile_photo_path . '' . $value['profile_photo_path'] . '" />'
                . '</span>'
                . '<span style="margin-left:10px;" class="extractable">'
                . $value['name']
                . '</span>'
                . '<span>'
                . '&nbsp;&raquo;&nbsp;'
                . $value['location']
                . '</span>'
                . '</div>'
                . '</a>'
                . '</li>';
            }
        } else {
            if ($data_restrict != NULL && empty($result)) {
                echo 'Seems like all stations have managers';
            } else if ($data_restrict != NULL) {
                echo 'There are no results for <i>' . $param . '</i>';
            } else {
                echo 'There are no results for <i>' . $param . '</i>';
            }
        }
        echo '</ul>';
    }

    private function presentStation($id) {
        $result = $this->dbQuery->getById('station_info', $id);
        echo $this->datum->getStationInfo(0, $result);
    }

    private function PresentDashboard($station_id) {
//        print_r($this->datum->getDashboard($station_id));
//        new UIReset($this->fr, [$station_id]);
        echo json_encode(["data" => $this->datum->getDashboard($station_id)], JSON_NUMERIC_CHECK);
    }

    private function PresentReadings($station_id) {
        echo json_encode(["data" => $this->datum->getReadings(0,NULL,$station_id)], JSON_NUMERIC_CHECK);
    }

}
