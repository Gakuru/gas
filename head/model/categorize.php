<?php

/**
 * Handles data that requires to be categorized
 *
 * @author nelson
 */
class Categorize {

    var $datum = NULL;

    public function __construct($fr, $data) {
        $this->datum = new Datam($fr);
        $funcs = ['pric' => 'categorizePrice'];
        $this->$funcs[$fr]($data);
    }

    private function categorizePrice($data) {
        echo $this->datum->getPrices(TRUE, $data);
    }

}
