<?php

session_start();
include_once '../../db/DbInsert.php';
include_once '../../db/DbQuery.php';
include_once '../../db/DbUpdateEntry.php';
include_once '../../helper/uploadFile.php';

/**
 * Saves the data
 * 
 * Invokes datum to display results
 *
 * @author nelson
 */
class Save {

    var $dbInsert = NULL;
    var $uploadFile = NULL;
    var $datum = NULL;
    var $dbQuery = NULL;
    var $dbUpdate = NULL;

    public function __construct($fr, $data) {
        $this->dbInsert = new DbInsert();
        $this->uploadFile = new FileUpload($fr);
        $this->datum = new Datam($fr);
        $this->dbQuery = new DbQuery();
        $this->dbUpdate = new DbUpdateEntry();

        $funcs = ['empl' => 'saveEmployee', 'shif' => 'saveShift', 'prod' => 'saveProduct',
            'pump' => 'savePump', 'pric' => 'savePrice', 'stor' => 'saveStorage',
            'expe' => 'saveExpense', 'wage' => 'saveWage', 'duty' => 'saveDuty',
            'cred' => 'saveCreditor', 'debt' => 'saveDebtor', 'prec' => 'savePrepayClient',
            'prep' => 'savePrepay', 'coll' => 'saveCollection', 'cont' => 'saveUserAccount',
            'acon' => 'saveUserModule', 'stat' => 'saveStation', 'mana' => 'saveManager'];
        if (array_key_exists('act', $data)) {
            $this->$funcs[$fr](explode(',', $data['data']), $data['act']);
        } else {
            $this->$funcs[$fr](explode(',', $data));
        }
    }

    private function saveEmployee($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_saveEmployee';
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            $this->$save_function($data, $timestamp);
        } else {
            //Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _saveEmployee($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('employee_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$data[3]}", "{$data[4]}", "{$data[5]}"
            , "{$this->uploadFile->uploaded_file_name}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getEmployees');
    }

    private function saveShift($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('shift_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getShifts');
    }

    private function saveProduct($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('product_insert_pro', ["{$data[0]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getProducts');
    }

    private function savePump($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('pump_insert_pro', ["{$data[0]}", "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getPumps');
    }

    private function savePrice($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('price_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getPrices');
    }

    private function saveStorage($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('storage_insert_pro', ["{$data[0]}", "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getStorages');
    }

    private function saveExpense($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $data[1] = strtotime($data[1]);
        $saved = $this->dbInsert->insertProcedure('expense_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getExpenses');
    }

    private function saveWage($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('wage_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getWages');
    }

    private function saveDuty($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('duty_insert_pro', ["{$data[0]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getDuties');
    }

    private function saveCreditor($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_saveCreditor';
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            $this->$save_function($data, $timestamp);
        } else {
            //Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _saveCreditor($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('creditor_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$this->uploadFile->uploaded_file_name}"
            , "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getCreditors');
    }

    private function saveDebtor($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_saveDebtor';
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            $this->$save_function($data, $timestamp);
        } else {
            //Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _saveDebtor($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('debtor_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$this->uploadFile->uploaded_file_name}"
            , "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getDebtors');
    }

    private function savePrepayClient($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_savePrepayClient';
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            $this->$save_function($data, $timestamp);
        } else {
            //Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _savePrepayClient($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('prepay_client_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$this->uploadFile->uploaded_file_name}"
            , "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getPrepayClients');
    }

    private function savePrepay($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('prepay_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getPrepays');
    }

    private function saveCollection($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        //Get price , product_id and the price_id      
        $result = $this->dbQuery->getByColumn('pump_product_price_view', $data[2], 'pump_id')[0];
        array_splice($data, 3, 0, [$result['id'], $result['price_id']]);
        array_splice($data, 7, 1);
        $expected_amount = ($result['amount'] * ($data[6] - $data[5]));
        array_push($data, $expected_amount, $timestamp);
        $saved = $this->dbInsert->insertProcedure('collection_insert_pro', $data);
        $updated = $this->dbUpdate->updateProcedure('collection_update_final_pump_reading_pro', [$data[6], $data[2]]);
        echo $this->dbUpdate->sql_error;
        if ($updated && $saved) {
            $this->refreshRecordSet($saved, 'getCollections');
        }
    }

    private function saveUserAccount($data, $act = NULL) {
        if ($act == NULL) {
            $timestamp = $_SERVER['REQUEST_TIME'];
            $saved = $this->dbInsert->insertProcedure('user_account_insert_pro', ["{$data[0]}", "{$timestamp}"]);
            $this->refreshRecordSet($saved, 'getControl');
        } else {
            switch ($act) {
                case 'amod':
                    $this->saveUserModule($data);
                    break;
            }
        }
    }

    private function saveUserModule($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('sys_user_control_insert_pro', ["{$data[0]}", "{$data[1]}", "{$timestamp}"]);
    }

    private function saveStation($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_saveStation';
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            $this->$save_function($data, $timestamp);
        } else {
            //Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _saveStation($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('station_info_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$data[3]}", "{$data[4]}"
            , "{$this->uploadFile->uploaded_file_name}", "{$timestamp}"]);
        $this->refreshRecordSet($saved, 'getStationInfo');
    }

    private function saveManager($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $save_function = '_saveManager';
        if ($this->uploadFile->uploadedFile($timestamp)) {
            //Save other data
            $this->$save_function($data, $timestamp);
        } else {
            //Image failed to upload nullify the image and save other data
            array_shift($data, '');
            $this->$save_function($data, $timestamp);
        }
    }

    private function _saveManager($data, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('manager_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$data[3]}", "{$data[4]}"
            , "{$this->uploadFile->uploaded_file_name}", TRUE, "{$timestamp}"]);
        if ($saved) {
            $last_manager_insert_id = $this->dbQuery->getByColumn('employee', $timestamp, 'date')[0]['id'];
            if ($this->createUser($data, $last_manager_insert_id, $timestamp)) {
                if ($this->dbInsert->executeQuery("update station_info set employee_id={$last_manager_insert_id} where id={$data[0]}")) {
                    $this->refreshRecordSet($saved, 'getManagers');
                }
            }
        }
    }

    private function createUser($data, $last_inset_id, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('login_insert_pro', ["{$data[0]}"
            , "{$last_inset_id}", "{$data[3]}", "{$data[3]}", 'a', "{$timestamp}"]);
        if ($saved) {
            $saved = $this->dbInsert->insertProcedure('sys_user_control_insert_pro', ["{$data[0]}"
                , "{$last_inset_id}", 22, "{$timestamp}"]);
            if ($saved) {
                $saved = $this->dbInsert->insertProcedure('user_account_insert_pro', ["{$data[0]}"
                    , "{$last_inset_id}", "{$timestamp}"]);
            }
        }
        return $saved;
    }

    private function refreshRecordSet($saved, $func) {
        if ($saved) {
            //Refresh            
            echo $this->datum->$func();
        } else {
            echo $this->dbInsert->sql_error;
            echo 0;
        }
    }

}
