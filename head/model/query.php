<?php

session_start();

/**
 * Specific queries
 *
 * @author codethorn
 */
class Query {

    var $dbQuery = NULL;
    var $dbInsert = NULL;
    var $dbUpdate = NULL;
    var $fr = NULL;

    public function __construct($fr) {
        $this->dbQuery = new DbQuery();
        $this->dbInsert = new DbInsert();
        $this->dbUpdate = new DbUpdateEntry();
        $this->fr = $fr;
    }

    public function createQuery($data) {
        $queries = ["select" => ["stat" => ["SELECT * FROM `station_info` where CONCAT(name,' ',location,' ',contact_address,' ',contact_phone,' ',contact_email) like CONCAT('%','{$data[2]}','%') and `pid`={$_SESSION['pid']} AND `employee_id`=0 AND `deleted`=0"]]];
        return $this->runQuery($data[0], $queries[$data[0]][$this->fr][$data[1]]);
    }

    private function runQuery($type, $query) {
        switch ($type) {
            case "select":
                return $this->dbQuery->runQuery($query);
        }
    }

}
