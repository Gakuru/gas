$(function ($) {
    var r = $('div#r'), datam = null, editing = false, loading = $('div#loading'), linkid = null, tableCursor = 0, dash_link = false;
    var standardfunctions =
            {
                allFunctions: function () {
                    this.standardFunctions().navBarFunctions().datamFunctions().imageUpload().tableOperationsClicked().trLinkClicked().graphTypeClicked();
                    $('.datepicker').datepicker({
                        dateFormat: 'dd-mm-yy'
                    });
                }, standardFunctions: function () {
                    dialogFunctions.standardfunctions();
                    saveBtnClicked();
                    btnCheckBox();
                    btnRadioOption();
                    return this;
                }, navBarFunctions: function () {
                    navBarFunctions.standardNavBarFunctions();
                    return this;
                }, datamFunctions: function () {
                    window.setTimeout(function () {
                        editBtnClicked();
                        removeBtnClicked();
                        tableNavigation();
                        wakesPopover();
                        if (dash_link) {
                            drawChart();
                        }
                    }, 0, false);
                    return this;
                }, imageUpload: function () {
                    $('.imgupload').unbind('click').on('click', function (e) {
                        var me = $(this);
                        $('#myfile').trigger('click');
                        $('#myfile').change(function (e) {
                            var reader = new FileReader(),
                                    files = e.dataTransfer ? e.dataTransfer.files : e.target.files,
                                    i = 0;
                            reader.onload = onFileLoad;
                            while (files[i])
                                reader.readAsDataURL(files[i++]);
                        });
                        function onFileLoad(e) {
                            var data = e.target.result;
                            me.attr("src", data);
                        }
                        e.preventDefault();
                    });
                    return this;
                },
                tableOperationsClicked: function () {
                    $('.tblops').unbind('click').on('click', function (e) {
                        var me = $(this);
                        var parent = me.parents('tr:eq(0)');
                        var opsactions = parent.find('.tblopsactions');
                        if (!me.hasClass('opsactive')) {
                            $('.tblops').removeClass('opsactive');
                            $('.tblopsactions:visible').addClass('hidden');
                            me.addClass('opsactive');
                            opsactions.removeClass('hidden');
                            standardfunctions.datamFunctions();
                        } else {
                            me.removeClass('opsactive');
                            opsactions.addClass('hidden');
                        }
                        e.preventDefault();
                    });
                    return this;
                },
                trLinkClicked: function () {
                    $('.tr-link').unbind('click').on('click', function (e) {
                        var id = $(this).attr('id');
                        var me = $(this);
                        var siblings = me.siblings();
                        var colspan = me.children().length;
                        var siblings_hidden = siblings.is(':hidden');
                        var more_records = '<tr class="more_records_holder"><td style="text-align:left;" colspan="' + colspan + '"><div class="more_records" style="margin-left:-15px;">records</div></td></tr>';
                        if (!siblings_hidden) {
                            siblings.hide();
                            //on hidding siblings show more records
                            post('{"act":"more", "fr":"' + linkid + '", "data": ' + '{"id":"' + id + '"}' + '}', function (data) {
                                $('.more_records').html(data);
                                standardfunctions.allFunctions();
                            });
                            $(more_records).insertAfter(me);
                        } else {
                            siblings.show();
                            //on showing siblings remove more records
                            $('.more_records_holder').remove();
                        }
                        e.preventDefault();
                    });
                    return this;
                },
                graphTypeClicked: function () {
                    $('.graph-type>li').unbind('click').on('click', function (e) {
                        var me = $(this);
                        $('#graph-type-title').text(me.text());
                        e.preventDefault();
                    });
                    return this;
                }
            };
    var dialogFunctions = {
        standardfunctions: function () {
            this.dialogMinimizeBtnClicked().dialogCloseBtnClicked();
            return this;
        },
        dialogMinimizeBtnClicked: function () {
            $('.dminimize').unbind('click').on('click', function () {
                $('.inputholder').toggle();
            });
            return this;
        },
        dialogCloseBtnClicked: function () {
            $('.dclose').unbind('click').on('click', function () {
                $('.inputdiv').addClass('hidden');
            });
            return this;
        }
    };
    var navBarFunctions = {
        standardNavBarFunctions: function () {
            this.newBtnClicked().refreshBtnClicked().search().backToResultsBtnClicked().filler();
        },
        newBtnClicked: function () {
            $('.new').unbind('click').on('click', function () {
                $('.save').removeClass('hidden');
                $('.update').addClass('hidden');
                resetDataEntryForm();
                $('.inputdiv').removeClass('hidden');
                $('.inputholder').show();
            });
            return this;
        },
        refreshBtnClicked: function () {
            $('.refresh').unbind('click').on('click', function () {
                navBarFunctions.refresh();
            });
            return this;
        },
        backToResultsBtnClicked: function () {
            $('.backtoresults').unbind('click').on('click', function () {
                //Same functionality as refresh
                navBarFunctions.refresh();
                $('.searchbox').val('');
                $(this).addClass('hidden');
            });
            return this;
        },
        refresh: function () {
            post('{"act":"refresh", "fr":"' + linkid + '"}', function (resp) {
                datam.html(resp);
                standardfunctions.allFunctions();
            });
        },
        search: function () {
            var param = null;
            var selections = [];
            var searchbox = null;
            var suggest = null;
            var linkto = null;
            var saveableInput = null;
            var me = null;
            var id = null;
            $('.searchable').unbind('keyup').on('keyup', function () {
                searchbox = $(this);
                suggest = $(this).parents('div').next('.suggest');
                linkto = ($(this).attr('itemid') === undefined ? linkid : $(this).attr('itemid'));
                saveableInput = searchbox.next('input:hidden.saveable');
                param = $.trim(this.value);
                if (param.length > 0) {
                    suggest.removeClass('hidden');
                    var data_restrict = (searchbox.attr('data-restrict') === undefined ? '' : searchbox.attr('data-restrict'));
                    post('{"act":"search", "fr":"' + linkto + '", "data": ' + '{"act":"search","data":"' + param + '","data-restrict":"' + data_restrict + '"}' + '}', function (suggestresp) {
                        suggest.html(suggestresp);
                        $('li.sresults').unbind('click').on('click', function () {
                            me = $(this);
                            id = me.attr('id');
                            if (searchbox.hasClass('searchable-inline')) {
                                search_inline();
                            } else {
                                post('{"act":"search", "fr":"' + linkid + '", "data": ' + '{"act":"present","data":"' + id + '"}' + '}', function (tableresp) {
                                    suggest.html('').addClass('hidden');
                                    $('.backtoresults').removeClass('hidden');
                                    datam.html(tableresp);
                                    standardfunctions.allFunctions();
                                });
                            }
                        });
                    });
                } else {
                    suggest.addClass('hidden').html('');
                    if (!searchbox.hasClass('searchable-inline')) {
                        navBarFunctions.refresh();
                    }
                }
            });
            {
                function search_inline() {
                    if (searchbox.hasClass('multiple-input')) {
                        $('.selections').append('<li class="label label-default">' + me.find('.extractable').text() + '&nbsp;<span class="cancel-selection">&#10007;</span></li>');
                        searchbox.val('');
                        selections.push(id);
                        saveableInput.val(selections);
                        var vals = saveableInput.val();
                        saveableInput.val(vals.replace(/,/g, ':'));
                        $('.cancel-selection').unbind('click').on('click', function () {
                            var parent = $(this).parents('li');
                            var parentId = parent.index();
                            parent.remove();
                            selections.splice(parentId, 1);
                            saveableInput.val(selections);
                            var vals = saveableInput.val();
                            saveableInput.val(vals.replace(/,/g, ':'));
                        });
                    } else if (searchbox.hasClass('sets-datam')) {
                        post('{"act":"refresh", "fr":"' + linkid + '","data": ' + '{"act":"redraw","data":"' + [id] + '"}' + '}', function (resp) {
                            if (dash_link) {
                                console.info(resp);
                            } else {
                                sortable(id, $.parseJSON(resp)); //Pass the entity id
                            }
                        });
                    } else if (searchbox.hasClass('insearch')) {
                        post('{"act":"insearch", "fr":"' + linkid + '","data": ' + '{"act":"present","data":"' + [id] + '"}' + '}', function (resp) {
                            if (dash_link) {
                                drawChart(false, resp.data);
                            } else {
                                datam.html(resp.data);
                            }
                        }, 'json');
                    } else {
                        var useable = (me.find('.useable').text() !== '' ? true : false);
                        if (useable) {
                            var useableVal = me.text();
                            useableVal = useableVal.replace(/ » /g, ':').split(':');
                            console.info(useableVal);
                            var toref = [];
                            var toberef = [];
                            me.find('span').each(function () {
                                if ($(this).attr('toref')) {
                                    var text = $(this).text();
                                    toref.push(text.replace(/ » /g, ''));
                                }
                            });
                            $('input:text').each(function () {
                                if ($(this).attr('toref')) {
                                    toberef = $(this);
                                }
                            });
                            toberef.each(function (i) {
                                $(this).val(toref[i]);
                            });
                            toref = [];
                            toberef = [];
                        }
                        saveableInput.val(id);
                    }
                    if (searchbox.hasClass('refsimage')) {
                        console.info(me.find('.preview-image').attr('src'));
                        $('.imgpreview:visible').attr('src', me.find('.preview-image:visible').attr('src'));
                    }
                    searchbox.val(me.find('.extractable').text());
                    suggest.html('').addClass('hidden');
                }
            }

            return this;
        },
        filler: function () {
            $('.filler').unbind('keyup').on('keyup', function () {
                console.info('gets from a paired extractable');
                //Get value from an extractable with that same paired value.
                console.info($(this).attr('pairedvalue'));
                var pairedvalue = $(this).attr('pairedvalue');
                var value = $(this).val();
                var output = null;
                $('input.extractable').each(function (i) {
                    if ($(this).attr('pairedvalue') === pairedvalue) {
                        console.info($(this).val());
                        output = eval(value + $(this).attr('computation') + $(this).val());
                        console.info(output);
                    }
                });
                $('input.fillable').each(function (i) {
                    if ($(this).attr('pairedvalue') === pairedvalue) {
                        $(this).val(output);
                    }
                });
            });
        }
    };
    function btnCheckBox() {
        $('.btncheckbox').unbind('click').on('click', function () {
            var text = $(this).text();
            if ($(this).hasClass('active')) {
                $(this).removeClass('btn-info');
                $(this).text($(this).attr('data-toggle-text'));
                $(this).attr('data-toggle-text', text);
            } else {
                $(this).addClass('btn-info');
                $(this).text($(this).attr('data-toggle-text'));
                $(this).attr('data-toggle-text', text);
            }
        });
    }

    function btnRadioOption() {
        $('.btnradio').unbind('click').on('click', function (e) {
            var me = $(this);
            var value = me.attr('value');
            if (value === 'all') {
                $('.refresh').trigger('click');
            } else {
                post('{"act":"categorize", "fr":"' + linkid + '","data":"' + value + '"}', function (resp) {
                    datam.html(resp);
                    standardfunctions.allFunctions();
                });
            }
            e.preventDefault();
        });
    }

    function ajaxSetup(url, type) {
        $.ajaxSetup({
            url: (url !== undefined ? url : 'control/control.php'),
            type: (type !== undefined ? type : 'POST'),
            dataType: (type !== undefined ? type : 'html')
        });
    }

    /**
     * Restore ajax settings to default
     * **/
    function ajaxReset() {
        $.ajaxSetup({
            url: 'control/control.php',
            type: 'POST',
            dataType: 'html'
        });
    }

    (function init() {
        ajaxSetup();
    }());
    (function fire() {
        navlinks();
        $('.linksleft:first').trigger('click');
    }());
    function navlinks() {
        $('.links').unbind('click').on('click', function (e) {
            $('.links').removeClass('bactive');
            $(this).addClass('bactive');
            linkid = this.id;
            dash_link = linkid === 'dash' ? true : false;
            post('{"act": "ui", "fr": "' + linkid + '"}', function (data) {
                r.html(data);
                datam = $('#datum');
                standardfunctions.allFunctions();
            });
            e.preventDefault();
        });
    }

    function editBtnClicked() {
        $('.edit').unbind('click').on('click', function (e) {
            var me = $(this);
            var parent = me.parents('tr:eq(0)');
            var id = parent.attr('id');
            var a = [];
            editing = true;
            if (editing) {
                $('.inputdiv').removeClass('hidden');
                $('.save').addClass('hidden');
                $('.update').removeClass('hidden');
                getEditValues(parent);
                a = getInputs();
                if (validInput(a)) {
                    updateBtnClicked(id, a);
                } else {
                    console.info('not okay');
                }
            }
            e.preventDefault();
        });
    }

    function removeBtnClicked() {
        $('.remove').unbind('click').on('click', function () {
            var me = $(this);
            var parent = me.parents('tr');
            var id = parent.attr('id');
            post('{"act":"remove", "fr":"' + linkid + '", "data": ' + '{"id":"' + id + '"}' + '}', function (resp) {
                if (resp) {
                    parent.remove();
                }
            });
        });
    }

    function saveBtnClicked() {
        $('.save').unbind('click').on('click', function (e) {
            var a = getInputs();
            if (validInput(a)) {
                $(this).unbind('click').on('click', function (e) {
                    var formdata = new FormData($('.dataform')[0]);
                    a = getInputs();
                    var data = '{"act":"save","fr":"' + linkid + '","data":"' + a + '"}';
                    formdata.append("data", data);
                    postForm(formdata, function (resp) {
                        console.info(resp);
                        if (resp) {
                            resetDataEntryForm();
                            datam.html(resp);
                            standardfunctions.allFunctions();
                        } else {
                            console.info('there was an error saving the data, please try again later');
                        }
                    });
                });
            } else {
                console.info('not okay');
            }
            e.preventDefault();
        });
    }

    function updateBtnClicked(id, a) {
        $('.update').unbind('click').on('click', function (e) {
            $(this).unbind('click').on('click', function (e) {
                var formdata = new FormData($('.dataform')[0]);
                a = getInputs();
                var data = '{"act":"update","fr":"' + linkid + '","id":"' + id + '","data":"' + a + '"}';
                formdata.append("data", data);
                postForm(formdata, function (resp) {
                    if (resp.length > 1) {
                        resetDataEntryForm();
                        datam.html(resp);
                        standardfunctions.allFunctions();
                    } else {
                        console.info('there was an error saving the data, please try again later');
                    }
                });
            });
            e.preventDefault();
        });
    }

    function getEditValues(parent) {
        var a = [];
        var b = [];
        parent.find('.editable').each(function () {
            a.push($(this).text());
            b.push($(this).attr('itemref'));
        });
        bindEditValues(a, b);
    }

    function bindEditValues(editVal, editRef) {
        $('.inputholder').find('.editable').each(function (i) {
            var me = $(this);
            console.info(i, me.attr('itemref'), editRef[i]);
            if (me.attr('itemref') === editRef[i]) {
                if (me.attr('itemref') === 'image') {
                    me.attr('src', editVal[i]);
                } else if (me.attr('itemref') === 'text') {
                    me.val(editVal[i]);
                } else if (me.attr('itemref') === 'combo') {
                    me.children('option').each(function (j) {
                        if (j > 0 && editVal[i].localeCompare($(this).val()) === 0) {
                            $(this).attr('selected', 'selected');
                        }
                    });
                } else if (me.attr('itemref') === 'radio') {
                    var name = $(this).attr('name');
                    $("input[name=" + name + "][value='" + editVal[i] + "']").prop("checked", true);
                } else if (me.attr('itemref') === 'checkbox') {
                    if (editVal[i] === 'yes') {
                        me.prop("checked", true);
                    } else {
                        me.prop("checked", false);
                    }
                }
            }
        });
    }

    function getInputs() {
        var a = [];
        $('.inputholder').find('.saveable,:radio:checked').each(function () {
            if ($(this).attr('type') === 'checkbox' && !$(this).is(':checkbox:checked')) {
                a.push('NULL');
            } else {
                a.push($(this).val());
            }
        });
        return a;
    }

    function validInput(a) {
        var okay = true;
        $.each(a, function (i) {
            if (a[i] === "") {
                okay = false;
            }
        });
        return okay;
    }

    function resetDataEntryForm() {
        var forms = document.getElementsByClassName('dataform');
        for (var i = 0; i < forms.length; i++) {
            forms[i].reset();
        }
        $('.dataform:visible').find('img').attr('src', '');
        $('.selections').find('li').remove();
    }

    function tableNavigation() {
        $('.table-navigation').unbind('click').on('click', function () {
            if (tableCursor < 0) {
                tableCursor = 0;
            } else {
                tableCursor = ($(this).attr('direction') == 'forward' ? (tableCursor + 8) : (tableCursor - 8));
            }
            if (tableCursor >= 0) {
                post('{"act":"navigate", "fr":"' + linkid + '", "data": ' + '{"start":"' + tableCursor + '"}' + '}', function (data) {
                    datam.html(data);
                    standardfunctions.allFunctions();
                });
            }
        });
    }

    function wakesPopover() {
        var me = null;
        $('.wakes-popover').on('mouseenter', function () {
            me = $(this);
            me.next('.description-popover').removeClass('hidden');
        }).on('mouseleave', function () {
            me.next('.description-popover').addClass('hidden');
        });
    }

    function post(data, callback, dataType) {
        var act = JSON.parse(data).act;
        $.ajax({
            dataType: dataType === undefined ? 'html' : dataType,
            beforeSend: function () {
                if (act === 'search') {
                    $('.ajxloader').removeClass('hidden');
                } else {
                    loading.removeClass('hidden');
                }
            },
            data: {data: data},
            success: function (resp) {
                if (act === 'search') {
                    $('.ajxloader').addClass('hidden');
                } else {
                    loading.addClass('hidden');
                }
                callback(resp);
            }
        });
    }

    function postForm(formdata, callback) {
        $('.dataform').unbind('submit').on('submit', function () {
            $.ajax({
                data: formdata,
                dataType: "html",
                success: function (msg) {
                    callback(msg);
                },
                cache: false,
                processData: false,
                contentType: false
            });
            return false;
        });
    }


    {
        var link_id = '#';
        var sender = null;
        var item = null;
        var item_id = null;
        var entity_id = null;
        //get modules for that particular user

        function sortable(id, data) {
            if (dash_link) {
                console.info(data);
            } else {
                entity_id = id;
                $('#smods').html(data.sys_mods);
                $('#umods').html(data.user_mods);
                $('ul.connected-sortable').each(function () {
                    link_id += $(this).attr('id') + ', #';
                });
                link_id = link_id.substring(0, link_id.length - 3);
                $(link_id).sortable({
                    connectWith: '.connected-sortable',
                    receive: function (event, ui) {
                        sender = ui.sender.attr('id');
                    },
                    stop: function (event, ui) {
                        item = ui.item[0];
                        item_id = item.getAttribute('id');
                        if (sender === 'sys_mods') {
                            addModule();
                        } else if (sender === 'user_mods') {
                            removeModule();
                        }
                        sender = null;
                    }
                }).disableSelection();
            }
        }

        function addModule() {
            post('{"act":"save", "fr":"' + linkid + '","data": ' + '{"act":"amod","data":"' + [entity_id, item_id] + '"}' + '}', function (data) {
                console.info(data);
            });
        }

        function removeModule() {
            post('{"act":"save", "fr":"' + linkid + '","data": ' + '{"act":"rem","data":"' + [item_id] + '"}' + '}', function (data) {
                console.info(data);
            });
        }
    }

    {
        var chartProperties = {type: 'column'};
        var serie = null;
        function drawChart(toPost, data) {
            var graphTypeTitle = $('#graph-type-title');
            var chartContainer = [$('#container0'), $('#container1'), $('#container2'), $('#container3')];
            var gaugesContainer = $('.gauge');
            graphTypeTitle.text(chartProperties.type);
            if (toPost) {
                post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"1"}' + '}', function (data) {
                    serie = JSON.parse(data);
                    draw(serie.day, serie.sold, chartContainer[0], serie.caption);
                }, 'json');
                post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"2"}' + '}', function (data) {
                    serie = JSON.parse(data);
                    draw(serie.shift, serie.sold, chartContainer[1], serie.caption);
                }, 'json');
                post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"3"}' + '}', function (data) {
                    serie = JSON.parse(data);
                    draw(serie.product, serie.sold, chartContainer[2], serie.caption);
                }, 'json');
                post('{"act":"chart", "fr":"' + linkid + '","data":' + '{"act":"4"}' + '}', function (data) {
                    serie = JSON.parse(data);
                    draw(serie.day, serie.amount, chartContainer[3], serie.caption);
                }, 'json');
            } else {
                try {
                    chartContainer = [];
                    $('.chartcontainer').each(function (i) {
                        var containerId = $(this).attr('id', '#container' + i);
                        chartContainer.push(containerId);
                        serie = JSON.parse(data[i]);
                        if (serie.draw[1] === 'pie') {
                            drawPie(serie.data, chartContainer[i], serie.caption);
                        } else if (serie.draw[1] === 'gauge') {
                            $(serie.data).each(function (j) {
                                var contId = 'container' + serie.data[j][0].replace(/ /g, '');
                                chartContainer[i].append('<p><span><strong>' + serie.data[j][0] + '</strong></span>&nbsp;|&nbsp;<span>' + serie.caption.scaleValue + '</span></p><div id="' + contId + '" style="height:300px;"></div>');
                                $('#' + contId).html(serie.data[j][1] + ' ' + serie.caption + ' ' + serie.data[j][2]);
//                                drawGauge($('#' + contId), serie.data[j][1], serie.caption, serie.data[j][2]);
                            });
                        } else {
                            draw(serie.xData, serie.yData, chartContainer[i], serie.caption);
                        }
                    });
                } catch (e) {
                    console.warn(e);
                }
            }
            $('.graph-type>li').unbind('click').bind('click', function () {
                var text = $(this).text();
                chartProperties.type = text.toLowerCase();
                graphTypeTitle.text(chartProperties.type);
                //Refresh
                navBarFunctions.refresh();
            });

            function draw(xData, yData, container, caption) {
                container.highcharts({
                    chart: {
                        type: chartProperties.type,
                        marginRight: 130,
                        marginBottom: 25
                    },
                    title: {
                        text: caption.title,
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'Source: Oak Platform',
                        x: -20
                    },
                    xAxis: {
                        categories: xData
                    },
                    yAxis: {
                        title: {
                            text: caption.ytitle
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        valueSuffix: caption.valuesuffix
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -10,
                        y: 100,
                        borderWidth: 0
                    },
                    series: [{
                            name: caption.seriesname,
                            data: yData
                        }]
                });
            }

            function drawPie(serie, container, caption) {
                container.highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: caption.title
                    },
                    subtitle: {
                        text: 'Source: Oak Platform'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage}%</b>',
                        percentageDecimals: -2,
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(2) + ' %';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: caption.seireName,
                            data:
                                    serie
                        }]
                });
            }
            function drawGauge(container, value, caption, max) {
                max !== undefined ? max : 100;
                var min = 0;
                console.info(container, value, caption, max);
                container.highcharts({
                    chart: {
                        type: 'gauge',
                        plotBackgroundColor: null,
                        plotBackgroundImage: null,
                        plotBorderWidth: 0,
                        plotShadow: true
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 'Source: Oak Platform'
                    },
                    pane: {
                        startAngle: -150,
                        endAngle: 150,
                        background: [{
                                backgroundColor: {
                                    linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                                    stops: [
                                        [0, '#FFF'],
                                        [1, '#333']
                                    ]
                                },
                                borderWidth: 0,
                                outerRadius: '109%'
                            }, {
                                backgroundColor: {
                                    linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                                    stops: [
                                        [0, '#333'],
                                        [1, '#FFF']
                                    ]
                                },
                                borderWidth: 1,
                                outerRadius: '107%'
                            }, {
                                // default background
                            }, {
                                backgroundColor: '#DDD',
                                borderWidth: 0,
                                outerRadius: '105%',
                                innerRadius: '103%'
                            }]
                    },
                    // the value axis
                    yAxis: {
                        min: min,
                        max: max,
                        minorTickInterval: 'auto',
                        minorTickWidth: 1,
                        minorTickLength: 10,
                        minorTickPosition: 'inside',
                        minorTickColor: '#666',
                        tickPixelInterval: 30,
                        tickWidth: 2,
                        tickPosition: 'inside',
                        tickLength: 10,
                        tickColor: '#666',
                        labels: {
                            step: 2,
                            rotation: 'auto'
                        },
                        title: {
                            text: caption.valueSuffix
                        },
                        plotBands: [{
                                from: 0,
                                to: parseInt(0.1 * max),
                                color: '#DF5353' // red
                            }, {
                                from: parseInt(0.1 * max),
                                to: parseInt(0.8 * max),
                                color: '#DDDF0D' // yellow
                            }, {
                                from: parseInt(0.8 * max),
                                to: max,
                                color: '#55BF3B' // green
                            }]
                    },
                    series: [{
                            name: 'Remaining',
                            data: [value],
                            tooltip: {
                                valueSuffix: caption.valueSuffix
                            }
                        }]

                });
            }
        }
    }
});
