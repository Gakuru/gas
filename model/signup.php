<?php

session_start();

/**
 * Description of login
 *
 * @author codethorn
 */
class Signup {

    var $dbQuery = NULL;
    var $dbInsert = NULL;

    public function __construct($data) {
        $this->dbQuery = new DbQuery();
        $this->dbInsert = new DbInsert();

        $_data = explode(',', $data);
        if ($_data[0] != NULL) {
            $this->signup($_data);
        } else {
            echo 'empty';
        }
    }

    private function signup($data) {
        $timestamp = $_SERVER['REQUEST_TIME'];
        $saved = $this->dbInsert->insertProcedure('parent_insert_pro', ["{$data[0]}"
            , "{$data[1]}", "{$data[2]}", "{$data[3]}", "{$timestamp}"]);
        if ($saved) {
            if ($this->createUser($data, $this->dbQuery->getByColumn('parent', $timestamp, 'save_timestamp')[0]['id'], $timestamp)) {
                echo '/gas/head/';
            }
        }
    }

    private function createUser($data, $last_inset_id, $timestamp) {
        $saved = $this->dbInsert->insertProcedure('login_insert_pro', ["{$last_inset_id}"
            , "{$last_inset_id}", "{$last_inset_id}", "{$data[4]}", "{$data[5]}", 'p', "{$timestamp}"]);
        $_SESSION['user_name'] = $data[4];
        $_SESSION['user_id'] = $last_inset_id;
        $_SESSION['user_group'] = 'p';
        $_SESSION['pid'] = $last_inset_id;
        $_SESSION['sid'] = $last_inset_id;
        return $saved;
    }

}
