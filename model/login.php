<?php

/**
 * Description of login
 *
 * @author codethorn
 */
class login {

    var $dbQuery = NULL;
    var $result = NULL;

    public function __construct($data) {
        $this->dbQuery = new DbQuery();
        $this->login(explode(',', $data));
    }

    private function login($data) {
        $this->AuthParent($data);
    }

    private function AuthParent($data) {
        $this->result = $this->dbQuery->runQuery("select * from login_parent_view where username='{$data[0]}' and password='{$data[1]}';")[0];
        if (!empty($this->result)) {
            $this->setSessions('p');
        } else {
            $this->AuthUser($data);
        }
    }

    private function AuthUser($data) {
        $this->result = $this->dbQuery->runQuery("select * from login_user_view where username='{$data[0]}' and password='{$data[1]}';")[0];
        if (!empty($this->result)) {
            $this->setSessions($this->result['user_group']);
        } else {
            echo '0';
        }
    }

    private function setSessions($userGroup) {
        switch ($userGroup) {
            case 'p':
                $this->setParentSessions();
                break;
            case 'a':
                $this->setUserSessions();
                break;
        }
    }

    private function setParentSessions() {
        $_SESSION['user_name'] = $this->result['username'];
        $_SESSION['user_id'] = $this->result['pid'];
        $_SESSION['pid'] = $this->result['pid'];
        $_SESSION['user_group'] = $this->result['p'];
        $_SESSION['image'] = $this->result['profile_photo_path'];
        echo 'head/';
    }

    private function setUserSessions() {
        $_SESSION['user_name'] = $this->result['user_name'];
        $_SESSION['user_id'] = $this->result['employee_id'];
        $_SESSION['pid'] = $this->result['pid'];
        $_SESSION['sid'] = $this->result['sid'];
        $_SESSION['user_group'] = $this->result['user_group'];
        $_SESSION['image'] = $this->result['profile_photo_path'];
        $_SESSION['station_name'] = $this->result['station_name'];
        echo 'station/';
    }

    private function Redirect($userGroup) {
        echo $_SESSION['user_group'];
//        switch ($userGroup) {
//            case 'p':
//                $this->redirectParent();
//                break;
//            case 'a':
//                $this->redirectUser();
//                break;
//        }
    }

    private function redirectParent() {
        echo 'head/';
    }

    private function redirectUser() {
        echo 'station/';
    }

}
