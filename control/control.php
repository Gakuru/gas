<?php

session_start();
include_once '../model/login.php';
include_once '../model/signup.php';
include_once '../db/DbQuery.php';
include_once '../db/DbInsert.php';

ini_set('display_errors', 0);

/**
 * Description of control
 *
 * @author nelson
 */
class control {

    public function __construct() {
        $data = json_decode(filter_input(INPUT_POST, 'data'), TRUE);
        switch ($data['act']) {
            case 'login':
                new login($data['data']);
                break;
            case 'signup':
                new Signup($data['data']);
                break;
            default:
                echo 'unimplemented';
                break;
        }
    }

}

new control();
