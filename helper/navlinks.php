<?php

require_once '../db/DbQuery.php';

function dbQuery() {
    return new DbQuery();
}

function getModules() {
    $result = dbQuery()->runQuery("select * from sys_user_control_view where employee_id={$_SESSION['user_id']} order by sort asc;");
    echo dbQuery()->sql_error;
    return $result;
}

function setLeftLinks() {
    $result = getModules();
    $a = NULL;
    foreach ($result as $value) {
        if (boolval($value['published']) && $value['place'] == 'acc') {
            $a.='<li class="linksleft links" id="' . $value['aid'] . '"><a href="javascript:void(0);">' . $value['activity'] . '</a></li>';
        }
    }
    return $a;
}

function setNavLinks() {
    $result = getModules();
    $a = NULL;
    foreach ($result as $value) {
        if (boolval($value['published']) && $value['place'] == 'nav') {
            $a.='<li class="navlinks dropdown">'
                    . '<a class="dropdown-toggle" data-toggle="dropdown">' . $value['class'] . '<b class="caret corner-caret"></b></a>'
                    . '<ul class="dropdown-menu">'
                    . '<li class="links" id="' . $value['aid'] . '"><a>' . $value['activity'] . '</a></li>'
                    . '</ul>'
                    . '</li>'
                    . '</li>';
        }
    }
    return $a;
}
