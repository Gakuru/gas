<?php

session_start();

/**
 * Generates options for combo boxes on request
 *
 * @author codethorn
 */
class comboLoader {

    var $fr;
    var $fors = NULL;
    var $dbQuery = NULL;

    public function __construct($fr) {
        $this->fr = $fr;
        $this->dbQuery = new DbQuery();
        $this->fors = ['pump' => 'product', 'pric' => 'product', 'stor' => 'product', 'stoc' => 'product', 'stor' => 'product', 'rent' => 'months']; //for pumps,prices and storage return products
    }

    private function getData() {
        return $this->dbQuery->getBySid($this->fors[$this->fr], $_SESSION['sid'], 0);
    }

    public function setOptions() {
        if ($this->fr == 'rent') {
            $months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
            foreach ($months as $key => $value) {
                $options.='<option value="' . ( ++$key) . '">' . $value . '</option>';
            }
            return $options;
        } else {
            foreach ($this->getData() as $value) {
                $options.='<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
            }
            return $options;
        }
    }

}
