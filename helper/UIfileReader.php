<?php

/**
 * Reads UI files
 *
 * @author nelson
 */
class UIfileReader {

    var $ptag = '{~}';
    var $f_handle = null;
    var $dt = NULL;

    public function __construct($file, $dt) {        
        $this->dt = $dt;
        $this->readFile($file);        
    }

    private function readFile($file) {
        $this->f_handle = fopen($file, 'r');
        $this->ui_write();
        fclose($this->f_handle);
    }

    private function ui_write() {
        if ($this->f_handle) {
            $i = 0;
            while (($line = fgets($this->f_handle)) !== FALSE) {                               
                //Process line read
                if (strpos($line, $this->ptag)) {
                    echo $this->dt[$i];
                    ++$i;
                } else {
                    echo $line;
                }                
            }
        } else {
            //Error opening file
            $this->readFile('../html/404.html');
        }
    }

}
