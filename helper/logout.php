<?php

/**
 * Description of logout
 *
 * @author codethorn
 */
class Logout {

    public function __construct() {
        $this->logOut();
    }

    private function logOut() {
        session_destroy();
        echo '<script> window.location="/"; </script>';
    }

}
