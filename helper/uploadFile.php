<?php

/*
 * 
 * Class to upload files
 * 
 */

class FileUpload {

    var $uploaded_file_name = NULL;
    var $uploaded_file_path = NULL;
    var $fr = NULL;

    public function __construct($fr) {
        $this->fr = $fr;
    }

    public function uploadedFile($timestamp) {
        $uploaded = TRUE;
        $file = 'myfile';
        $thefile = $timestamp . '_' . $_FILES[$file]['name'];
        if ($this->fr == 'stat') {
            $path = '../../uploads/admi/media/images/' . $thefile;
        } else if ($this->fr == 'mana') {
            $path = '../../uploads/empl/media/images/' . $thefile;
        } else {
            $path = '../../uploads/' . $this->fr . '/media/images/' . $thefile;
        }
        if (!move_uploaded_file($_FILES[$file]['tmp_name'], $path)) {
            $uploaded = FALSE;
        } else {
            $this->uploaded_file_name = $thefile;
            $this->uploaded_file_path = $path;
        }        
        return $uploaded;
    }

}
