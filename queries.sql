/* 
 * Copyright 2016 codethorn.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Author:  codethorn
 * Created: Feb 10, 2016
 */

-- drop view stock_view;
-- CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_view` AS 
-- select `st`.`id` AS `id`,`st`.`pid` AS `pid`,`st`.`sid` AS `sid`,`st`.`quantity` AS `quantity`,`st`.`product_id` AS `product_id`,sr.id as storage_id,sr.name as storage_name,`st`.`amount` AS `amount`,`st`.`save_timestamp` AS `save_timestamp`,`pr`.`name` AS `product_name`,`st`.`deleted` AS `deleted` from (`stock` `st` join `product` `pr` on((`st`.`product_id` = `pr`.`id`)) join storage sr on st.product_id=sr.product_id)

-- SELECT sum(mpp.amount) as amount,hour(transaction_datestamp) trhr FROM `m_pesa_payments` mpp where sid=1 and transaction_datestamp like '%2015-01-04 15%'  group by trhr having trhr between 12 and 19

-- drop view prepay_consumed_view;
-- create view prepay_consumed_view as SELECT pc.*,fcv.product_name,fcv.pump_price_per_litre,fcv.pump_no,fcv.shift_name,round((fcv.final_reading-fcv.initial_reading),2)as litres_sold,fcv.employee_name FROM `prepay_consumed` pc join fuel_collection_view fcv on pc.save_timestamp=fcv.save_timestamp
-- SELECT pc.*,fcv.product_name,fcv.pump_price_per_litre,fcv.pump_no,fcv.shift_name,round((fcv.final_reading-fcv.initial_reading),2)as litres_sold,fcv.employee_name FROM `prepay_consumed` pc join fuel_collection_view fcv on pc.save_timestamp=fcv.save_timestamp
-- SELECT cr.*,ppv.product_name,ppv.amount as sale_price_per_litre,ppv.pump_no,round((cr.final_reading-cr.initial_reading),2)as litres_sold FROM `credit` cr join pump_product_price_view ppv on cr.pump_id=ppv.pump_id
-- drop view login_user_view;
-- create view login_user_view as select `lo`.`id` AS `id`,`lo`.`pid` AS `pid`,`lo`.`sid` AS `sid`,`lo`.`employee_id` AS `employee_id`,`lo`.`username` AS `username`,`lo`.`password` AS `password`,`lo`.`user_group` AS `user_group`,st.`name` as station_name,`lo`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `user_name`,`em`.`profile_photo_path` AS `profile_photo_path` from (`login` `lo` join `employee` `em` on((`lo`.`employee_id` = `em`.`id`)) join station_info st on st.id= lo.sid) where ((`em`.`deleted` = 0) and (`lo`.`user_group` <> 'p'));
-- select `lo`.`id` AS `id`,`lo`.`pid` AS `pid`,`lo`.`sid` AS `sid`,`lo`.`employee_id` AS `employee_id`,`lo`.`username` AS `username`,`lo`.`password` AS `password`,`lo`.`user_group` AS `user_group`,st.`name`,`lo`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `user_name`,`em`.`profile_photo_path` AS `profile_photo_path` from (`login` `lo` join `employee` `em` on((`lo`.`employee_id` = `em`.`id`)) join station_info st on st.id= lo.sid) where ((`em`.`deleted` = 0) and (`lo`.`user_group` <> 'p'))


-- patch for rent

-- drop view r_rent_view;
-- create view r_rent_view as
select rr.*,rtv.name,rtv.unit_no from r_rent rr join r_tenant_view rtv on rr.tenant_id=rtv.id;

-- end patch for rent