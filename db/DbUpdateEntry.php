<?php

include_once 'dbconn.php';
include_once 'dbGeneralFunctions.php';

/**
 * Interacts with the database;
 *
 * @author nelson
 */
class DbUpdateEntry {

    var $dbh = NULL;
    var $sql_error=array();

    public function __construct() {
        $this->dbh = connect();
    }

    public function updateProcedure($procedure, $data) {
        $okay = FALSE;
        $q = setQs(count($data));
        $stmt = $this->dbh->prepare("call {$procedure}({$q})");
        $i = 1;
        foreach ($data as $value) {
            $stmt->bindValue(($i++), $value);
        }
        if ($stmt->execute()) {
            $okay = TRUE;
        }
        $this->sql_error=$stmt->errorInfo()[2];
        return $okay;
    }

}
