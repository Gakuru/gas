<?php

include_once 'dbconn.php';
include_once 'dbGeneralFunctions.php';

/**
 * Interacts with the database;
 *
 * @author nelson
 */
class DbRemoveEntry {

    var $dbh = NULL;

    public function __construct() {
        $this->dbh = connect();
    }

    public function trashRecord($table, $id) {
        $okay = FALSE;
        $stmt = $this->dbh->prepare('UPDATE `' . $table . '` SET `deleted`=? WHERE `id`=?;');
        $stmt->bindValue(1, TRUE);
        $stmt->bindParam(2, $id);
        $stmt->execute();
        if ($stmt->execute()) {
            $okay = TRUE;
        }
        return $okay;
    }

}
