TRUNCATE `collection`;
TRUNCATE `creditor`;
TRUNCATE `debtor`;
TRUNCATE `duty`;
TRUNCATE `employee`;
TRUNCATE `expense`;
TRUNCATE `fuel_gauge`;
TRUNCATE `login`;
TRUNCATE `parent`;
TRUNCATE `prepay`;
TRUNCATE `prepay_client`;
TRUNCATE `price`;
TRUNCATE `product`;
TRUNCATE `pump`;
TRUNCATE `shift`;
TRUNCATE `station_info`;
TRUNCATE `stock`;
TRUNCATE `storage`;
TRUNCATE `sys_user_control`;
TRUNCATE `user_account`;
TRUNCATE `wage`;
