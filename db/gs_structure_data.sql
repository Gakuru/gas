-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 24, 2016 at 01:53 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `collection_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_shift_id` INT, IN `p_pump_id` INT, IN `p_product_id` INT, IN `p_price_id` INT, IN `p_initial_reading` DOUBLE, IN `p_final_reading` DOUBLE, IN `p_amount` DOUBLE, IN `p_expected_amount` DOUBLE, IN `p_save_timestamp` INT)
    NO SQL
insert into collection (pid,sid,employee_id,shift_id,pump_id,product_id,price_id,initial_reading,final_reading,amount,expected_amount,save_timestamp) values (p_pid,p_sid,p_employee_id,p_shift_id,p_pump_id,p_product_id,p_price_id,p_initial_reading,p_final_reading,p_amount,p_expected_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `collection_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `fuel_collection_view` where CONCAT(employee_name,' ',shift_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0 group by employee_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `collection_update_final_pump_reading_pro`(IN `p_final_reading` INT, IN `p_id` INT)
    NO SQL
update pump set final_reading=p_final_reading where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `creditor_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_creditor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into creditor (pid,sid,creditor_name,description,contact_phone,profile_photo_path,save_timestamp) values (p_pid,p_sid,p_creditor_name,p_description,p_contact_phone,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `creditor_search_pro`(IN `param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `creditor` where CONCAT(creditor_name,' ',description,' ',contact_phone) like CONCAT('%',param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `creditor_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_creditor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_id` INT)
    NO SQL
update `creditor` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`creditor_name`=p_creditor_name,`description`=p_description,`contact_phone`=p_contact_phone where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `debtor_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_debtor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into debtor (pid,sid,debtor_name,description,contact_phone,profile_photo_path,save_timestamp) values (p_pid,p_sid,p_debtor_name,p_description,p_contact_phone,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `debtor_search_pro`(IN `param` VARCHAR(50), IN `sid` INT)
    NO SQL
SELECT * FROM `debtor` where CONCAT(debtor_name,' ',description,' ',contact_phone) like CONCAT('%',param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `debtor_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_debtor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_id` INT)
    NO SQL
update `debtor` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`debtor_name`=p_debtor_name,`description`=p_description,`contact_phone`=p_contact_phone where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `duty_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(30), IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `duty` (`pid`,`sid`,`name`,`save_timestamp`) VALUES (p_pid,p_sid,p_name,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `duty_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `duty` where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `duty_update_pro`(IN `p_name` VARCHAR(30), IN `p_id` INT)
    NO SQL
UPDATE `duty` SET `name`=p_name WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `employee_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_surname` VARCHAR(30), IN `p_other_names` VARCHAR(50), IN `p_id_no` VARCHAR(30), IN `p_contact_phone_no` VARCHAR(30), IN `p_wage_id` INT, IN `p_duty_id` INT, IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `employee` (`pid`,`sid`,`surname`,`other_names`,`id_no`,`contact_phone`,`wage_id`,`duty_id`,`profile_photo_path`,`date`) values (p_pid,p_sid,p_surname,p_other_names,p_id_no,p_contact_phone_no,p_wage_id,p_duty_id,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `employee_search_pro`(IN `param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `employee_view` where CONCAT(surname,' ',other_names,' ',id_no,' ',contact_phone,' ',duty_name) like CONCAT('%',param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `employee_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_surname` VARCHAR(20), IN `p_other_names` VARCHAR(50), IN `p_id_no` VARCHAR(30), IN `p_contact_phone` VARCHAR(30), IN `p_wage_id` INT, IN `p_duty_id` INT, IN `p_id` INT)
    NO SQL
update `employee` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`surname`=p_surname,`other_names`=p_other_names,`id_no`=p_id_no,`contact_phone`=p_contact_phone,`wage_id`=p_wage_id,`duty_id`=p_duty_id where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `expense_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_expense_name` VARCHAR(30), IN `p_expense_date` INT, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
insert `expense` (pid,sid,expense_name,expense_date,amount,save_timestamp) values (p_pid,p_sid,p_expense_name,p_expense_date,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `expense_search_pro`(IN `p_param` VARCHAR(20), IN `p_sid` INT)
    NO SQL
SELECT * FROM `expense` where CONCAT(expense_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `expense_update_pro`(IN `p_expense_name` VARCHAR(30), IN `p_expense_date` INT, IN `p_amount` DOUBLE, IN `p_id` INT)
    NO SQL
update expense set expense_name=p_expense_name,expense_date=p_expense_date,amount=p_amount where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fuel_gauge_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_product_id` INT, IN `p_fill_up` DOUBLE, IN `p_fetch_out` DOUBLE)
    NO SQL
insert into fuel_gauge (pid,sid,product_id,fill_up,fetch_out) values (p_pid,p_sid,p_product_id,p_fill_up,p_fetch_out) on duplicate key update fill_up=fill_up+p_fill_up,fetch_out=fetch_out+p_fetch_out$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `login_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_username` VARCHAR(50), IN `p_password` VARCHAR(50), IN `p_user_group` VARCHAR(1), IN `p_save_timestamp` INT)
    NO SQL
insert into login (pid,sid,employee_id,username,password,user_group,save_timestamp) values (p_pid,p_sid,p_employee_id,p_username,p_password,p_user_group,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `manager_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_surname` VARCHAR(30), IN `p_other_names` VARCHAR(50), IN `p_id_no` VARCHAR(30), IN `p_contact_phone_no` VARCHAR(30), IN `p_profile_photo_path` VARCHAR(255), IN `p_mans` BOOLEAN, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `employee` (`pid`,`sid`,`surname`,`other_names`,`id_no`,`contact_phone`,`profile_photo_path`,`mans`,`date`) values (p_pid,p_sid,p_surname,p_other_names,p_id_no,p_contact_phone_no,p_profile_photo_path,p_mans,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `parent_insert_pro`(IN `p_prop_name` VARCHAR(50), IN `p_stat_name` VARCHAR(50), IN `p_contact_phone` VARCHAR(20), IN `p_contact_email` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into parent (prop_name,stat_name,contact_phone,contact_email,save_timestamp) values (p_prop_name,p_stat_name,p_contact_phone,p_contact_email,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_client_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into prepay_client (pid,sid,name,description,contact_phone,profile_photo_path,save_timestamp) values (p_pid,p_sid,p_name,p_description,p_contact_phone,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_client_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `prepay_client` where CONCAT(name,' ',description,' ',contact_phone) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_client_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_id` INT)
    NO SQL
update `prepay_client` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`name`=p_name,`description`=p_description,`contact_phone`=p_contact_phone where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_prepay_client_id` INT, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `prepay` (`pid`,`sid`,`prepay_client_id`,`amount`,`save_timestamp`) VALUES (p_pid,p_sid,p_prepay_client_id,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `prepay_total_view`where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_update_pro`(IN `p_prepay_client_id` INT, IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
UPDATE `prepay` SET `prepay_client_id`=p_prepay_client_id,`amount`=p_amount WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `price_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_product_id` INT, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `price` (`pid`,`sid`,`product_id`,`amount`,`save_timestamp`) VALUES (p_pid,p_sid,p_product_id,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `price_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `price_view`where CONCAT(product_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `price_update_pro`(IN `p_product_id` INT, IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
UPDATE `price` SET `product_id`=p_product_id,`amount`=p_amount WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `product_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(50), IN `p_save_timestamp` INT)
    NO SQL
insert into product (pid,sid,name,save_timestamp) values (p_pid,p_sid,p_name,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `product_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `product` where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `product_update_pro`(IN `p_name` VARCHAR(50), IN `p_id` INT)
    NO SQL
update product set name=p_name where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pump_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_pump_no` VARCHAR(30), IN `p_product_id` INT, IN `p_initial_reading` INT, IN `p_save_timestamp` INT)
    NO SQL
insert into pump (pid,sid,pump_no,product_id,initial_reading,final_reading,save_timestamp) VALUES (p_pid,p_sid,p_pump_no,p_product_id,p_initial_reading,p_initial_reading,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pump_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT puv.id as pump_id,puv.initial_reading as final_reading,puv.sid,puv.product_name,puv.pump_no,prv.amount FROM `pump_view` puv join price_view prv on puv.product_id=prv.product_id where concat(puv.pump_no,' ',puv.product_name) like CONCAT('%',p_param,'%') and puv.sid = p_sid group by puv.id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pump_update_pro`(IN `p_pump_no` VARCHAR(30), IN `p_product_id` INT, IN `p_initial_reading` INT, IN `p_id` INT)
    NO SQL
update pump set pump_no=p_pump_no, product_id=p_product_id,initial_reading=p_initial_reading where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `shift_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(20), IN `p_sfrom` VARCHAR(20), IN `p_sto` VARCHAR(20), IN `p_save_timestamp` INT)
    NO SQL
insert `shift` (pid,sid,name,sfrom,sto,save_timestamp) values (p_pid,p_sid,p_name,p_sfrom,p_sto,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `shift_search_pro`(IN `p_param` VARCHAR(20), IN `p_sid` INT)
    NO SQL
SELECT * FROM `shift` where CONCAT(name,' ',sfrom,' ',sto) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `shift_update_pro`(IN `p_name` VARCHAR(20), IN `p_sfrom` VARCHAR(20), IN `p_sto` VARCHAR(20), IN `p_id` INT)
    NO SQL
update shift set name=p_name,sfrom=p_sfrom,sto=p_sto where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `station_info_insert_pro`(IN `p_pid` INT, IN `p_name` VARCHAR(50), IN `p_location` VARCHAR(100), IN `p_contact_phone` VARCHAR(20), IN `p_contact_address` VARCHAR(255), IN `p_contact_email` VARCHAR(255), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into station_info (pid,name,location,contact_phone,contact_address,contact_email,profile_photo_path,save_timestamp) values (p_pid,p_name,p_location,p_contact_phone,p_contact_address,p_contact_email,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `station_info_search_pro`(IN `p_param` VARCHAR(50), IN `p_pid` INT)
    NO SQL
SELECT * FROM `station_info` where CONCAT(name,' ',location,' ',contact_address,' ',contact_phone,' ',contact_email) like CONCAT('%',p_param,'%') AND pid=p_pid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `station_info_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_name` VARCHAR(50), IN `p_station_manager` VARCHAR(50), IN `p_location` VARCHAR(100), IN `p_contact_phone` VARCHAR(20), IN `p_contact_address` VARCHAR(255), IN `p_contact_email` VARCHAR(255), IN `p_id` INT)
    NO SQL
update station_info set profile_photo_path =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path), name=p_name, station_manager=p_station_manager, location=p_location,contact_phone=p_contact_phone,contact_address=p_contact_address,contact_email=p_contact_email where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stock_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_product_id` INT, IN `p_storage_id` INT, IN `p_quantity` DOUBLE, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
insert into stock (pid,sid,product_id,storage_id,quantity,amount,save_timestamp) VALUES (p_pid,p_sid,p_product_id,p_storage_id,p_quantity,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stock_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `stock_view` where CONCAT(product_name,' ',quantity,' ',amount) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stock_update_pro`(IN `p_product_id` INT, IN `p_storage_id` INT, IN `p_quantity` DOUBLE, IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
update stock set product_id=p_product_id,storage_id=p_storage_id,quantity=p_quantity,amount=p_amount where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `storage_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(30), IN `p_product_id` INT, IN `p_capacity` DOUBLE, IN `p_save_timestamp` INT)
    NO SQL
insert into storage (pid,sid,name,product_id,capacity,save_timestamp) VALUES (p_pid,p_sid,p_name,p_product_id,p_capacity,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `storage_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `storage_view` where CONCAT(storage_name,' ',product_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `storage_update_pro`(IN `p_name` VARCHAR(30), IN `p_product_id` INT, IN `p_capacity` INT, IN `p_id` INT)
    NO SQL
update storage set name=p_name, product_id=p_product_id,capacity=p_capacity where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sys_user_control_get_system_modules_pro`(IN `p_sid` INT, IN `p_employee_id` INT)
    NO SQL
select sm.* from sys_modules as sm where sm.id not in (select sc.module_id from sys_user_control as sc where sc.sid=p_sid and sc.employee_id=p_employee_id) and sm.published=1 and sm.aid!='aid' order by sort asc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sys_user_control_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_module_id` INT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `sys_user_control` (`pid`,`sid`,`employee_id`,`module_id`,`save_timestamp`) VALUES (p_pid,p_sid,p_employee_id,p_module_id,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_account_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `user_account` (`pid`,`sid`,`employee_id`,`save_timestamp`) VALUES (p_pid,p_sid,p_employee_id,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_account_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `user_account_view` where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_account_update_pro`(IN `p_employee_id` INT, IN `p_id` INT)
    NO SQL
UPDATE `user_account` SET `employee_id`=p_employee_id WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `wage_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_wage_group` VARCHAR(10), IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `wage` (`pid`,`sid`,`wage_group`,`amount`,`save_timestamp`) VALUES (p_pid,p_sid,p_wage_group,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `wage_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `wage`where CONCAT(wage_group,' ',amount) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `wage_update_pro`(IN `p_wage_group` VARCHAR(10), IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
UPDATE `wage` SET `wage_group`=p_wage_group,`amount`=p_amount WHERE `id`=p_id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE IF NOT EXISTS `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `pump_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `initial_reading` bigint(20) NOT NULL,
  `final_reading` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `expected_amount` double NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `save_timestamp` (`save_timestamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `collection`
--

INSERT INTO `collection` (`id`, `pid`, `sid`, `employee_id`, `shift_id`, `pump_id`, `product_id`, `price_id`, `initial_reading`, `final_reading`, `amount`, `expected_amount`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 2, 1, 1, 2, 1, 0, 1525, 122000, 122000, 0, 1454921134),
(2, 1, 1, 2, 2, 1, 2, 1, 0, 10, 800, 800, 0, 1454921613),
(3, 3, 5, 14, 3, 6, 9, 5, 0, 10, 890, 899, 0, 1455181295),
(4, 3, 5, 14, 3, 7, 9, 5, 0, 100, 8990, 8990, 0, 1455181321),
(5, 3, 5, 14, 4, 8, 8, 6, 0, 10, 789, 798, 0, 1455181367),
(6, 1, 6, 15, 5, 3, 3, 3, 0, 100, 8990, 8990, 0, 1455181983),
(7, 1, 6, 15, 6, 3, 3, 3, 0, 1000, 89900, 89900, 0, 1455182052),
(8, 1, 7, 16, 7, 10, 6, 7, 0, 1000, 89900, 89800, 0, 1455183715),
(9, 1, 7, 16, 8, 11, 5, 8, 0, 1000, 78900, 78800, 0, 1455183823),
(10, 1, 10, 18, 9, 14, 10, 11, 30, 600, 35000, 40128, 0, 1455876142),
(11, 1, 10, 18, 10, 13, 11, 10, 0, 1000, 88900, 88900, 0, 1455876263),
(12, 1, 1, 2, 2, 1, 2, 1, 0, 100, 8000, 8000, 0, 1456124803),
(13, 1, 1, 2, 11, 15, 12, 12, 0, 30, 1305, 1305, 0, 1456143002),
(14, 1, 1, 2, 1, 15, 12, 12, 30, 40, 430, 435, 0, 1456143146),
(15, 1, 1, 2, 2, 15, 12, 12, 40, 50, 440, 435, 0, 1456143171),
(16, 1, 1, 19, 1, 2, 1, 2, 600, 770, 15300, 15300, 0, 1456144303),
(17, 1, 6, 20, 5, 3, 3, 3, 1000, 1110, 9889, 9889, 0, 1456209938),
(18, 1, 6, 15, 5, 5, 4, 4, 0, 20, 1576, 1576, 0, 1456213667),
(19, 1, 6, 15, 6, 5, 4, 4, 20, 26, 473, 472.8, 0, 1456214530),
(20, 1, 6, 20, 5, 5, 4, 4, 26, 27, 79, 78.8, 0, 1456214617),
(21, 1, 6, 15, 5, 5, 4, 4, 27, 30, 200, 236.4, 0, 1456214672),
(22, 1, 6, 20, 6, 4, 3, 3, 0, 100, 8990, 8990, 0, 1456216826),
(23, 1, 6, 20, 6, 4, 3, 3, 100, 222, 10968, 10967.8, 0, 1456216895),
(24, 1, 6, 15, 5, 4, 3, 3, 222, 333, 9979, 9978.9, 0, 1456217173),
(25, 1, 6, 20, 5, 4, 3, 3, 333, 452, 10698, 10698.1, 0, 1456217251),
(26, 1, 6, 15, 5, 4, 3, 3, 452, 554, 9170, 9169.8, 0, 1456217434),
(27, 1, 6, 20, 6, 4, 3, 3, 554, 563, 809, 809.1, 0, 1456224254),
(28, 1, 6, 20, 6, 4, 3, 3, 563, 664, 9080, 9079.9, 0, 1456224984),
(29, 1, 6, 15, 6, 4, 3, 3, 664, 675, 989, 988.9, 0, 1456225111),
(30, 1, 6, 20, 6, 4, 3, 3, 675, 686, 989, 988.9, 0, 1456225183),
(31, 1, 6, 20, 6, 4, 3, 3, 686, 698, 1078.8, 1078.8, 0, 1456225477),
(32, 1, 6, 20, 5, 4, 3, 3, 698, 797, 8900.1, 8900.1, 0, 1456240514),
(33, 1, 1, 2, 1, 2, 1, 2, 770, 900, 11700, 11700, 0, 1456288731);

-- --------------------------------------------------------

--
-- Table structure for table `creditor`
--

CREATE TABLE IF NOT EXISTS `creditor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `creditor_name` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_phone` (`contact_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `debtor`
--

CREATE TABLE IF NOT EXISTS `debtor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `debtor_name` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_phone` (`contact_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `duty`
--

CREATE TABLE IF NOT EXISTS `duty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `duty`
--

INSERT INTO `duty` (`id`, `pid`, `sid`, `name`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 'Pump attendant', 0, 1454920350),
(2, 3, 5, 'Pump attendant', 0, 1455181033),
(3, 1, 6, 'Pump attendant', 0, 1455181496),
(4, 1, 7, 'Pump attendant', 0, 1455183421),
(5, 1, 10, 'Attendant', 0, 1455876048);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `other_names` varchar(50) NOT NULL,
  `id_no` varchar(30) NOT NULL,
  `contact_phone` varchar(30) NOT NULL,
  `wage_id` int(11) DEFAULT NULL,
  `duty_id` int(11) DEFAULT NULL,
  `profile_photo_path` varchar(255) DEFAULT NULL,
  `mans` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_no` (`id_no`),
  UNIQUE KEY `contact_phone` (`contact_phone`),
  UNIQUE KEY `name_unique` (`surname`,`other_names`) COMMENT 'surname and other names are unique'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `pid`, `sid`, `surname`, `other_names`, `id_no`, `contact_phone`, `wage_id`, `duty_id`, `profile_photo_path`, `mans`, `deleted`, `date`) VALUES
(1, 1, 1, 'Bll', 'James', '27665710', '0724519109', NULL, NULL, '1454919614_male_icon.png', 1, 0, 1454919614),
(2, 1, 1, 'Mary', 'Johnson', '2370123', '07143214332', 1, 1, '1454920560_male_icon.png', 0, 0, 1454920560),
(8, 1, 6, 'Gakuru', 'Maina', '27665720', '0725648121', NULL, NULL, '1454971333_DSC_0000002.jpg', 1, 0, 1454971333),
(9, 1, 7, 'Gakuru', 'Wanja', '27665730', '0771026329', NULL, NULL, '1454971512_DSC_0000003.jpg', 1, 0, 1454971512),
(10, 2, 2, 'Sandra', 'Ortiz', '27665740', '0734324324', NULL, NULL, '1454972018_DSC_0000175.jpg', 1, 0, 1454972018),
(11, 2, 3, 'Gakuru', 'Esther', '3523123', '0736914422', NULL, NULL, '1454972463_IMG0024A.jpg', 1, 0, 1454972463),
(12, 3, 4, 'Karanja', 'Njihia', '27665750', '0711314520', NULL, NULL, '1455079813_male_icon.png', 1, 0, 1455079813),
(13, 3, 5, 'Michael', 'Johnson', '27665760', '0703314464', NULL, NULL, '1455080164_male_icon.png', 1, 0, 1455080164),
(14, 3, 5, 'Mark', 'Smith', '27665761', '0716777893', 2, 2, '1455181196_male_icon.png', 0, 0, 1455181196),
(15, 1, 6, 'Dino', 'Dino', '27665721', '0706914422', 3, 3, '1455181941_male_icon.png', 0, 0, 1455181941),
(16, 1, 7, 'Tobby', 'Marshall', '27665731', '0731121212', 4, 4, '1455183459_male_icon.png', 0, 0, 1455183459),
(17, 1, 10, 'Otieno', 'Joseph', '22282720', '0724518900', NULL, NULL, '1455873688_enlarge (copy).png', 1, 0, 1455873688),
(18, 1, 10, 'Beg', 'Beg', '26542344', '0771326327', 5, 5, '', 0, 0, 1455876111),
(19, 1, 1, 'Oak', 'Smith', '27665711', '0771012232', 1, 1, '1456144056_Oak1.png', 0, 0, 1456144056),
(20, 1, 6, 'Toby', 'Marshal', '27665722', '0706914423', 3, 3, '1456209755_bike_karizmaZMR.png', 0, 0, 1456209755);

-- --------------------------------------------------------

--
-- Stand-in structure for view `employee_view`
--
CREATE TABLE IF NOT EXISTS `employee_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`surname` varchar(20)
,`other_names` varchar(50)
,`id_no` varchar(30)
,`contact_phone` varchar(30)
,`wage_id` int(11)
,`duty_id` int(11)
,`profile_photo_path` varchar(255)
,`deleted` tinyint(1)
,`date` int(11)
,`wage_group` varchar(10)
,`duty_name` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `expense_name` varchar(30) NOT NULL,
  `expense_date` int(11) NOT NULL,
  `amount` double NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `fuel_collection_view`
--
CREATE TABLE IF NOT EXISTS `fuel_collection_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`employee_id` int(11)
,`shift_id` int(11)
,`pump_id` int(11)
,`product_id` int(11)
,`price_id` int(11)
,`initial_reading` bigint(20)
,`final_reading` bigint(20)
,`amount` double
,`expected_amount` double
,`deleted` tinyint(1)
,`save_timestamp` int(11)
,`employee_name` varchar(71)
,`product_name` varchar(50)
,`pump_price_per_litre` float
,`pump_no` varchar(30)
,`shift_name` varchar(20)
);
-- --------------------------------------------------------

--
-- Table structure for table `fuel_gauge`
--

CREATE TABLE IF NOT EXISTS `fuel_gauge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `fill_up` double NOT NULL,
  `fetch_out` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pid_sid_product` (`pid`,`sid`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `fuel_gauge`
--

INSERT INTO `fuel_gauge` (`id`, `pid`, `sid`, `product_id`, `fill_up`, `fetch_out`) VALUES
(1, 1, 1, 2, 7000, 1635),
(2, 1, 1, 1, 7000, 1670),
(3, 1, 6, 4, 7000, 103),
(4, 1, 6, 3, 15000, 7954),
(5, 3, 5, 9, 20000, 110),
(6, 3, 5, 8, 37000, 10),
(12, 1, 7, 6, 30000, 1000),
(13, 1, 7, 5, 22000, 1000),
(14, 1, 10, 11, 20000, 1000),
(15, 1, 10, 10, 25000, 600),
(16, 1, 1, 12, 6000, 120);

-- --------------------------------------------------------

--
-- Stand-in structure for view `fuel_gauge_view`
--
CREATE TABLE IF NOT EXISTS `fuel_gauge_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`product_id` int(11)
,`fill_up` double
,`fetch_out` double
,`product_name` varchar(50)
,`capacity` double
);
-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL COMMENT 'also parent id',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_group` varchar(1) NOT NULL DEFAULT 'b',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `pid`, `sid`, `employee_id`, `username`, `password`, `user_group`, `save_timestamp`) VALUES
(1, 1, 1, 1, 'noc', 'noc', 'p', 1454919314),
(2, 1, 1, 1, '27665710', '27665710', 'a', 1454919614),
(3, 2, 2, 2, 'agip', 'agip', 'p', 1454970614),
(4, 3, 3, 3, 'caltex', 'caltex', 'p', 1454970999),
(5, 1, 6, 8, '27665720', '27665720', 'a', 1454971333),
(6, 1, 7, 9, '27665730', '27665730', 'a', 1454971512),
(7, 2, 2, 10, '27665740', '27665740', 'a', 1454972018),
(8, 2, 3, 11, '27665770', '27665770', 'a', 1454972463),
(9, 3, 4, 12, '27665750', '27665750', 'a', 1455079813),
(10, 3, 5, 13, '27665760', '27665760', 'a', 1455080164),
(11, 1, 10, 17, '22282720', '22282720', 'a', 1455873688);

-- --------------------------------------------------------

--
-- Stand-in structure for view `login_parent_view`
--
CREATE TABLE IF NOT EXISTS `login_parent_view` (
`id` int(11)
,`pid` int(11)
,`username` varchar(50)
,`password` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `login_user_view`
--
CREATE TABLE IF NOT EXISTS `login_user_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`employee_id` int(11)
,`username` varchar(50)
,`password` varchar(50)
,`user_group` varchar(1)
,`save_timestamp` int(11)
,`user_name` varchar(71)
,`profile_photo_path` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `manager_view`
--
CREATE TABLE IF NOT EXISTS `manager_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`surname` varchar(20)
,`other_names` varchar(50)
,`id_no` varchar(30)
,`contact_phone` varchar(30)
,`profile_photo_path` varchar(255)
,`deleted` tinyint(1)
,`date` int(11)
,`station_name` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `m_pesa_payments`
--

CREATE TABLE IF NOT EXISTS `m_pesa_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `service_name` varchar(40) NOT NULL,
  `business_number` varchar(10) NOT NULL,
  `transaction_reference` varchar(20) NOT NULL,
  `internal_transaction_id` varchar(20) NOT NULL,
  `transaction_timestamp` int(11) NOT NULL,
  `transaction_datestamp` varchar(30) NOT NULL,
  `transaction_type` varchar(20) NOT NULL,
  `account_number` varchar(30) NOT NULL,
  `sender_phone` varchar(20) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `amount` double NOT NULL,
  `currency` varchar(20) NOT NULL,
  `signature` varchar(50) NOT NULL,
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `m_pesa_payments`
--

INSERT INTO `m_pesa_payments` (`id`, `pid`, `sid`, `service_name`, `business_number`, `transaction_reference`, `internal_transaction_id`, `transaction_timestamp`, `transaction_datestamp`, `transaction_type`, `account_number`, `sender_phone`, `first_name`, `middle_name`, `last_name`, `amount`, `currency`, `signature`, `save_timestamp`) VALUES
(1, 1, 1, 'M-PESA', '892127', 'GG32WY210', '2607200', 1420373070, '2015-01-04 15:04:30', 'buygoods', 'Not Listed', '254708293116', 'Job', '', 'Muriuki', 500, 'Ksh', 'tMCssk7lKbT9EFfGkCOXuzBLvno=', 1420373070),
(2, 1, 1, 'M-PESA', '892127', 'GG32WY210', '2607200', 1420373060, '2016-02-24 15:04:20', 'buygoods', 'Not Listed', '254708293116', 'Job', '', 'Muriuki', 1000, 'Ksh', 'tMCssk7lKbT9EFfGkCOXuzBLvno=', 1420373060),
(3, 1, 2, 'M-PESA', '892127', 'GG32WY210', '2607200', 1420372060, '2016-02-24 14:47:40', 'buygoods', 'Not Listed', '254708293116', 'Job', '', 'Muriuki', 2000, 'Ksh', 'tMCssk7lKbT9EFfGkCOXuzBLvno=', 1420372060),
(4, 1, 2, 'M-PESA', '892127', 'GG32WY210', '2607200', 1420372260, '2015-01-04 14:51:00\n', 'buygoods', 'Not Listed', '254708293116', 'Job', '', 'Muriuki', 500, 'Ksh', 'tMCssk7lKbT9EFfGkCOXuzBLvno=', 1420372260);

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

CREATE TABLE IF NOT EXISTS `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_name` varchar(50) NOT NULL,
  `stat_name` varchar(50) NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `parent`
--

INSERT INTO `parent` (`id`, `prop_name`, `stat_name`, `contact_phone`, `contact_email`, `save_timestamp`) VALUES
(1, 'NOC', 'NOC', '0725648070', 'gakurumaina@gmail.com', 1454919314),
(2, 'Agip Kenya Ltd', 'Agip', '0733123123', 'info.@agip.co.ke', 1454970614),
(3, 'Caltex Kenya Ltd', 'Caltex', '0770839839', 'info@caltextke.com', 1454970999);

-- --------------------------------------------------------

--
-- Table structure for table `prepay`
--

CREATE TABLE IF NOT EXISTS `prepay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `prepay_client_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prepay_client`
--

CREATE TABLE IF NOT EXISTS `prepay_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_phone` (`contact_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `prepay_total_view`
--
CREATE TABLE IF NOT EXISTS `prepay_total_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`prepay_client_id` int(11)
,`name` varchar(50)
,`profile_photo_path` varchar(255)
,`total_amount` double
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `prepay_view`
--
CREATE TABLE IF NOT EXISTS `prepay_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`prepay_client_id` int(11)
,`amount` float
,`deleted` tinyint(1)
,`save_timestamp` int(11)
,`name` varchar(50)
,`profile_photo_path` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id`, `pid`, `sid`, `product_id`, `amount`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 2, 80, 0, 1454920242),
(2, 1, 1, 1, 90, 0, 1454920247),
(3, 1, 6, 3, 89.9, 0, 1455103184),
(4, 1, 6, 4, 78.8, 0, 1455103193),
(5, 3, 5, 9, 89.9, 0, 1455180850),
(6, 3, 5, 8, 79.8, 0, 1455180862),
(7, 1, 7, 6, 89.8, 0, 1455183640),
(8, 1, 7, 5, 78.8, 0, 1455183648),
(9, 1, 7, 7, 53.4, 0, 1455183657),
(10, 1, 10, 11, 88.9, 0, 1455876031),
(11, 1, 10, 10, 70.4, 0, 1455876040),
(12, 1, 1, 12, 43.5, 0, 1456142012);

-- --------------------------------------------------------

--
-- Stand-in structure for view `price_view`
--
CREATE TABLE IF NOT EXISTS `price_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`product_id` int(11)
,`product_name` varchar(50)
,`amount` float
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product` (`sid`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `pid`, `sid`, `name`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 'Super', 0, 1454919725),
(2, 1, 1, 'Diesel', 0, 1454919730),
(3, 1, 6, 'Super', 0, 1455103115),
(4, 1, 6, 'Diesel', 0, 1455103120),
(5, 1, 7, 'Diesel', 0, 1455105332),
(6, 1, 7, 'Super', 0, 1455105336),
(7, 1, 7, 'Kerosine', 0, 1455105340),
(8, 3, 5, 'Diesel', 0, 1455180730),
(9, 3, 5, 'Super', 0, 1455180735),
(10, 1, 10, 'Diesel', 0, 1455873955),
(11, 1, 10, 'Super', 0, 1455873959),
(12, 1, 1, 'Kerosine', 0, 1456141979);

-- --------------------------------------------------------

--
-- Table structure for table `pump`
--

CREATE TABLE IF NOT EXISTS `pump` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `pump_no` varchar(30) NOT NULL,
  `product_id` int(11) NOT NULL,
  `initial_reading` int(11) NOT NULL,
  `final_reading` int(11) DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pump_no_sid` (`pump_no`,`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `pump`
--

INSERT INTO `pump` (`id`, `pid`, `sid`, `pump_no`, `product_id`, `initial_reading`, `final_reading`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 'Pump one Noozle one', 2, 0, 100, 0, 1454920459),
(2, 1, 1, 'Pump one Noozle two', 1, 300, 900, 0, 1454920489),
(3, 1, 6, 'Pump one Noozle one', 3, 0, 1110, 0, 1455103217),
(4, 1, 6, 'Pump one Noozle two', 3, 0, 797, 0, 1455103228),
(5, 1, 6, 'Pump one Noozle three', 4, 0, 30, 0, 1455103237),
(6, 3, 5, 'Nozzle A001', 9, 0, 10, 0, 1455181053),
(7, 3, 5, 'Noozle A002', 9, 0, 100, 0, 1455181069),
(8, 3, 5, 'Noozle B001', 8, 0, 10, 0, 1455181083),
(9, 3, 5, 'Noozle B002', 8, 0, 0, 0, 1455181092),
(10, 1, 7, 'Noozle one', 6, 0, 1000, 0, 1455183563),
(11, 1, 7, 'Noozel two', 5, 0, 1000, 0, 1455183575),
(12, 1, 7, 'Noozle three', 7, 0, 0, 0, 1455183585),
(13, 1, 10, 'PMS1', 11, 0, 1000, 0, 1455874472),
(14, 1, 10, 'Noozle one AGO', 10, 30, 600, 0, 1455874506),
(15, 1, 1, 'Pump three noozle one', 12, 0, 50, 0, 1456142055);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pump_product_price_view`
--
CREATE TABLE IF NOT EXISTS `pump_product_price_view` (
`id` int(11)
,`pid` int(11)
,`prsid` int(11)
,`pusid` int(11)
,`pisid` int(11)
,`product_name` varchar(50)
,`price_id` int(11)
,`amount` float
,`pump_id` int(11)
,`pump_no` varchar(30)
,`initial_reading` int(11)
,`final_reading` int(11)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `pump_view`
--
CREATE TABLE IF NOT EXISTS `pump_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`pump_no` varchar(30)
,`initial_reading` int(11)
,`product_id` int(11)
,`product_name` varchar(50)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE IF NOT EXISTS `shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `sfrom` varchar(20) NOT NULL,
  `sto` varchar(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shift_duration` (`pid`,`sid`,`sfrom`,`sto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `pid`, `sid`, `name`, `sfrom`, `sto`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 'Morning', '06:00', '12:00', 0, 1454920267),
(2, 1, 1, 'Afternoon', '12:00', '18:00', 0, 1454920281),
(3, 3, 5, 'Morning', '06:00', '12:00', 0, 1455180880),
(4, 3, 5, 'Afternoon', '12:00', '19:00', 0, 1455180903),
(5, 1, 6, 'Morning', '05:00', '12:00', 0, 1455181837),
(6, 1, 6, 'Afternoon', '12:00', '19:00', 0, 1455181849),
(7, 1, 7, 'Morning', '06:00', '12:00', 0, 1455183510),
(8, 1, 7, 'Afternoon', '12:00', '18:00', 0, 1455183531),
(9, 1, 10, 'Morning', '06:00', '12:00', 0, 1455876004),
(10, 1, 10, 'Afte', '12:00', '18:00', 0, 1455876015),
(11, 1, 1, 'Night', '18:00', '22:00', 0, 1456141956),
(12, 1, 6, 'Night', '19:00', '00:00', 0, 1456237202);

-- --------------------------------------------------------

--
-- Table structure for table `station_info`
--

CREATE TABLE IF NOT EXISTS `station_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `employee_id` int(11) DEFAULT '0' COMMENT 'Station manager',
  `location` varchar(100) DEFAULT NULL,
  `contact_phone` varchar(20) DEFAULT NULL,
  `contact_address` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `station_info`
--

INSERT INTO `station_info` (`id`, `pid`, `name`, `employee_id`, `location`, `contact_phone`, `contact_address`, `contact_email`, `profile_photo_path`, `deleted`, `save_timestamp`) VALUES
(1, 1, 'NOC times', 1, 'Haille', '0724524780', '585867 Nairobi', 'noctimes@gmail.com', '', 0, 1454919404),
(2, 2, 'Agip Gatitu', 10, 'Thika Gatitu', '0722123123', '51 Thika', 'agipgatitu@gmail.com', '1454970799_agiplogo.jpeg', 0, 1454970799),
(3, 2, 'Agip Wote', 11, 'Woto', '0712123123', '50 Wote', 'agipwote@gmail.com', '1454970872_agiplogo.jpeg', 0, 1454970872),
(4, 3, 'Caltex Embu', 12, 'Embu Township', '0720323232', '34 - Embu', 'caltexembu@gmail.com', '1454971066_caltexlogo.jpeg', 0, 1454971066),
(5, 3, 'Caltex Kathonzweni', 13, 'Kathonzweni', '0731313131', '30 Kathonzweni', 'caltexkatho@gmail.com', '1454971144_caltexlogo.jpeg', 0, 1454971144),
(6, 1, 'NOC KU', 8, 'Kenyatta University', '0713131313', '234 - Nairobi', 'nocku@gmail.com', '', 0, 1454971235),
(7, 1, 'NOC Garissa Road', 9, 'Thika', '0724242424', '234 Thika', 'nocgarissaroad@gmail.com', '', 0, 1454971465),
(8, 3, 'Caltex Edoret', 0, 'Eldoret town', '0751515151', '40 Eldoret', 'caltexembu@gmail', '1454978346_caltexlogo.jpeg', 0, 1454978346),
(9, 3, 'Caltext Kisii', 0, 'Kisii', '0752525252', '32 Kisii', 'caltexkisii@gmail.com', '1454978393_caltexlogo.jpeg', 0, 1454978393),
(10, 1, 'NOC Thika', 17, 'Thika town', '0711314234', '555 thika', 'nocthika@gmail.com', '', 0, 1455873465);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `pid`, `sid`, `product_id`, `storage_id`, `quantity`, `amount`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 2, 0, 4000, 65, 0, 1454920717),
(2, 1, 1, 1, 0, 7000, 70, 0, 1454920761),
(3, 1, 6, 4, 4, 7000, 73.4, 0, 1455104085),
(4, 1, 6, 3, 3, 15000, 83.3, 0, 1455105127),
(5, 3, 5, 9, 8, 20000, 83.1, 0, 1455180816),
(6, 3, 5, 8, 9, 37000, 71.2, 0, 1455180833),
(7, 1, 7, 6, 7, 30000, 80.2, 0, 1455183261),
(8, 1, 7, 5, 6, 22000, 70.2, 0, 1455183318),
(9, 1, 1, 2, 1, 3000, 79.7, 0, 1455543800),
(10, 1, 10, 11, 10, 20000, 80.7, 0, 1455874093),
(11, 1, 10, 10, 11, 25000, 78.8, 0, 1455874111),
(12, 1, 1, 12, 12, 6000, 36.3, 0, 1456142137);

-- --------------------------------------------------------

--
-- Stand-in structure for view `stock_view`
--
CREATE TABLE IF NOT EXISTS `stock_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`quantity` double
,`product_id` int(11)
,`storage_id` int(11)
,`storage_name` varchar(30)
,`amount` float
,`save_timestamp` int(11)
,`product_name` varchar(50)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE IF NOT EXISTS `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `product_id` int(11) NOT NULL,
  `capacity` double NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `storage`
--

INSERT INTO `storage` (`id`, `pid`, `sid`, `name`, `product_id`, `capacity`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 'Storage one', 2, 8000, 0, 1454920367),
(2, 1, 1, 'storage two', 1, 10000, 0, 1454920381),
(3, 1, 6, 'Storage one', 3, 30000, 0, 1455103140),
(4, 1, 6, 'Storage two', 4, 25000, 0, 1455103152),
(5, 1, 7, 'Tank one', 7, 5000, 0, 1455105364),
(6, 1, 7, 'Tank two', 5, 25000, 0, 1455105378),
(7, 1, 7, 'Tank three', 6, 43000, 0, 1455105391),
(8, 3, 5, 'Tank one', 9, 25000, 0, 1455180754),
(9, 3, 5, 'Tank two', 8, 40000, 0, 1455180773),
(10, 1, 10, 'Storage one', 11, 30000, 0, 1455873976),
(11, 1, 10, 'Storage two', 10, 45000, 0, 1455874060),
(12, 1, 1, 'Storage three', 12, 7000, 0, 1456141998);

-- --------------------------------------------------------

--
-- Stand-in structure for view `storage_view`
--
CREATE TABLE IF NOT EXISTS `storage_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`storage_name` varchar(30)
,`capacity` double
,`product_id` int(11)
,`product_name` varchar(50)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `sys_modules`
--

CREATE TABLE IF NOT EXISTS `sys_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` varchar(50) NOT NULL,
  `place` varchar(10) NOT NULL,
  `class` varchar(50) NOT NULL,
  `activity` varchar(75) NOT NULL,
  `sort` varchar(3) DEFAULT NULL,
  `grouped` varchar(3) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `sys_modules`
--

INSERT INTO `sys_modules` (`id`, `aid`, `place`, `class`, `activity`, `sort`, `grouped`, `published`) VALUES
(1, 'duty', 'acc', 'Tasks', 'Duty', '9', NULL, 1),
(2, 'expe', 'acc', 'Tasks', 'Expenses', '8', NULL, 1),
(3, 'coll', 'acc', 'Tasks', 'Collection', '2', NULL, 1),
(4, 'prep', 'acc', 'Prepay', 'Prepay-ments', '7b', 'aa', 1),
(5, 'shif', 'acc', 'Shifts', 'Shifts', '4', NULL, 1),
(6, 'pump', 'acc', 'Pumps', 'Pumps', '5', NULL, 1),
(7, 'pric', 'acc', 'Prices', 'Prices', '6', NULL, 1),
(8, 'gas', 'acc', 'Extras', 'Gas', '', NULL, 0),
(9, 'lubricants', 'acc', 'Extras', 'Lubricants', '', NULL, 0),
(10, 'empl', 'acc', 'Human resource', 'Employees', '3', NULL, 1),
(11, 'debt', 'acc', 'Debtors', 'Debtors', '11', NULL, 1),
(12, 'stoc', 'acc', 'Stock', 'Stock', '15', NULL, 1),
(13, 'stor', 'acc', 'Tasks', 'Storage', '13', NULL, 1),
(14, 'wage', 'acc', 'Tasks', 'Wages', '10', NULL, 1),
(15, 'cinvoice', 'nav', 'Quick Accounting', 'Create Invoice', '', NULL, 0),
(16, 'ccashbook', 'nav', 'Quick Accounting', 'Cash book', '', NULL, 0),
(17, 'prod', 'acc', 'Products', 'Products', '14', NULL, 1),
(18, 'dash', 'acc', 'Panel', 'Panel', '1', NULL, 1),
(19, 'mpayroll', 'nav', 'Payroll', 'Manage Payroll', '', NULL, 0),
(20, 'stat', 'acc', 'stations', 'My stations', '', NULL, 0),
(21, 'admi', 'nav', 'Administration', 'Administration', '13', NULL, 1),
(22, 'cont', 'nav', 'Controls', 'Access control', '', NULL, 1),
(23, 'cred', 'acc', 'Creditors', 'Creditors', '12', NULL, 1),
(24, 'prec', 'acc', 'Prepay', 'Prepay Clients', '7a', 'aa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sys_user_control`
--

CREATE TABLE IF NOT EXISTS `sys_user_control` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emp_mod` (`employee_id`,`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

--
-- Dumping data for table `sys_user_control`
--

INSERT INTO `sys_user_control` (`id`, `pid`, `sid`, `employee_id`, `module_id`, `save_timestamp`) VALUES
(1, 1, 1, 1, 22, 1454919614),
(2, 1, 1, 1, 12, 1454919666),
(3, 1, 1, 1, 13, 1454919668),
(4, 1, 1, 1, 6, 1454919671),
(5, 1, 1, 1, 7, 1454919673),
(6, 1, 1, 1, 1, 1454919679),
(7, 1, 1, 1, 14, 1454919683),
(8, 1, 1, 1, 18, 1454919687),
(9, 1, 1, 1, 17, 1454919690),
(10, 1, 1, 1, 3, 1454919692),
(11, 1, 1, 1, 10, 1454919694),
(12, 1, 1, 1, 5, 1454919698),
(13, 1, 6, 8, 22, 1454971333),
(14, 1, 7, 9, 22, 1454971512),
(15, 2, 2, 10, 22, 1454972018),
(16, 2, 3, 11, 22, 1454972463),
(17, 3, 4, 12, 22, 1455079813),
(18, 3, 5, 13, 22, 1455080164),
(19, 2, 3, 11, 1, 1455102560),
(20, 2, 3, 11, 10, 1455102579),
(21, 2, 3, 11, 5, 1455102582),
(22, 2, 3, 11, 6, 1455102584),
(23, 2, 3, 11, 17, 1455102587),
(24, 2, 3, 11, 7, 1455102596),
(25, 2, 3, 11, 3, 1455102599),
(26, 2, 3, 11, 12, 1455102601),
(27, 2, 3, 11, 13, 1455102602),
(28, 2, 3, 11, 14, 1455102604),
(29, 2, 3, 11, 18, 1455102607),
(30, 1, 6, 8, 1, 1455102681),
(32, 1, 6, 8, 3, 1455102686),
(33, 1, 6, 8, 5, 1455102688),
(34, 1, 6, 8, 6, 1455102693),
(35, 1, 6, 8, 10, 1455102695),
(36, 1, 6, 8, 7, 1455102697),
(37, 1, 6, 8, 18, 1455102700),
(38, 1, 6, 8, 12, 1455102702),
(39, 1, 6, 8, 13, 1455102704),
(40, 1, 6, 8, 14, 1455102706),
(41, 1, 6, 8, 17, 1455102709),
(42, 1, 7, 9, 1, 1455102792),
(43, 1, 7, 9, 2, 1455102794),
(44, 1, 7, 9, 3, 1455102796),
(45, 1, 7, 9, 5, 1455102798),
(46, 1, 7, 9, 6, 1455102800),
(47, 1, 7, 9, 7, 1455102802),
(48, 1, 7, 9, 10, 1455102803),
(49, 1, 7, 9, 12, 1455102805),
(50, 1, 7, 9, 13, 1455102807),
(51, 1, 7, 9, 14, 1455102808),
(52, 1, 7, 9, 17, 1455102810),
(53, 1, 7, 9, 18, 1455102812),
(54, 2, 2, 10, 1, 1455102854),
(55, 2, 2, 10, 2, 1455102855),
(56, 2, 2, 10, 3, 1455102857),
(57, 2, 2, 10, 5, 1455102860),
(58, 2, 2, 10, 6, 1455102862),
(59, 2, 2, 10, 7, 1455102864),
(60, 2, 2, 10, 10, 1455102866),
(61, 2, 2, 10, 12, 1455102868),
(62, 2, 2, 10, 13, 1455102869),
(63, 2, 2, 10, 14, 1455102871),
(64, 2, 2, 10, 17, 1455102872),
(65, 2, 2, 10, 18, 1455102874),
(66, 3, 4, 12, 1, 1455102911),
(68, 3, 4, 12, 5, 1455102920),
(69, 3, 4, 12, 6, 1455102922),
(70, 3, 4, 12, 3, 1455102925),
(71, 3, 4, 12, 7, 1455102929),
(72, 3, 4, 12, 10, 1455102930),
(73, 3, 4, 12, 12, 1455102932),
(74, 3, 4, 12, 13, 1455102934),
(75, 3, 4, 12, 14, 1455102936),
(76, 3, 4, 12, 17, 1455102938),
(77, 3, 4, 12, 18, 1455102941),
(78, 3, 5, 13, 1, 1455102969),
(79, 3, 5, 13, 3, 1455102970),
(80, 3, 5, 13, 5, 1455102973),
(81, 3, 5, 13, 6, 1455102974),
(82, 3, 5, 13, 7, 1455102976),
(83, 3, 5, 13, 10, 1455102977),
(85, 3, 5, 13, 12, 1455102981),
(86, 3, 5, 13, 13, 1455102983),
(87, 3, 5, 13, 14, 1455102984),
(88, 3, 5, 13, 17, 1455102986),
(89, 3, 5, 13, 18, 1455102987),
(90, 1, 10, 17, 22, 1455873688),
(91, 1, 10, 17, 1, 1455873796),
(92, 1, 10, 17, 3, 1455873799),
(93, 1, 10, 17, 5, 1455873801),
(94, 1, 10, 17, 6, 1455873802),
(95, 1, 10, 17, 7, 1455873868),
(96, 1, 10, 17, 10, 1455873876),
(97, 1, 10, 17, 17, 1455873879),
(98, 1, 10, 17, 12, 1455873883),
(99, 1, 10, 17, 13, 1455873886),
(100, 1, 10, 17, 14, 1455873891),
(101, 1, 10, 17, 18, 1455873895);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sys_user_control_view`
--
CREATE TABLE IF NOT EXISTS `sys_user_control_view` (
`su_id` int(11)
,`employee_id` int(11)
,`module_id` int(11)
,`id` int(11)
,`aid` varchar(50)
,`place` varchar(10)
,`class` varchar(50)
,`activity` varchar(75)
,`published` tinyint(1)
,`grouped` varchar(3)
,`sort` varchar(3)
);
-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`id`, `pid`, `sid`, `employee_id`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 1, 0, 1454919614),
(2, 1, 6, 8, 0, 1454971333),
(3, 1, 7, 9, 0, 1454971512),
(4, 2, 2, 10, 0, 1454972018),
(5, 2, 3, 11, 0, 1454972463),
(6, 3, 4, 12, 0, 1455079813),
(7, 3, 5, 13, 0, 1455080164),
(8, 1, 10, 17, 0, 1455873688);

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_account_view`
--
CREATE TABLE IF NOT EXISTS `user_account_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`employee_id` int(11)
,`deleted` tinyint(1)
,`save_timestamp` int(11)
,`name` varchar(71)
,`profile_photo_path` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `wage`
--

CREATE TABLE IF NOT EXISTS `wage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `wage_group` varchar(10) NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `wage`
--

INSERT INTO `wage` (`id`, `pid`, `sid`, `wage_group`, `amount`, `deleted`, `save_timestamp`) VALUES
(1, 1, 1, 'A', 15000, 0, 1454920297),
(2, 3, 5, 'A', 12000, 0, 1455181019),
(3, 1, 6, 'A', 15000, 0, 1455181481),
(4, 1, 7, 'A', 12500, 0, 1455183435),
(5, 1, 10, 'A', 20000, 0, 1455876102);

-- --------------------------------------------------------

--
-- Structure for view `employee_view`
--
DROP TABLE IF EXISTS `employee_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `employee_view` AS select `em`.`id` AS `id`,`em`.`pid` AS `pid`,`em`.`sid` AS `sid`,`em`.`surname` AS `surname`,`em`.`other_names` AS `other_names`,`em`.`id_no` AS `id_no`,`em`.`contact_phone` AS `contact_phone`,`em`.`wage_id` AS `wage_id`,`em`.`duty_id` AS `duty_id`,`em`.`profile_photo_path` AS `profile_photo_path`,`em`.`deleted` AS `deleted`,`em`.`date` AS `date`,`wa`.`wage_group` AS `wage_group`,`du`.`name` AS `duty_name` from ((`employee` `em` join `wage` `wa` on((`em`.`wage_id` = `wa`.`id`))) join `duty` `du` on((`em`.`duty_id` = `du`.`id`))) where (`em`.`mans` = 0);

-- --------------------------------------------------------

--
-- Structure for view `fuel_collection_view`
--
DROP TABLE IF EXISTS `fuel_collection_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fuel_collection_view` AS select `cl`.`id` AS `id`,`cl`.`pid` AS `pid`,`cl`.`sid` AS `sid`,`cl`.`employee_id` AS `employee_id`,`cl`.`shift_id` AS `shift_id`,`cl`.`pump_id` AS `pump_id`,`cl`.`product_id` AS `product_id`,`cl`.`price_id` AS `price_id`,`cl`.`initial_reading` AS `initial_reading`,`cl`.`final_reading` AS `final_reading`,`cl`.`amount` AS `amount`,`cl`.`expected_amount` AS `expected_amount`,`cl`.`deleted` AS `deleted`,`cl`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `employee_name`,`pppv`.`product_name` AS `product_name`,`pppv`.`amount` AS `pump_price_per_litre`,`pppv`.`pump_no` AS `pump_no`,`sh`.`name` AS `shift_name` from (((`collection` `cl` join `employee` `em` on((`cl`.`employee_id` = `em`.`id`))) join `shift` `sh` on((`cl`.`shift_id` = `sh`.`id`))) join `pump_product_price_view` `pppv` on((`cl`.`pump_id` = `pppv`.`pump_id`))) group by `cl`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `fuel_gauge_view`
--
DROP TABLE IF EXISTS `fuel_gauge_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fuel_gauge_view` AS select `fg`.`id` AS `id`,`fg`.`pid` AS `pid`,`fg`.`sid` AS `sid`,`fg`.`product_id` AS `product_id`,`fg`.`fill_up` AS `fill_up`,`fg`.`fetch_out` AS `fetch_out`,`pr`.`name` AS `product_name`,sum(`st`.`capacity`) AS `capacity` from ((`fuel_gauge` `fg` join `product` `pr` on((`fg`.`product_id` = `pr`.`id`))) join `storage` `st` on((`fg`.`product_id` = `st`.`product_id`))) where ((`pr`.`deleted` = 0) and (`st`.`deleted` = 0)) group by `fg`.`product_id`;

-- --------------------------------------------------------

--
-- Structure for view `login_parent_view`
--
DROP TABLE IF EXISTS `login_parent_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `login_parent_view` AS select `login`.`id` AS `id`,`login`.`pid` AS `pid`,`login`.`username` AS `username`,`login`.`password` AS `password` from `login` where (`login`.`user_group` = 'p');

-- --------------------------------------------------------

--
-- Structure for view `login_user_view`
--
DROP TABLE IF EXISTS `login_user_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `login_user_view` AS select `lo`.`id` AS `id`,`lo`.`pid` AS `pid`,`lo`.`sid` AS `sid`,`lo`.`employee_id` AS `employee_id`,`lo`.`username` AS `username`,`lo`.`password` AS `password`,`lo`.`user_group` AS `user_group`,`lo`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `user_name`,`em`.`profile_photo_path` AS `profile_photo_path` from (`login` `lo` join `employee` `em` on((`lo`.`employee_id` = `em`.`id`))) where ((`em`.`deleted` = 0) and (`lo`.`user_group` <> 'p'));

-- --------------------------------------------------------

--
-- Structure for view `manager_view`
--
DROP TABLE IF EXISTS `manager_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `manager_view` AS select `em`.`id` AS `id`,`em`.`pid` AS `pid`,`em`.`sid` AS `sid`,`em`.`surname` AS `surname`,`em`.`other_names` AS `other_names`,`em`.`id_no` AS `id_no`,`em`.`contact_phone` AS `contact_phone`,`em`.`profile_photo_path` AS `profile_photo_path`,`em`.`deleted` AS `deleted`,`em`.`date` AS `date`,`si`.`name` AS `station_name` from (`employee` `em` join `station_info` `si` on((`em`.`sid` = `si`.`id`))) where (`em`.`mans` = 1);

-- --------------------------------------------------------

--
-- Structure for view `prepay_total_view`
--
DROP TABLE IF EXISTS `prepay_total_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `prepay_total_view` AS select `prepay_view`.`id` AS `id`,`prepay_view`.`pid` AS `pid`,`prepay_view`.`sid` AS `sid`,`prepay_view`.`prepay_client_id` AS `prepay_client_id`,`prepay_view`.`name` AS `name`,`prepay_view`.`profile_photo_path` AS `profile_photo_path`,sum(`prepay_view`.`amount`) AS `total_amount`,`prepay_view`.`deleted` AS `deleted` from `prepay_view` group by `prepay_view`.`prepay_client_id`;

-- --------------------------------------------------------

--
-- Structure for view `prepay_view`
--
DROP TABLE IF EXISTS `prepay_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `prepay_view` AS select `pr`.`id` AS `id`,`pr`.`pid` AS `pid`,`pr`.`sid` AS `sid`,`pr`.`prepay_client_id` AS `prepay_client_id`,`pr`.`amount` AS `amount`,`pr`.`deleted` AS `deleted`,`pr`.`save_timestamp` AS `save_timestamp`,`pc`.`name` AS `name`,`pc`.`profile_photo_path` AS `profile_photo_path` from (`prepay` `pr` join `prepay_client` `pc` on((`pr`.`prepay_client_id` = `pc`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `price_view`
--
DROP TABLE IF EXISTS `price_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `price_view` AS select `pr`.`id` AS `id`,`pr`.`pid` AS `pid`,`pr`.`sid` AS `sid`,`pr`.`product_id` AS `product_id`,`pt`.`name` AS `product_name`,`pr`.`amount` AS `amount`,`pr`.`deleted` AS `deleted` from (`price` `pr` join `product` `pt` on((`pr`.`product_id` = `pt`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `pump_product_price_view`
--
DROP TABLE IF EXISTS `pump_product_price_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pump_product_price_view` AS select distinct `pr`.`id` AS `id`,`pr`.`pid` AS `pid`,`pr`.`sid` AS `prsid`,`pu`.`sid` AS `pusid`,`pi`.`sid` AS `pisid`,`pr`.`name` AS `product_name`,`pi`.`id` AS `price_id`,`pi`.`amount` AS `amount`,`pu`.`id` AS `pump_id`,`pu`.`pump_no` AS `pump_no`,`pu`.`initial_reading` AS `initial_reading`,`pu`.`final_reading` AS `final_reading`,`pu`.`deleted` AS `deleted` from ((`product` `pr` join `price` `pi` on((`pr`.`id` = `pi`.`product_id`))) join `pump` `pu` on((`pr`.`id` = `pu`.`product_id`)));

-- --------------------------------------------------------

--
-- Structure for view `pump_view`
--
DROP TABLE IF EXISTS `pump_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pump_view` AS select `pu`.`id` AS `id`,`pu`.`pid` AS `pid`,`pu`.`sid` AS `sid`,`pu`.`pump_no` AS `pump_no`,`pu`.`initial_reading` AS `initial_reading`,`pu`.`product_id` AS `product_id`,`pr`.`name` AS `product_name`,`pu`.`deleted` AS `deleted` from (`pump` `pu` join `product` `pr` on((`pu`.`product_id` = `pr`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `stock_view`
--
DROP TABLE IF EXISTS `stock_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_view` AS select `st`.`id` AS `id`,`st`.`pid` AS `pid`,`st`.`sid` AS `sid`,`st`.`quantity` AS `quantity`,`st`.`product_id` AS `product_id`,`sr`.`id` AS `storage_id`,`sr`.`name` AS `storage_name`,`st`.`amount` AS `amount`,`st`.`save_timestamp` AS `save_timestamp`,`pr`.`name` AS `product_name`,`st`.`deleted` AS `deleted` from ((`stock` `st` join `product` `pr` on((`st`.`product_id` = `pr`.`id`))) join `storage` `sr` on((`st`.`product_id` = `sr`.`product_id`)));

-- --------------------------------------------------------

--
-- Structure for view `storage_view`
--
DROP TABLE IF EXISTS `storage_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storage_view` AS select `st`.`id` AS `id`,`st`.`pid` AS `pid`,`st`.`sid` AS `sid`,`st`.`name` AS `storage_name`,`st`.`capacity` AS `capacity`,`st`.`product_id` AS `product_id`,`pr`.`name` AS `product_name`,`st`.`deleted` AS `deleted` from (`storage` `st` join `product` `pr` on((`st`.`product_id` = `pr`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `sys_user_control_view`
--
DROP TABLE IF EXISTS `sys_user_control_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sys_user_control_view` AS select `su`.`id` AS `su_id`,`su`.`employee_id` AS `employee_id`,`su`.`module_id` AS `module_id`,`sm`.`id` AS `id`,`sm`.`aid` AS `aid`,`sm`.`place` AS `place`,`sm`.`class` AS `class`,`sm`.`activity` AS `activity`,`sm`.`published` AS `published`,`sm`.`grouped` AS `grouped`,`sm`.`sort` AS `sort` from (`sys_user_control` `su` join `sys_modules` `sm` on((`sm`.`id` = `su`.`module_id`)));

-- --------------------------------------------------------

--
-- Structure for view `user_account_view`
--
DROP TABLE IF EXISTS `user_account_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_account_view` AS select `ua`.`id` AS `id`,`ua`.`pid` AS `pid`,`ua`.`sid` AS `sid`,`ua`.`employee_id` AS `employee_id`,`ua`.`deleted` AS `deleted`,`ua`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `name`,`em`.`profile_photo_path` AS `profile_photo_path` from (`user_account` `ua` join `employee` `em` on((`ua`.`employee_id` = `em`.`id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
