<?php

include_once 'dbconn.php';
include_once 'dbGeneralFunctions.php';
include_once 'interfaces/dbSelect.php';

/**
 * Interacts with the database;
 *
 * @author nelson
 */
class DbQuery implements dbSelect {

    var $dbh = NULL;
    var $sql_error = NULL;

    public function __construct() {
        $this->dbh = connect();
    }

    public function getAll($table) {
        $stmt = $this->dbh->prepare('SELECT * FROM `' . $table . '`;');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getById($table, $id) {
        $stmt = $this->dbh->prepare('SELECT * FROM `' . $table . '` WHERE `id`=' . $id . ';');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getByColumn($table, $id, $column = 'id') {
        $stmt = $this->dbh->prepare('SELECT * FROM `' . $table . '` WHERE `' . $column . '`=' . $id . ' ORDER BY `id` DESC;');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getColumsById($table, $columns, $id) {
        $stmt = $this->dbh->prepare("SELECT {$columns} FROM `{$table}` WHERE `id`={$id};");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getBySid($table, $sid, $limitStart = 0, $trashed = FALSE) {
        $stmt = $this->dbh->prepare("SELECT * FROM `{$table}` WHERE `sid`={$sid} AND `deleted`=" . ($trashed == FALSE ? 0 : 1) . " ORDER BY `id` DESC LIMIT {$limitStart},100;");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getByPid($table, $id, $limitStart = 0) {
        $stmt = $this->dbh->prepare("SELECT * FROM `{$table}` WHERE `pid`={$id} ORDER BY `id` DESC LIMIT {$limitStart},8;");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getProcedure($procedure, $where) {
        $q = setQs(count($where));
        $stmt = $this->dbh->prepare("call {$procedure}({$q})");
        $i = 1;
        foreach ($where as $value) {
            $stmt->bindValue(($i++), $value);
        }
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->sql_error = $stmt->errorInfo()[2];
        return $result;
    }

    public function runQuery($query) {
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function selectProcedure($procedure, $data) {
        $q = setQs(count($data));
        $stmt = $this->dbh->prepare("call {$procedure}({$q})");
        $i = 1;
        foreach ($data as $value) {
            $stmt->bindValue(($i++), $value);
        }
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}
