<?php

/**
 * @author Nelson
 * 
 * Db Select interface
 * 
 */
interface dbSelect {
    /** Return all results from a table **/
    public function getAll($table);
    /** Return a single result by an ID **/
    public function getById($table,$id);
    /** Return results for a sid **/
    public function getBySid($table,$id);
    /** Return results for a pid **/
    public function getByPid($table,$id);
    /** Return results using a stored procedure **/
    public function getProcedure($procedure, $data);
    /** Executes a query **/
    public function runQuery($query);
}
