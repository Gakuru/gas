<?php

session_start();
include_once 'dbconn.php';
include_once 'dbGeneralFunctions.php';

/**
 * Interacts with the database;
 *
 * @author nelson
 */
class DbInsert {

    var $dbh = NULL;
    var $sql_error = NULL;

    public function __construct() {
        $this->dbh = connect();
    }

    public function insertProcedure($procedure, $data) {
        if (filter_input(INPUT_SERVER, 'REQUEST_URI') == '/gas/station/control/control.php') {
            array_unshift($data, $_SESSION['pid'], $_SESSION['sid']); //Set the pid and sid
        } else if (filter_input(INPUT_SERVER, 'REQUEST_URI') == '/gas/head/control/control.php') {
            array_unshift($data, $_SESSION['pid']); //Set the pid
        }
        $okay = FALSE;
        $q = setQs(count($data));
        $stmt = $this->dbh->prepare("call {$procedure}({$q})");
        $i = 1;
        foreach ($data as $value) {
            $stmt->bindValue(($i++), $value);
        }
        if ($stmt->execute()) {
            $okay = TRUE;
        }
        $this->sql_error = $stmt->errorInfo()[2];
        return $okay;
    }

    public function executeQuery($query) {
        $okay = FALSE;
        $stmt = $this->dbh->prepare($query);
        if ($stmt->execute()) {
            $okay = TRUE;
        }
        $this->sql_error = $stmt->errorInfo()[2];
        return $okay;
    }

}
