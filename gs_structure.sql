-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 24, 2016 at 01:53 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `collection_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_shift_id` INT, IN `p_pump_id` INT, IN `p_product_id` INT, IN `p_price_id` INT, IN `p_initial_reading` DOUBLE, IN `p_final_reading` DOUBLE, IN `p_amount` DOUBLE, IN `p_expected_amount` DOUBLE, IN `p_save_timestamp` INT)
    NO SQL
insert into collection (pid,sid,employee_id,shift_id,pump_id,product_id,price_id,initial_reading,final_reading,amount,expected_amount,save_timestamp) values (p_pid,p_sid,p_employee_id,p_shift_id,p_pump_id,p_product_id,p_price_id,p_initial_reading,p_final_reading,p_amount,p_expected_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `collection_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `fuel_collection_view` where CONCAT(employee_name,' ',shift_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0 group by employee_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `collection_update_final_pump_reading_pro`(IN `p_final_reading` INT, IN `p_id` INT)
    NO SQL
update pump set final_reading=p_final_reading where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `creditor_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_creditor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into creditor (pid,sid,creditor_name,description,contact_phone,profile_photo_path,save_timestamp) values (p_pid,p_sid,p_creditor_name,p_description,p_contact_phone,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `creditor_search_pro`(IN `param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `creditor` where CONCAT(creditor_name,' ',description,' ',contact_phone) like CONCAT('%',param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `creditor_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_creditor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_id` INT)
    NO SQL
update `creditor` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`creditor_name`=p_creditor_name,`description`=p_description,`contact_phone`=p_contact_phone where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `debtor_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_debtor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into debtor (pid,sid,debtor_name,description,contact_phone,profile_photo_path,save_timestamp) values (p_pid,p_sid,p_debtor_name,p_description,p_contact_phone,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `debtor_search_pro`(IN `param` VARCHAR(50), IN `sid` INT)
    NO SQL
SELECT * FROM `debtor` where CONCAT(debtor_name,' ',description,' ',contact_phone) like CONCAT('%',param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `debtor_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_debtor_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_id` INT)
    NO SQL
update `debtor` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`debtor_name`=p_debtor_name,`description`=p_description,`contact_phone`=p_contact_phone where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `duty_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(30), IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `duty` (`pid`,`sid`,`name`,`save_timestamp`) VALUES (p_pid,p_sid,p_name,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `duty_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `duty` where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `duty_update_pro`(IN `p_name` VARCHAR(30), IN `p_id` INT)
    NO SQL
UPDATE `duty` SET `name`=p_name WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `employee_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_surname` VARCHAR(30), IN `p_other_names` VARCHAR(50), IN `p_id_no` VARCHAR(30), IN `p_contact_phone_no` VARCHAR(30), IN `p_wage_id` INT, IN `p_duty_id` INT, IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `employee` (`pid`,`sid`,`surname`,`other_names`,`id_no`,`contact_phone`,`wage_id`,`duty_id`,`profile_photo_path`,`date`) values (p_pid,p_sid,p_surname,p_other_names,p_id_no,p_contact_phone_no,p_wage_id,p_duty_id,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `employee_search_pro`(IN `param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `employee_view` where CONCAT(surname,' ',other_names,' ',id_no,' ',contact_phone,' ',duty_name) like CONCAT('%',param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `employee_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_surname` VARCHAR(20), IN `p_other_names` VARCHAR(50), IN `p_id_no` VARCHAR(30), IN `p_contact_phone` VARCHAR(30), IN `p_wage_id` INT, IN `p_duty_id` INT, IN `p_id` INT)
    NO SQL
update `employee` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`surname`=p_surname,`other_names`=p_other_names,`id_no`=p_id_no,`contact_phone`=p_contact_phone,`wage_id`=p_wage_id,`duty_id`=p_duty_id where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `expense_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_expense_name` VARCHAR(30), IN `p_expense_date` INT, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
insert `expense` (pid,sid,expense_name,expense_date,amount,save_timestamp) values (p_pid,p_sid,p_expense_name,p_expense_date,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `expense_search_pro`(IN `p_param` VARCHAR(20), IN `p_sid` INT)
    NO SQL
SELECT * FROM `expense` where CONCAT(expense_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `expense_update_pro`(IN `p_expense_name` VARCHAR(30), IN `p_expense_date` INT, IN `p_amount` DOUBLE, IN `p_id` INT)
    NO SQL
update expense set expense_name=p_expense_name,expense_date=p_expense_date,amount=p_amount where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fuel_gauge_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_product_id` INT, IN `p_fill_up` DOUBLE, IN `p_fetch_out` DOUBLE)
    NO SQL
insert into fuel_gauge (pid,sid,product_id,fill_up,fetch_out) values (p_pid,p_sid,p_product_id,p_fill_up,p_fetch_out) on duplicate key update fill_up=fill_up+p_fill_up,fetch_out=fetch_out+p_fetch_out$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `login_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_username` VARCHAR(50), IN `p_password` VARCHAR(50), IN `p_user_group` VARCHAR(1), IN `p_save_timestamp` INT)
    NO SQL
insert into login (pid,sid,employee_id,username,password,user_group,save_timestamp) values (p_pid,p_sid,p_employee_id,p_username,p_password,p_user_group,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `manager_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_surname` VARCHAR(30), IN `p_other_names` VARCHAR(50), IN `p_id_no` VARCHAR(30), IN `p_contact_phone_no` VARCHAR(30), IN `p_profile_photo_path` VARCHAR(255), IN `p_mans` BOOLEAN, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `employee` (`pid`,`sid`,`surname`,`other_names`,`id_no`,`contact_phone`,`profile_photo_path`,`mans`,`date`) values (p_pid,p_sid,p_surname,p_other_names,p_id_no,p_contact_phone_no,p_profile_photo_path,p_mans,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `parent_insert_pro`(IN `p_prop_name` VARCHAR(50), IN `p_stat_name` VARCHAR(50), IN `p_contact_phone` VARCHAR(20), IN `p_contact_email` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into parent (prop_name,stat_name,contact_phone,contact_email,save_timestamp) values (p_prop_name,p_stat_name,p_contact_phone,p_contact_email,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_client_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into prepay_client (pid,sid,name,description,contact_phone,profile_photo_path,save_timestamp) values (p_pid,p_sid,p_name,p_description,p_contact_phone,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_client_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `prepay_client` where CONCAT(name,' ',description,' ',contact_phone) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_client_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_name` VARCHAR(50), IN `p_description` MEDIUMTEXT, IN `p_contact_phone` VARCHAR(20), IN `p_id` INT)
    NO SQL
update `prepay_client` set `profile_photo_path` =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path),`name`=p_name,`description`=p_description,`contact_phone`=p_contact_phone where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_prepay_client_id` INT, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `prepay` (`pid`,`sid`,`prepay_client_id`,`amount`,`save_timestamp`) VALUES (p_pid,p_sid,p_prepay_client_id,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `prepay_total_view`where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prepay_update_pro`(IN `p_prepay_client_id` INT, IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
UPDATE `prepay` SET `prepay_client_id`=p_prepay_client_id,`amount`=p_amount WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `price_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_product_id` INT, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `price` (`pid`,`sid`,`product_id`,`amount`,`save_timestamp`) VALUES (p_pid,p_sid,p_product_id,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `price_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `price_view`where CONCAT(product_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `price_update_pro`(IN `p_product_id` INT, IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
UPDATE `price` SET `product_id`=p_product_id,`amount`=p_amount WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `product_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(50), IN `p_save_timestamp` INT)
    NO SQL
insert into product (pid,sid,name,save_timestamp) values (p_pid,p_sid,p_name,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `product_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `product` where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `product_update_pro`(IN `p_name` VARCHAR(50), IN `p_id` INT)
    NO SQL
update product set name=p_name where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pump_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_pump_no` VARCHAR(30), IN `p_product_id` INT, IN `p_initial_reading` INT, IN `p_save_timestamp` INT)
    NO SQL
insert into pump (pid,sid,pump_no,product_id,initial_reading,final_reading,save_timestamp) VALUES (p_pid,p_sid,p_pump_no,p_product_id,p_initial_reading,p_initial_reading,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pump_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT puv.id as pump_id,puv.initial_reading as final_reading,puv.sid,puv.product_name,puv.pump_no,prv.amount FROM `pump_view` puv join price_view prv on puv.product_id=prv.product_id where concat(puv.pump_no,' ',puv.product_name) like CONCAT('%',p_param,'%') and puv.sid = p_sid group by puv.id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pump_update_pro`(IN `p_pump_no` VARCHAR(30), IN `p_product_id` INT, IN `p_initial_reading` INT, IN `p_id` INT)
    NO SQL
update pump set pump_no=p_pump_no, product_id=p_product_id,initial_reading=p_initial_reading where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `shift_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(20), IN `p_sfrom` VARCHAR(20), IN `p_sto` VARCHAR(20), IN `p_save_timestamp` INT)
    NO SQL
insert `shift` (pid,sid,name,sfrom,sto,save_timestamp) values (p_pid,p_sid,p_name,p_sfrom,p_sto,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `shift_search_pro`(IN `p_param` VARCHAR(20), IN `p_sid` INT)
    NO SQL
SELECT * FROM `shift` where CONCAT(name,' ',sfrom,' ',sto) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `shift_update_pro`(IN `p_name` VARCHAR(20), IN `p_sfrom` VARCHAR(20), IN `p_sto` VARCHAR(20), IN `p_id` INT)
    NO SQL
update shift set name=p_name,sfrom=p_sfrom,sto=p_sto where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `station_info_insert_pro`(IN `p_pid` INT, IN `p_name` VARCHAR(50), IN `p_location` VARCHAR(100), IN `p_contact_phone` VARCHAR(20), IN `p_contact_address` VARCHAR(255), IN `p_contact_email` VARCHAR(255), IN `p_profile_photo_path` VARCHAR(255), IN `p_save_timestamp` INT)
    NO SQL
insert into station_info (pid,name,location,contact_phone,contact_address,contact_email,profile_photo_path,save_timestamp) values (p_pid,p_name,p_location,p_contact_phone,p_contact_address,p_contact_email,p_profile_photo_path,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `station_info_search_pro`(IN `p_param` VARCHAR(50), IN `p_pid` INT)
    NO SQL
SELECT * FROM `station_info` where CONCAT(name,' ',location,' ',contact_address,' ',contact_phone,' ',contact_email) like CONCAT('%',p_param,'%') AND pid=p_pid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `station_info_update_pro`(IN `p_profile_photo_path` VARCHAR(255), IN `p_name` VARCHAR(50), IN `p_station_manager` VARCHAR(50), IN `p_location` VARCHAR(100), IN `p_contact_phone` VARCHAR(20), IN `p_contact_address` VARCHAR(255), IN `p_contact_email` VARCHAR(255), IN `p_id` INT)
    NO SQL
update station_info set profile_photo_path =if(p_profile_photo_path='',profile_photo_path,p_profile_photo_path), name=p_name, station_manager=p_station_manager, location=p_location,contact_phone=p_contact_phone,contact_address=p_contact_address,contact_email=p_contact_email where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stock_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_product_id` INT, IN `p_storage_id` INT, IN `p_quantity` DOUBLE, IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
insert into stock (pid,sid,product_id,storage_id,quantity,amount,save_timestamp) VALUES (p_pid,p_sid,p_product_id,p_storage_id,p_quantity,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stock_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `stock_view` where CONCAT(product_name,' ',quantity,' ',amount) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stock_update_pro`(IN `p_product_id` INT, IN `p_storage_id` INT, IN `p_quantity` DOUBLE, IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
update stock set product_id=p_product_id,storage_id=p_storage_id,quantity=p_quantity,amount=p_amount where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `storage_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_name` VARCHAR(30), IN `p_product_id` INT, IN `p_capacity` DOUBLE, IN `p_save_timestamp` INT)
    NO SQL
insert into storage (pid,sid,name,product_id,capacity,save_timestamp) VALUES (p_pid,p_sid,p_name,p_product_id,p_capacity,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `storage_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `storage_view` where CONCAT(storage_name,' ',product_name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `storage_update_pro`(IN `p_name` VARCHAR(30), IN `p_product_id` INT, IN `p_capacity` INT, IN `p_id` INT)
    NO SQL
update storage set name=p_name, product_id=p_product_id,capacity=p_capacity where id=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sys_user_control_get_system_modules_pro`(IN `p_sid` INT, IN `p_employee_id` INT)
    NO SQL
select sm.* from sys_modules as sm where sm.id not in (select sc.module_id from sys_user_control as sc where sc.sid=p_sid and sc.employee_id=p_employee_id) and sm.published=1 and sm.aid!='aid' order by sort asc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sys_user_control_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_module_id` INT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `sys_user_control` (`pid`,`sid`,`employee_id`,`module_id`,`save_timestamp`) VALUES (p_pid,p_sid,p_employee_id,p_module_id,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_account_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_employee_id` INT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `user_account` (`pid`,`sid`,`employee_id`,`save_timestamp`) VALUES (p_pid,p_sid,p_employee_id,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_account_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `user_account_view` where CONCAT(name) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_account_update_pro`(IN `p_employee_id` INT, IN `p_id` INT)
    NO SQL
UPDATE `user_account` SET `employee_id`=p_employee_id WHERE `id`=p_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `wage_insert_pro`(IN `p_pid` INT, IN `p_sid` INT, IN `p_wage_group` VARCHAR(10), IN `p_amount` FLOAT, IN `p_save_timestamp` INT)
    NO SQL
INSERT INTO `wage` (`pid`,`sid`,`wage_group`,`amount`,`save_timestamp`) VALUES (p_pid,p_sid,p_wage_group,p_amount,p_save_timestamp)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `wage_search_pro`(IN `p_param` VARCHAR(50), IN `p_sid` INT)
    NO SQL
SELECT * FROM `wage`where CONCAT(wage_group,' ',amount) like CONCAT('%',p_param,'%') AND sid=p_sid AND `deleted`=0$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `wage_update_pro`(IN `p_wage_group` VARCHAR(10), IN `p_amount` FLOAT, IN `p_id` INT)
    NO SQL
UPDATE `wage` SET `wage_group`=p_wage_group,`amount`=p_amount WHERE `id`=p_id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE IF NOT EXISTS `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `pump_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `initial_reading` bigint(20) NOT NULL,
  `final_reading` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `expected_amount` double NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `save_timestamp` (`save_timestamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `creditor`
--

CREATE TABLE IF NOT EXISTS `creditor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `creditor_name` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_phone` (`contact_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `debtor`
--

CREATE TABLE IF NOT EXISTS `debtor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `debtor_name` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_phone` (`contact_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `duty`
--

CREATE TABLE IF NOT EXISTS `duty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `other_names` varchar(50) NOT NULL,
  `id_no` varchar(30) NOT NULL,
  `contact_phone` varchar(30) NOT NULL,
  `wage_id` int(11) DEFAULT NULL,
  `duty_id` int(11) DEFAULT NULL,
  `profile_photo_path` varchar(255) DEFAULT NULL,
  `mans` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_no` (`id_no`),
  UNIQUE KEY `contact_phone` (`contact_phone`),
  UNIQUE KEY `name_unique` (`surname`,`other_names`) COMMENT 'surname and other names are unique'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `employee_view`
--
CREATE TABLE IF NOT EXISTS `employee_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`surname` varchar(20)
,`other_names` varchar(50)
,`id_no` varchar(30)
,`contact_phone` varchar(30)
,`wage_id` int(11)
,`duty_id` int(11)
,`profile_photo_path` varchar(255)
,`deleted` tinyint(1)
,`date` int(11)
,`wage_group` varchar(10)
,`duty_name` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `expense_name` varchar(30) NOT NULL,
  `expense_date` int(11) NOT NULL,
  `amount` double NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `fuel_collection_view`
--
CREATE TABLE IF NOT EXISTS `fuel_collection_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`employee_id` int(11)
,`shift_id` int(11)
,`pump_id` int(11)
,`product_id` int(11)
,`price_id` int(11)
,`initial_reading` bigint(20)
,`final_reading` bigint(20)
,`amount` double
,`expected_amount` double
,`deleted` tinyint(1)
,`save_timestamp` int(11)
,`employee_name` varchar(71)
,`product_name` varchar(50)
,`pump_price_per_litre` float
,`pump_no` varchar(30)
,`shift_name` varchar(20)
);
-- --------------------------------------------------------

--
-- Table structure for table `fuel_gauge`
--

CREATE TABLE IF NOT EXISTS `fuel_gauge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `fill_up` double NOT NULL,
  `fetch_out` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pid_sid_product` (`pid`,`sid`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `fuel_gauge_view`
--
CREATE TABLE IF NOT EXISTS `fuel_gauge_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`product_id` int(11)
,`fill_up` double
,`fetch_out` double
,`product_name` varchar(50)
,`capacity` double
);
-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL COMMENT 'also parent id',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_group` varchar(1) NOT NULL DEFAULT 'b',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `login_parent_view`
--
CREATE TABLE IF NOT EXISTS `login_parent_view` (
`id` int(11)
,`pid` int(11)
,`username` varchar(50)
,`password` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `login_user_view`
--
CREATE TABLE IF NOT EXISTS `login_user_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`employee_id` int(11)
,`username` varchar(50)
,`password` varchar(50)
,`user_group` varchar(1)
,`save_timestamp` int(11)
,`user_name` varchar(71)
,`profile_photo_path` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `manager_view`
--
CREATE TABLE IF NOT EXISTS `manager_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`surname` varchar(20)
,`other_names` varchar(50)
,`id_no` varchar(30)
,`contact_phone` varchar(30)
,`profile_photo_path` varchar(255)
,`deleted` tinyint(1)
,`date` int(11)
,`station_name` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `m_pesa_payments`
--

CREATE TABLE IF NOT EXISTS `m_pesa_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `service_name` varchar(40) NOT NULL,
  `business_number` varchar(10) NOT NULL,
  `transaction_reference` varchar(20) NOT NULL,
  `internal_transaction_id` varchar(20) NOT NULL,
  `transaction_timestamp` int(11) NOT NULL,
  `transaction_datestamp` varchar(30) NOT NULL,
  `transaction_type` varchar(20) NOT NULL,
  `account_number` varchar(30) NOT NULL,
  `sender_phone` varchar(20) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `amount` double NOT NULL,
  `currency` varchar(20) NOT NULL,
  `signature` varchar(50) NOT NULL,
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

CREATE TABLE IF NOT EXISTS `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_name` varchar(50) NOT NULL,
  `stat_name` varchar(50) NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prepay`
--

CREATE TABLE IF NOT EXISTS `prepay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `prepay_client_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prepay_client`
--

CREATE TABLE IF NOT EXISTS `prepay_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_phone` (`contact_phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `prepay_total_view`
--
CREATE TABLE IF NOT EXISTS `prepay_total_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`prepay_client_id` int(11)
,`name` varchar(50)
,`profile_photo_path` varchar(255)
,`total_amount` double
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `prepay_view`
--
CREATE TABLE IF NOT EXISTS `prepay_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`prepay_client_id` int(11)
,`amount` float
,`deleted` tinyint(1)
,`save_timestamp` int(11)
,`name` varchar(50)
,`profile_photo_path` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `price_view`
--
CREATE TABLE IF NOT EXISTS `price_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`product_id` int(11)
,`product_name` varchar(50)
,`amount` float
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product` (`sid`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pump`
--

CREATE TABLE IF NOT EXISTS `pump` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `pump_no` varchar(30) NOT NULL,
  `product_id` int(11) NOT NULL,
  `initial_reading` int(11) NOT NULL,
  `final_reading` int(11) DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pump_no_sid` (`pump_no`,`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `pump_product_price_view`
--
CREATE TABLE IF NOT EXISTS `pump_product_price_view` (
`id` int(11)
,`pid` int(11)
,`prsid` int(11)
,`pusid` int(11)
,`pisid` int(11)
,`product_name` varchar(50)
,`price_id` int(11)
,`amount` float
,`pump_id` int(11)
,`pump_no` varchar(30)
,`initial_reading` int(11)
,`final_reading` int(11)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `pump_view`
--
CREATE TABLE IF NOT EXISTS `pump_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`pump_no` varchar(30)
,`initial_reading` int(11)
,`product_id` int(11)
,`product_name` varchar(50)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE IF NOT EXISTS `shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `sfrom` varchar(20) NOT NULL,
  `sto` varchar(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shift_duration` (`pid`,`sid`,`sfrom`,`sto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `station_info`
--

CREATE TABLE IF NOT EXISTS `station_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `employee_id` int(11) DEFAULT '0' COMMENT 'Station manager',
  `location` varchar(100) DEFAULT NULL,
  `contact_phone` varchar(20) DEFAULT NULL,
  `contact_address` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `profile_photo_path` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `stock_view`
--
CREATE TABLE IF NOT EXISTS `stock_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`quantity` double
,`product_id` int(11)
,`storage_id` int(11)
,`storage_name` varchar(30)
,`amount` float
,`save_timestamp` int(11)
,`product_name` varchar(50)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE IF NOT EXISTS `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `product_id` int(11) NOT NULL,
  `capacity` double NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `storage_view`
--
CREATE TABLE IF NOT EXISTS `storage_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`storage_name` varchar(30)
,`capacity` double
,`product_id` int(11)
,`product_name` varchar(50)
,`deleted` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `sys_modules`
--

CREATE TABLE IF NOT EXISTS `sys_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` varchar(50) NOT NULL,
  `place` varchar(10) NOT NULL,
  `class` varchar(50) NOT NULL,
  `activity` varchar(75) NOT NULL,
  `sort` varchar(3) DEFAULT NULL,
  `grouped` varchar(3) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_user_control`
--

CREATE TABLE IF NOT EXISTS `sys_user_control` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emp_mod` (`employee_id`,`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `sys_user_control_view`
--
CREATE TABLE IF NOT EXISTS `sys_user_control_view` (
`su_id` int(11)
,`employee_id` int(11)
,`module_id` int(11)
,`id` int(11)
,`aid` varchar(50)
,`place` varchar(10)
,`class` varchar(50)
,`activity` varchar(75)
,`published` tinyint(1)
,`grouped` varchar(3)
,`sort` varchar(3)
);
-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_account_view`
--
CREATE TABLE IF NOT EXISTS `user_account_view` (
`id` int(11)
,`pid` int(11)
,`sid` int(11)
,`employee_id` int(11)
,`deleted` tinyint(1)
,`save_timestamp` int(11)
,`name` varchar(71)
,`profile_photo_path` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `wage`
--

CREATE TABLE IF NOT EXISTS `wage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `wage_group` varchar(10) NOT NULL,
  `amount` float NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `save_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `employee_view`
--
DROP TABLE IF EXISTS `employee_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `employee_view` AS select `em`.`id` AS `id`,`em`.`pid` AS `pid`,`em`.`sid` AS `sid`,`em`.`surname` AS `surname`,`em`.`other_names` AS `other_names`,`em`.`id_no` AS `id_no`,`em`.`contact_phone` AS `contact_phone`,`em`.`wage_id` AS `wage_id`,`em`.`duty_id` AS `duty_id`,`em`.`profile_photo_path` AS `profile_photo_path`,`em`.`deleted` AS `deleted`,`em`.`date` AS `date`,`wa`.`wage_group` AS `wage_group`,`du`.`name` AS `duty_name` from ((`employee` `em` join `wage` `wa` on((`em`.`wage_id` = `wa`.`id`))) join `duty` `du` on((`em`.`duty_id` = `du`.`id`))) where (`em`.`mans` = 0);

-- --------------------------------------------------------

--
-- Structure for view `fuel_collection_view`
--
DROP TABLE IF EXISTS `fuel_collection_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fuel_collection_view` AS select `cl`.`id` AS `id`,`cl`.`pid` AS `pid`,`cl`.`sid` AS `sid`,`cl`.`employee_id` AS `employee_id`,`cl`.`shift_id` AS `shift_id`,`cl`.`pump_id` AS `pump_id`,`cl`.`product_id` AS `product_id`,`cl`.`price_id` AS `price_id`,`cl`.`initial_reading` AS `initial_reading`,`cl`.`final_reading` AS `final_reading`,`cl`.`amount` AS `amount`,`cl`.`expected_amount` AS `expected_amount`,`cl`.`deleted` AS `deleted`,`cl`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `employee_name`,`pppv`.`product_name` AS `product_name`,`pppv`.`amount` AS `pump_price_per_litre`,`pppv`.`pump_no` AS `pump_no`,`sh`.`name` AS `shift_name` from (((`collection` `cl` join `employee` `em` on((`cl`.`employee_id` = `em`.`id`))) join `shift` `sh` on((`cl`.`shift_id` = `sh`.`id`))) join `pump_product_price_view` `pppv` on((`cl`.`pump_id` = `pppv`.`pump_id`))) group by `cl`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `fuel_gauge_view`
--
DROP TABLE IF EXISTS `fuel_gauge_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fuel_gauge_view` AS select `fg`.`id` AS `id`,`fg`.`pid` AS `pid`,`fg`.`sid` AS `sid`,`fg`.`product_id` AS `product_id`,`fg`.`fill_up` AS `fill_up`,`fg`.`fetch_out` AS `fetch_out`,`pr`.`name` AS `product_name`,sum(`st`.`capacity`) AS `capacity` from ((`fuel_gauge` `fg` join `product` `pr` on((`fg`.`product_id` = `pr`.`id`))) join `storage` `st` on((`fg`.`product_id` = `st`.`product_id`))) where ((`pr`.`deleted` = 0) and (`st`.`deleted` = 0)) group by `fg`.`product_id`;

-- --------------------------------------------------------

--
-- Structure for view `login_parent_view`
--
DROP TABLE IF EXISTS `login_parent_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `login_parent_view` AS select `login`.`id` AS `id`,`login`.`pid` AS `pid`,`login`.`username` AS `username`,`login`.`password` AS `password` from `login` where (`login`.`user_group` = 'p');

-- --------------------------------------------------------

--
-- Structure for view `login_user_view`
--
DROP TABLE IF EXISTS `login_user_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `login_user_view` AS select `lo`.`id` AS `id`,`lo`.`pid` AS `pid`,`lo`.`sid` AS `sid`,`lo`.`employee_id` AS `employee_id`,`lo`.`username` AS `username`,`lo`.`password` AS `password`,`lo`.`user_group` AS `user_group`,`lo`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `user_name`,`em`.`profile_photo_path` AS `profile_photo_path` from (`login` `lo` join `employee` `em` on((`lo`.`employee_id` = `em`.`id`))) where ((`em`.`deleted` = 0) and (`lo`.`user_group` <> 'p'));

-- --------------------------------------------------------

--
-- Structure for view `manager_view`
--
DROP TABLE IF EXISTS `manager_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `manager_view` AS select `em`.`id` AS `id`,`em`.`pid` AS `pid`,`em`.`sid` AS `sid`,`em`.`surname` AS `surname`,`em`.`other_names` AS `other_names`,`em`.`id_no` AS `id_no`,`em`.`contact_phone` AS `contact_phone`,`em`.`profile_photo_path` AS `profile_photo_path`,`em`.`deleted` AS `deleted`,`em`.`date` AS `date`,`si`.`name` AS `station_name` from (`employee` `em` join `station_info` `si` on((`em`.`sid` = `si`.`id`))) where (`em`.`mans` = 1);

-- --------------------------------------------------------

--
-- Structure for view `prepay_total_view`
--
DROP TABLE IF EXISTS `prepay_total_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `prepay_total_view` AS select `prepay_view`.`id` AS `id`,`prepay_view`.`pid` AS `pid`,`prepay_view`.`sid` AS `sid`,`prepay_view`.`prepay_client_id` AS `prepay_client_id`,`prepay_view`.`name` AS `name`,`prepay_view`.`profile_photo_path` AS `profile_photo_path`,sum(`prepay_view`.`amount`) AS `total_amount`,`prepay_view`.`deleted` AS `deleted` from `prepay_view` group by `prepay_view`.`prepay_client_id`;

-- --------------------------------------------------------

--
-- Structure for view `prepay_view`
--
DROP TABLE IF EXISTS `prepay_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `prepay_view` AS select `pr`.`id` AS `id`,`pr`.`pid` AS `pid`,`pr`.`sid` AS `sid`,`pr`.`prepay_client_id` AS `prepay_client_id`,`pr`.`amount` AS `amount`,`pr`.`deleted` AS `deleted`,`pr`.`save_timestamp` AS `save_timestamp`,`pc`.`name` AS `name`,`pc`.`profile_photo_path` AS `profile_photo_path` from (`prepay` `pr` join `prepay_client` `pc` on((`pr`.`prepay_client_id` = `pc`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `price_view`
--
DROP TABLE IF EXISTS `price_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `price_view` AS select `pr`.`id` AS `id`,`pr`.`pid` AS `pid`,`pr`.`sid` AS `sid`,`pr`.`product_id` AS `product_id`,`pt`.`name` AS `product_name`,`pr`.`amount` AS `amount`,`pr`.`deleted` AS `deleted` from (`price` `pr` join `product` `pt` on((`pr`.`product_id` = `pt`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `pump_product_price_view`
--
DROP TABLE IF EXISTS `pump_product_price_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pump_product_price_view` AS select distinct `pr`.`id` AS `id`,`pr`.`pid` AS `pid`,`pr`.`sid` AS `prsid`,`pu`.`sid` AS `pusid`,`pi`.`sid` AS `pisid`,`pr`.`name` AS `product_name`,`pi`.`id` AS `price_id`,`pi`.`amount` AS `amount`,`pu`.`id` AS `pump_id`,`pu`.`pump_no` AS `pump_no`,`pu`.`initial_reading` AS `initial_reading`,`pu`.`final_reading` AS `final_reading`,`pu`.`deleted` AS `deleted` from ((`product` `pr` join `price` `pi` on((`pr`.`id` = `pi`.`product_id`))) join `pump` `pu` on((`pr`.`id` = `pu`.`product_id`)));

-- --------------------------------------------------------

--
-- Structure for view `pump_view`
--
DROP TABLE IF EXISTS `pump_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pump_view` AS select `pu`.`id` AS `id`,`pu`.`pid` AS `pid`,`pu`.`sid` AS `sid`,`pu`.`pump_no` AS `pump_no`,`pu`.`initial_reading` AS `initial_reading`,`pu`.`product_id` AS `product_id`,`pr`.`name` AS `product_name`,`pu`.`deleted` AS `deleted` from (`pump` `pu` join `product` `pr` on((`pu`.`product_id` = `pr`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `stock_view`
--
DROP TABLE IF EXISTS `stock_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_view` AS select `st`.`id` AS `id`,`st`.`pid` AS `pid`,`st`.`sid` AS `sid`,`st`.`quantity` AS `quantity`,`st`.`product_id` AS `product_id`,`sr`.`id` AS `storage_id`,`sr`.`name` AS `storage_name`,`st`.`amount` AS `amount`,`st`.`save_timestamp` AS `save_timestamp`,`pr`.`name` AS `product_name`,`st`.`deleted` AS `deleted` from ((`stock` `st` join `product` `pr` on((`st`.`product_id` = `pr`.`id`))) join `storage` `sr` on((`st`.`product_id` = `sr`.`product_id`)));

-- --------------------------------------------------------

--
-- Structure for view `storage_view`
--
DROP TABLE IF EXISTS `storage_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `storage_view` AS select `st`.`id` AS `id`,`st`.`pid` AS `pid`,`st`.`sid` AS `sid`,`st`.`name` AS `storage_name`,`st`.`capacity` AS `capacity`,`st`.`product_id` AS `product_id`,`pr`.`name` AS `product_name`,`st`.`deleted` AS `deleted` from (`storage` `st` join `product` `pr` on((`st`.`product_id` = `pr`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `sys_user_control_view`
--
DROP TABLE IF EXISTS `sys_user_control_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sys_user_control_view` AS select `su`.`id` AS `su_id`,`su`.`employee_id` AS `employee_id`,`su`.`module_id` AS `module_id`,`sm`.`id` AS `id`,`sm`.`aid` AS `aid`,`sm`.`place` AS `place`,`sm`.`class` AS `class`,`sm`.`activity` AS `activity`,`sm`.`published` AS `published`,`sm`.`grouped` AS `grouped`,`sm`.`sort` AS `sort` from (`sys_user_control` `su` join `sys_modules` `sm` on((`sm`.`id` = `su`.`module_id`)));

-- --------------------------------------------------------

--
-- Structure for view `user_account_view`
--
DROP TABLE IF EXISTS `user_account_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_account_view` AS select `ua`.`id` AS `id`,`ua`.`pid` AS `pid`,`ua`.`sid` AS `sid`,`ua`.`employee_id` AS `employee_id`,`ua`.`deleted` AS `deleted`,`ua`.`save_timestamp` AS `save_timestamp`,concat(`em`.`surname`,' ',`em`.`other_names`) AS `name`,`em`.`profile_photo_path` AS `profile_photo_path` from (`user_account` `ua` join `employee` `em` on((`ua`.`employee_id` = `em`.`id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
